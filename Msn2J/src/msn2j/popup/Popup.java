package msn2j.popup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Popup extends javax.swing.JFrame implements ActionListener {
    
    private int count = 1;
    private Dimension screenSize;
    private Timer timer;
    private int popNum;
    private int toBigValue;
    private Insets desktopInsets = null;
    
    public Popup(boolean[] popList, String message) {
        setVisible(false);
        for (int i = 0; i < popList.length; i++) {
            if (popList[i] == false) {
                popNum = i;
                PopupMaker.addPopListNumber(i);
                break;
            }
        }
        this.popNum = popNum;
        screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        toBigValue = (int)Math.floor(screenSize.height / 100) - 1;
        initComponents();
        desktopInsets = Linsets.getInsets();
        
        System.out.println(desktopInsets.top);
        
        int extraX = (int)(popNum / toBigValue) * 200;
        int extraH = popNum * 102 - (int)(popNum / toBigValue) * toBigValue * 102;
        this.setBounds(screenSize.width - 200 - extraX - desktopInsets.right, screenSize.height - extraH - 10 * count - desktopInsets.bottom, 200, 10 * count);
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                setVisible(true);
            }
        });
        
        timer = new Timer(100, this);
        timer.setActionCommand("expand");
        timer.start(); 

        lMessage.setText(message);

    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("expand")) {
            int extraX = (int)(popNum / toBigValue) * 200;
            int extraH = popNum * 102 - (int)(popNum / toBigValue) * toBigValue * 102;
            this.setBounds(screenSize.width - 200 - extraX - desktopInsets.right, screenSize.height - extraH - 10 * count - desktopInsets.bottom, 200, 10 * count);
            if (count == 10) {
                timer.stop();
                timer = new Timer(5000, this);
                timer.setActionCommand("dispose");
                timer.start();
            } else
                count++;
        } else {
            PopupMaker.deletePopListNumber(popNum);
            this.dispose();
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jScrollPane1 = new javax.swing.JScrollPane();
        lMessage = new javax.swing.JTextPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBounds(new java.awt.Rectangle(0, 0, 200, 10));
        setFocusableWindowState(false);
        setResizable(false);
        setUndecorated(true);
        lMessage.setEditable(false);
        lMessage.setOpaque(false);
        jScrollPane1.setViewportView(lMessage);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                .addContainerGap())
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextPane lMessage;
    // End of variables declaration//GEN-END:variables
    
}
