package msn2j.popup;

public final class PopupMaker {
    
    private static boolean[] popList = new boolean[128];
    
    public PopupMaker() {
        for (int i = 0; i < popList.length; i++) {
            popList[i] = false;
        }
    }
    
    public static void newPopup(String message) {  
        
        Popup popup = new Popup(popList, message);
    }
    
    public static void addPopListNumber(int popNum) {
        popList[popNum] = true;
    }
    
    public static void deletePopListNumber(int popNum) {
        popList[popNum] = false;
    }
    
}
