/* Linsets
 *
 * 	A class for getting the screen
 * 	insets for non-Windows Operating Systems
 *
 * 	By:
 * 		A. Tres Finocchiaro
 * 	Liscense:
 * 		http://www.gnu.org/copyleft/gpl.html
 *	Compatible with:
 *		Java 1.5 SE, Java 1.6 SE
 *	Keywords:
 *		insets, osx, linux, unix, 
 *		solaris, kde, gnome, xfce, 
 *		ubuntu, java, darwin
 */
package msn2j.popup;

import java.io.*;
import java.util.*;
import java.awt.*;
 
public class Linsets {
	private static final String DESKTOP_ENVIRONMENTS = "kdesktop|gnome-panel|xfce|darwin";
	
	
	private static final String GNOME_CONFIG = "%gconf.xml";
	private static final String GNOME_PANEL = "_panel_screen";
	private static final String GNOME_ROOT = System.getProperty("user.home") + "/.gconf/apps/panel/toplevels/";
	
	private static final String KDE_CONFIG = System.getProperty("user.home") + "/.kde/share/config/kickerrc";
	
	private static final String XFCE_CONFIG = System.getProperty("user.home") + "/.config/xfce4/mcs_settings/panel.xml";
	
	private static final String OS_NAME = System.getProperty("os.name");
	
	public static Insets getInsets() {
		switch (isCompatibleOS()) {
			case  0: return getKDEInsets();
			case  1: return getGnomeInsets();
			case  2: return getXfceInsets();
			case  3: return getDarwinInsets();
			default: return getDefaultInsets();
		}
	}
	
	/*
	 * Default insets if an error occured
	 */
	private static Insets getDefaultInsets() {
		System.err.println("\nPlease see: http://www.google.com/search?q=gnome+kde+insets\n\n"); 
		System.err.println("\nTrying default insets for " + OS_NAME);
		try {
			for (GraphicsDevice gs : GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()) 
				for (GraphicsConfiguration gc : gs.getConfigurations())
					return(Toolkit.getDefaultToolkit().getScreenInsets(gc));
				
		}
		catch (HeadlessException h) {
			System.err.println("Error: Headless error (you aren't running a GUI)");
		}
		return new Insets(0,0,0,0);
	}
	
	/*
	 * Created for Mac OS 10.x.x
	 */
	private static Insets getDarwinInsets() {
		return getDefaultInsets();
	}
	
	/*
	 * Created for Gnome 2.12.x.x
	 */
        
        /*
         * Not the original function, replaces with povtux' code snippet,
         * http://forum.java.sun.com/thread.jspa?threadID=5169228&start=30&tstart=0
         */
        private static Insets getGnomeInsets() {
            int n=0; int s=0; int e=0; int w=0;
 
            try {
                    // get the list of panels
                    Process p = Runtime.getRuntime().exec("gconftool --all-dirs /apps/panel/toplevels");
                    BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
 
                    Process p2;
                    BufferedReader r2;
                    String line = r.readLine();
                    while (line != null) {
                            // get the orientation of the panel (top/bottom/left/right
                            p2 = Runtime.getRuntime().exec("gconftool -g "+line+"/orientation");
                            r2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
                            String orientation = r2.readLine();
                            
                            // get the auto_hide value to know witch size to get
                            p2 = Runtime.getRuntime().exec("gconftool -g "+line+"/auto_hide");
                            r2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
                            String auto_hide = r2.readLine();
                            
                            // get the right size
                            int theSize = 0;
                            if(auto_hide.toLowerCase().startsWith("true")) {
                                p2 = Runtime.getRuntime().exec("gconftool -g "+line+"/auto_hide_size");
                                r2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
                                theSize = Integer.parseInt(r2.readLine());
                            }
                            else {
                                p2 = Runtime.getRuntime().exec("gconftool -g "+line+"/size");
                                r2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
                                theSize = Integer.parseInt(r2.readLine());
                            }
                            
                            // affect the right size's value to the good variable (n, s, e, w)
                            if(orientation.toLowerCase().startsWith("top")) {
                                n += theSize;
                            }
                            else if(orientation.toLowerCase().startsWith("bottom")) {
                                s += theSize;
                            }
                            else if(orientation.toLowerCase().startsWith("left")) {
                                w += theSize;
                            }
                            else if(orientation.toLowerCase().startsWith("right")) {
                                e += theSize;
                            }
 
                            line = r.readLine();
                    }
            }
            catch (Exception ex) {
                    ex.printStackTrace();
                    System.out.println("Error: IO Exception during Runtime exec");
            }
            
            return new Insets(n, w, s, e);
	}
	
	private static Insets getXfceInsets() {
		return getDefaultInsets();
	}
	
	/* 
	 * Created for KDE 3.5 
	 */
	private static Insets getKDEInsets() {
		/* KDE:		2 Top			|	JAVA:		0 Top
		 *	0  Left		1  Right	|		1 Left 		3 Right
		 *		3 Bottom		|			2 Bottom
		 */
		int[] sizes = {24, 30, 46, 58, 0};	// 	xSmall, Small, Medium, Large, xLarge, Null
		int[] i = {0, 0, 0, 0, 0}; 		// 	Left, Right, Top, Bottom, Null
		
		
		/* Needs to be fixed.  Doesn't know the difference between CustomSize and Size */
		int customSiz = getKdeINI("General", "CustomSize");
		int siz = getKdeINI("General", "Size");
		int pos = getKdeINI("General", "Position");
		int position = pos==-1?3:pos;
		int size = (customSiz==-1||siz!=4)?siz:customSiz;
		size = size<24?sizes[size]:size;
		i[position]=size;
		return new Insets(i[2],i[0],i[3],i[1]);
	}
	
	/*
	 * Determine if current Operating System is a *nix flavor 
	 */
	private static int isCompatibleOS() {
		if (!OS_NAME.toLowerCase().startsWith("windows")) {
			try {
				Process p = Runtime.getRuntime().exec("ps ax");
				BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
				
				java.util.List<String> desktopList = Arrays.asList(DESKTOP_ENVIRONMENTS.split("\\|"));
				
				String line = r.readLine();
				while (line != null) {
					for (String s : desktopList)
						if (line.contains(s) && !line.contains("grep"))
							return desktopList.indexOf(s);
					
					line = r.readLine();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error: IO Exception during Runtime exec");
			}
		}
		return -1;
	}
	
	/*
	 * Parse XML file for some gnome sizes
	 */
	private static int getGnomeXML(File xmlFile) {
		try {	
			boolean found = false;
			FileReader reader = new FileReader(xmlFile);
			BufferedReader buffer = new BufferedReader(reader);
			String temp = buffer.readLine();
			while (temp != null) {
				if (temp.contains("<entry name=\"size\"")) {
					found = true; break;
				}
				temp = buffer.readLine();
			}
			buffer.close();	reader.close();
			if (found) {
				temp = temp.substring(temp.indexOf("value=\"") + 7);
				return Integer.parseInt(temp.substring(0, temp.indexOf("\">")));
			}
		}
		catch (Exception e) {}
		return -1;
	}
	
	private static int getKdeINI(String category, String component) {
		try {	
			File f = new File(KDE_CONFIG);
			if (!f.exists() || category == null || component == null) return -1;
			
			boolean found = false;
			FileReader reader = new FileReader(f);
			BufferedReader buffer = new BufferedReader(reader);
			String value = null;
			String temp = buffer.readLine();
			while (temp != null) {
				if (temp.trim().equals("[" + category + "]")) {
					temp = buffer.readLine();
					while (temp != null) {
						if (temp.trim().startsWith("["))
							return -1;
						else if (temp.startsWith(component + "=")) {
							value = temp.substring(component.length() + 1);
							found = true;
							break;
						}
						temp = buffer.readLine();	
					}
				}
				if (found == true) break;
				temp = buffer.readLine();
			}
			buffer.close();	reader.close();
			if (found)
				return Integer.parseInt(value);
		}
		catch (Exception e) {}
		return -1;
	}
}

