package msn2j;

import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.UIManager;

public class Main {
    
    public Main() {
    }

    public static void main(String[] args) {
        
        try {
            // Set System L&F
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } 
        catch (UnsupportedLookAndFeelException e) {
           // handle exception
        }
        catch (ClassNotFoundException e) {
           // handle exception
        }
        catch (InstantiationException e) {
           // handle exception
        }
        catch (IllegalAccessException e) {
           // handle exception
        }
         
        byte[] bArray = new byte[]{95, 1, 0, 0};
        System.out.println(messengerconnector.LittleEndian.LEDByteArrayToBigInteger(bArray).toString());
        
        new msn2j.gui.LoginWindow().setVisible(true);
    }
    
}
