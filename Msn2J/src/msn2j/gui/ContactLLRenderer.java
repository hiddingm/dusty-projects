package msn2j.gui;

import java.awt.*;
import javax.swing.tree.*;
import javax.swing.*;
import messengerconnector.Contact;

public class ContactLLRenderer implements ListCellRenderer {
    
    protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
    
    private Icon online = createImageIcon("images/online.gif");
    private Icon busy = createImageIcon("images/busy.gif");
    private Icon berightback = createImageIcon("images/away.gif");
    private Icon away = createImageIcon("images/away.gif");
    private Icon onthephone = createImageIcon("images/onthephone.gif");
    private Icon outtolunch = createImageIcon("images/outtolunch.gif");
    private Icon offline = createImageIcon("images/offline.gif");
    private ImageIcon block = createImageIcon("images/block.gif");
    private Icon onlineB = ImageOverlap.returnOverlapImageIcon(createImageIcon("images/online.gif"), block, 9, 9, 16, 16);
    private Icon busyB = ImageOverlap.returnOverlapImageIcon(createImageIcon("images/busy.gif"), block, 9, 9, 16, 16);
    private Icon berightbackB = ImageOverlap.returnOverlapImageIcon(createImageIcon("images/away.gif"), block, 9, 9, 16, 16);
    private Icon awayB = ImageOverlap.returnOverlapImageIcon(createImageIcon("images/away.gif"), block, 9, 9, 16, 16);
    private Icon onthephoneB = ImageOverlap.returnOverlapImageIcon(createImageIcon("images/onthephone.gif"), block, 9, 9, 16, 16);
    private Icon outtolunchB = ImageOverlap.returnOverlapImageIcon(createImageIcon("images/outtolunch.gif"), block, 9, 9, 16, 16);
    private Icon offlineB = ImageOverlap.returnOverlapImageIcon(createImageIcon("images/offline.gif"), block, 9, 9, 16, 16);
    
    public ContactLLRenderer() {
    }
    
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        Font mainFont = new Font("Verdana", Font.PLAIN, 10);
        Font groupFont = new Font("Verdana", Font.BOLD, 12);
        if (value instanceof Contact) {
            renderer.setFont(mainFont);
            Contact contact = (Contact)value;
            if ((Integer.parseInt(contact.getLists()) & 4) == 0) {
                if(contact.getStatus().equals("NLN"))
                    renderer.setIcon(online);
                else if(contact.getStatus().equals("BSY"))
                    renderer.setIcon(busy);
                else if(contact.getStatus().equals("BRB"))
                    renderer.setIcon(berightback);
                else if(contact.getStatus().equals("AWY"))
                    renderer.setIcon(away);
                else if(contact.getStatus().equals("IDL"))
                    renderer.setIcon(away);
                else if(contact.getStatus().equals("PHN"))  
                    renderer.setIcon(onthephone);
                else if(contact.getStatus().equals("LUN"))
                    renderer.setIcon(outtolunch);
                else
                    renderer.setIcon(offline);
            } else {
                if(contact.getStatus().equals("NLN"))
                    renderer.setIcon(onlineB);
                else if(contact.getStatus().equals("BSY"))
                    renderer.setIcon(busyB);
                else if(contact.getStatus().equals("BRB"))
                    renderer.setIcon(berightbackB);
                else if(contact.getStatus().equals("AWY"))
                    renderer.setIcon(awayB);
                else if(contact.getStatus().equals("IDL"))
                    renderer.setIcon(awayB);
                else if(contact.getStatus().equals("PHN"))  
                    renderer.setIcon(onthephoneB);
                else if(contact.getStatus().equals("LUN"))
                    renderer.setIcon(outtolunchB);
                else
                    renderer.setIcon(offlineB);
                renderer.setForeground(Color.RED);
            }
        } else {
            renderer.setForeground(new Color(51, 51, 255));
            renderer.setFont(groupFont);
        }
        return renderer;
    }
    
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = ContactLLRenderer.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
    
}
