package msn2j.gui;

import messengerconnector.Contact;
import javax.swing.DefaultListModel;

public class RemContactFrame extends javax.swing.JFrame {
    
    private MainFrame cList = null;
    private DefaultListModel listModel = new DefaultListModel();
    
    public RemContactFrame(MainFrame cList) {
        initComponents();
        this.cList = cList;
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        //Initialize Combo Box
        for (int i = 1; i < cList.contactList.contactNumbers; i++) {
            listModel.addElement(cList.contactList.contacts[i]);
        }
        delContactList.setModel(listModel);
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        delContactList = new javax.swing.JList();
        remButton = new javax.swing.JButton();
        blockBox = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jLabel1.setText("Remove a contact");

        jScrollPane1.setViewportView(delContactList);

        remButton.setText("Remove");
        remButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remButtonPressed(evt);
            }
        });

        blockBox.setText("Block");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(remButton)
                        .addContainerGap(61, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(blockBox, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGap(135, 135, 135)
                .addComponent(jLabel1)
                .addContainerGap(136, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(remButton)
                        .addContainerGap())
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(blockBox)
                .addContainerGap(47, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void remButtonPressed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remButtonPressed
        Contact selectedContact = (Contact)delContactList.getSelectedValue();
        String[] cGroups = selectedContact.getGroups();
        String groupString = "";
        for (int i = 0; i < cGroups.length; i++) {
            cList.notificationserver.removeContactFromForwardList(selectedContact.getEmail(), cGroups[i]);
        }
        if (blockBox.isSelected()) {
            cList.notificationserver.removeContactFromAllowList(selectedContact.getEmail());
            cList.notificationserver.addContactToBlockList(selectedContact.getEmail(), selectedContact.getEmail());
        }
        
        dispose();
    }//GEN-LAST:event_remButtonPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox blockBox;
    private javax.swing.JList delContactList;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton remButton;
    // End of variables declaration//GEN-END:variables
    
}
