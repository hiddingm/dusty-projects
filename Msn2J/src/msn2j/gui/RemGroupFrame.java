package msn2j.gui;

import messengerconnector.Contact;
import messengerconnector.Group;
import javax.swing.DefaultListModel;

public class RemGroupFrame extends javax.swing.JFrame {
    
    private MainFrame cList = null;
    private DefaultListModel listModel = new DefaultListModel();
    
    public RemGroupFrame(MainFrame cList) {
        initComponents();
        this.cList = cList;
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        System.out.println(cList.contactList.contacts[1].getGroups()[0]);

        //Initialize Combo Box
        boolean cantAdd = false;
        for (int i = 1; i < cList.contactList.groupNumbers; i++) {
            Contact[] contacts = cList.contactList.contacts;
            for (int y = 0; y < cList.contactList.contactNumbers; y++) {
                String groups[] = contacts[y].getGroups();
                for (int k = 0; k < groups.length; k++) {
                    if (groups[k].equals(Integer.toString(i)))
                        cantAdd = true;
                }
            }
            if (cantAdd == false)
                listModel.addElement(cList.contactList.groups[i]);
            cantAdd = false;
        }
        delGroupList.setModel(listModel);
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        delGroupList = new javax.swing.JList();
        remButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jLabel1.setText("Remove a group");

        jScrollPane1.setViewportView(delGroupList);

        remButton.setText("Remove");
        remButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remButtonPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(remButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(135, 135, 135)
                        .addComponent(jLabel1)))
                .addContainerGap(36, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(75, Short.MAX_VALUE)
                .addComponent(remButton)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void remButtonPressed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remButtonPressed
        cList.notificationserver.removeGroup(Integer.toString(delGroupList.getSelectedIndex() + 1));
        
        dispose();
    }//GEN-LAST:event_remButtonPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList delGroupList;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton remButton;
    // End of variables declaration//GEN-END:variables
    
}
