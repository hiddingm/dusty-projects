package msn2j.gui;

import java.awt.Color;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.swing.text.StyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.BadLocationException;
import messengerconnector.Contact;
import messengerconnector.ChatListener;
import messengerconnector.SwitchboardHandler;
import messengerconnector.stringeditor.Slicer;

public class ChatWindow extends javax.swing.JFrame implements ChatListener {

    private ContactChatWindow ccw = null;
    private Contact[] participants = null;
    private SwitchboardHandler swl = null;
    private StyledDocument doc = null;
    private boolean hasConnection = false;
    private boolean waitingToSay = false;
    
    public ChatWindow(ContactChatWindow ccw, Contact[] participants, SwitchboardHandler swl) {
        initComponents();
        this.ccw = ccw;
        this.participants = participants;
        System.out.println("Participants 0: " + participants[0]);
        this.swl = swl;
        hasConnection = true;
        participantLabel.setText("To: " + participants[0].getDisplayName() + " <" + participants[0].getEmail() + ">");
        
        //StyleConstants for color in the chatArea
        doc = chatArea.getStyledDocument();
        
        Style red = chatArea.addStyle("Red", null);
        StyleConstants.setForeground(red, Color.red);
        StyleConstants.setBold(red, true);
        
        Style blue = chatArea.addStyle("Blue", null);
        StyleConstants.setForeground(blue, Color.blue);
        StyleConstants.setBold(blue, true);
        
        Style orange = chatArea.addStyle("Orange", null);
        StyleConstants.setForeground(orange, Color.orange);
        StyleConstants.setBold(orange, true);
    }
    
    public void reloadWindow(Contact[] participants, SwitchboardHandler swl) {
        this.ccw = ccw;
        this.participants = participants;
        this.swl = swl;
    }
    
    public void contactJoin(String email) {
        hasConnection = true;
        if (waitingToSay) {
            String sendString = new String();

            sendString += "MIME-Version: 1.0\r\n";
            sendString += "Content-Type: text/plain; charset=UTF-8\r\n";
            sendString += "X-MMS-IM-Format: FN=Verdana; EF=; CO=006100; CS=0; PF=0\r\n";
            sendString += "\r\n";
            sendString += sayArea.getText();

            swl.sayText(sendString);


            // Makes text red
            Style style = chatArea.addStyle("Red", null);
            StyleConstants.setForeground(style, Color.red);
            StyleConstants.setBold(style, true);

            try {
                doc.insertString(doc.getLength(), ccw.contactList.contacts[0].getDisplayName() + ":\r\n" + sayArea.getText() + "\r\n", null);
                doc.setCharacterAttributes(doc.getLength() - (ccw.contactList.contacts[0].getDisplayName() + ":\r\n" + sayArea.getText() + "\r\n").length(), (ccw.contactList.contacts[0].getDisplayName() + ":\r\n").length(), chatArea.getStyle("Blue"), true);
                chatArea.setCaretPosition(chatArea.getDocument().getLength());
            } catch (BadLocationException ble) { }
            sayArea.setText("");
            waitingToSay = false;
        }
    }
    
    public void messageReceived(String command, String content) {
        String[] commands = Slicer.sliceInPieces(command, " ");
        if (content.indexOf("text/plain") != -1) {
            try {
                doc.insertString(doc.getLength(), URLDecoder.decode(commands[2], "UTF-8") + ":\r\n" + content.substring(content.indexOf("\r\n\r\n") + 4) + "\r\n", null);
                doc.setCharacterAttributes(doc.getLength() - (URLDecoder.decode(commands[2], "UTF-8") + ":\r\n" + content.substring(content.indexOf("\r\n\r\n") + 4) + "\r\n").length(), (URLDecoder.decode(commands[2], "UTF-8") + ":").length(), chatArea.getStyle("Red"), true);
                chatArea.setCaretPosition(chatArea.getDocument().getLength());
            } catch (BadLocationException ble) { 
            } catch (UnsupportedEncodingException uee) { }
        }
    }
    
    public void participantOffline(String email, boolean idle) {
        if (idle == true) {
            try {
                doc.insertString(doc.getLength(), "The conversation has been closed, due to no activity\r\n", null);
                doc.setCharacterAttributes(doc.getLength() - "The conversation has been closed, due to no activity\r\n".length(), "The conversation has been closed, due to no activity\r\n".length(), chatArea.getStyle("Orange"), true);
            } catch (BadLocationException ble) {}
        } else {
            try {
                doc.insertString(doc.getLength(), email + " has left the conversation\r\n", null);
                doc.setCharacterAttributes(doc.getLength() - (email + " has left the conversation\r\n").length(), (email + " has left the conversation\r\n").length(), chatArea.getStyle("Orange"), true);
            } catch (BadLocationException ble) {}
        }
        hasConnection = false;
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        chatArea = new javax.swing.JTextPane();
        jPanel1 = new javax.swing.JPanel();
        sendButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        sayArea = new javax.swing.JTextPane();
        participantLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jSplitPane1.setDividerLocation(250);
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setResizeWeight(0.2);

        chatArea.setEditable(false);
        jScrollPane1.setViewportView(chatArea);

        jSplitPane1.setTopComponent(jScrollPane1);

        sendButton.setText("Send");
        sendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendMessage(evt);
            }
        });

        jScrollPane2.setViewportView(sayArea);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sendButton, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(26, Short.MAX_VALUE)
                .addComponent(sendButton)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        jSplitPane1.setRightComponent(jPanel1);

        participantLabel.setText("To: ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(participantLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 508, Short.MAX_VALUE)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 508, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(participantLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sendMessage(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendMessage
        if (hasConnection) {
            String sendString = new String();

            sendString += "MIME-Version: 1.0\r\n";
            sendString += "Content-Type: text/plain; charset=UTF-8\r\n";
            sendString += "X-MMS-IM-Format: FN=Verdana; EF=; CO=006100; CS=0; PF=0\r\n";
            sendString += "\r\n";
            sendString += sayArea.getText();

            swl.sayText(sendString);


            // Makes text red
            Style style = chatArea.addStyle("Red", null);
            StyleConstants.setForeground(style, Color.red);
            StyleConstants.setBold(style, true);

            try {
                doc.insertString(doc.getLength(), ccw.contactList.contacts[0].getDisplayName() + ":\r\n" + sayArea.getText() + "\r\n", null);
                doc.setCharacterAttributes(doc.getLength() - (ccw.contactList.contacts[0].getDisplayName() + ":\r\n" + sayArea.getText() + "\r\n").length(), (ccw.contactList.contacts[0].getDisplayName() + ":\r\n").length(), chatArea.getStyle("Blue"), true);
                chatArea.setCaretPosition(chatArea.getDocument().getLength());
            } catch (BadLocationException ble) { }
            sayArea.setText("");
        } else {
            ccw.notificationserver.openConversation(participants[0]);
            waitingToSay = true;
        }
    }//GEN-LAST:event_sendMessage

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        swl.closeConnection();
        ccw.chatWindowHasClosed();
        ccw.setMyEmail("");
        ccw = null;
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextPane chatArea;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JLabel participantLabel;
    private javax.swing.JTextPane sayArea;
    private javax.swing.JButton sendButton;
    // End of variables declaration//GEN-END:variables
    
}
