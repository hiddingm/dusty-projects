package msn2j.gui;

import messengerconnector.ContactList;
import messengerconnector.NotificationserverHandler;
import javax.swing.JDialog;

public class AddContactDialog extends JDialog {

    public String email;
    public NotificationserverHandler notificationserver;
    public ContactList contactList;
    
    public AddContactDialog(String email, NotificationserverHandler notificationserver, ContactList contactList) {
        this.notificationserver = notificationserver;
        this.contactList = contactList;
        this.email = email;
        add(new AddContactDialogPanel(this));
        pack();
    }
    
}
