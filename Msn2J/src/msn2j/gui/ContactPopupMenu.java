package msn2j.gui;

import messengerconnector.Contact;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;

public class ContactPopupMenu extends JPopupMenu {
    
    public ContactPopupMenu(MainFrame cList, Contact contact, int selectedIndex) {
        ContactPopupMenuActionListener listener = new ContactPopupMenuActionListener(cList, contact, selectedIndex);
        
        JMenuItem menuItem = new JMenuItem("Send A Message");
        menuItem.setActionCommand("sendmsg");
        menuItem.addActionListener(listener);
        add(menuItem);
        addSeparator();
        menuItem = new JMenuItem("View Profile");
        menuItem.setActionCommand("vprof");
        menuItem.addActionListener(listener);
        add(menuItem);
        addSeparator();
        if ((Integer.parseInt(contact.getLists()) & 4) == 0) {
            menuItem = new JMenuItem("Block Contact");
            menuItem.setActionCommand("blck");
            menuItem.addActionListener(listener);
        } else {
            menuItem = new JMenuItem("Unblock Contact");
            menuItem.setActionCommand("unblck");
            menuItem.addActionListener(listener);
        }
        add(menuItem);
        menuItem = new JMenuItem("Delete Contact");
        menuItem.setActionCommand("dl");
        menuItem.addActionListener(listener);
        add(menuItem);
    }
    
}
