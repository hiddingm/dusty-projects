package msn2j.gui;

import messengerconnector.Contact;
import messengerconnector.ContactList;
import messengerconnector.NotificationserverHandler;
import messengerconnector.SwitchboardHandler;

public class ContactChatWindow {
    
    private ChatWindow myChatWindow;
    private String myEmail;
    public ContactList contactList = null;
    public NotificationserverHandler notificationserver = null;
    
    public ContactChatWindow(String email, ContactList contactList, NotificationserverHandler notificationserver) {
        myEmail = email;
        this.contactList = contactList;
        this.notificationserver = notificationserver;
    }
    
    public void newChatWindow(Contact[] participants, SwitchboardHandler swl) {
        if (myChatWindow == null) {
            myChatWindow = new ChatWindow(this, participants, swl);
            swl.addChatListener(myChatWindow);
            myChatWindow.setVisible(true);
        } else {
            swl.addChatListener(myChatWindow);
            myChatWindow.reloadWindow(participants, swl);
            myChatWindow.setVisible(true);
        }
    }
    
    public boolean hasChatWindow() {
        if (myChatWindow == null)
            return false;
        else 
            return true;
    }
    
    public String getMyEmail() {
        return myEmail;
    }
    
    public void setMyEmail(String email) {
        myEmail = email;
    }
    
    public void chatWindowHasClosed() {
        myChatWindow = null;
    }
    
}
