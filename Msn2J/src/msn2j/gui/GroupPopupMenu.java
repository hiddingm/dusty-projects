package msn2j.gui;

import messengerconnector.Group;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;

public class GroupPopupMenu  extends JPopupMenu {

    public GroupPopupMenu(MainFrame cList, Group selectedGroup) {
        GroupPopupMenuActionListener listener = new GroupPopupMenuActionListener(cList, selectedGroup);

        JMenuItem menuItem = new JMenuItem("Rename Group");
        menuItem.setActionCommand("rngroup");
        menuItem.addActionListener(listener);
        add(menuItem);
        
        menuItem = new JMenuItem("Remove Group");
        menuItem.setActionCommand("rmgroup");
        menuItem.addActionListener(listener);
        add(menuItem);
    }
    
}
