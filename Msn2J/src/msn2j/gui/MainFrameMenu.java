package msn2j.gui;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MainFrameMenu extends JMenuBar {
    
    private MainFrame cList = null;
    
    public MainFrameMenu(MainFrame cList) {
        this.cList = cList;
        
        JMenu menu;
        JMenuItem menuItem;
        JMenu subMenu;
        MainFrameMenuActionListener clmal = new MainFrameMenuActionListener(cList);
        
        menu = new JMenu("Main");
        add(menu);
        
        menuItem = new JMenuItem("Log out");
        menuItem.setActionCommand("logout");
        menuItem.addActionListener(clmal);
        menu.add(menuItem);
        
        menu.addSeparator();
        
        subMenu = new JMenu("Change Status");
        menuItem = new JMenuItem("Online");
        menuItem.setActionCommand("chnnln");
        menuItem.addActionListener(clmal);
        subMenu.add(menuItem);
        menuItem = new JMenuItem("Busy");
        menuItem.setActionCommand("chnbsy");
        menuItem.addActionListener(clmal);
        subMenu.add(menuItem);
        menuItem = new JMenuItem("Be Right Back");
        menuItem.setActionCommand("chnbrb");
        menuItem.addActionListener(clmal);
        subMenu.add(menuItem);
        menuItem = new JMenuItem("Away");
        menuItem.setActionCommand("chnawy");
        menuItem.addActionListener(clmal);
        subMenu.add(menuItem);
        menuItem = new JMenuItem("On the Phone");
        menuItem.setActionCommand("chnphn");
        menuItem.addActionListener(clmal);
        subMenu.add(menuItem);
        menuItem = new JMenuItem("Out to Lunch");
        menuItem.setActionCommand("chnlun");
        menuItem.addActionListener(clmal);
        subMenu.add(menuItem);
        menuItem = new JMenuItem("Hidden");
        menuItem.setActionCommand("chnhdn");
        menuItem.addActionListener(clmal);
        subMenu.add(menuItem);
        menu.add(subMenu);
        
        menuItem = new JMenuItem("Change name");
        menuItem.setActionCommand("chnname");
        menuItem.addActionListener(clmal);
        menu.add(menuItem);
        
        menu.addSeparator();
        
        menuItem = new JMenuItem("Quit");
        menuItem.setActionCommand("quit");
        menuItem.addActionListener(clmal);
        menu.add(menuItem);
        
        menu = new JMenu("Contacts");
        add(menu);

        menuItem = new JMenuItem("Add a contact");
        menuItem.setActionCommand("addcontact");
        menuItem.addActionListener(clmal);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Remove a contact");
        menuItem.setActionCommand("remcontact");
        menuItem.addActionListener(clmal);
        menu.add(menuItem);
        
        add(menu);
        
        menu = new JMenu("Groups");
        
        menuItem = new JMenuItem("Add a group");
        menuItem.setActionCommand("addgroup");
        menuItem.addActionListener(clmal);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Remove a group");
        menuItem.setActionCommand("remgroup");
        menuItem.addActionListener(clmal);
        menu.add(menuItem);
        
        add(menu);
    }
    
}
