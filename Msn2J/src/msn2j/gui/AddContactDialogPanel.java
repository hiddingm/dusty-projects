package msn2j.gui;

import javax.swing.ButtonGroup;

public class AddContactDialogPanel extends javax.swing.JPanel {
    
    private AddContactDialog acd = null;
    
    /** Creates new form AddContactDialogPanel */
    public AddContactDialogPanel(AddContactDialog acd) {
        initComponents();
        ButtonGroup group = new ButtonGroup();
        group.add(allowButton);
        group.add(blockButton);
        this.acd = acd;
        //Initialize Combo Box
        for (int i = 0; i < acd.contactList.groupNumbers; i++) {
            groupComboBox.addItem(acd.contactList.groups[i].getGroupName());
        }
        topLabel.setText(acd.email + " has added you to his/her contactlist.");
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        topLabel = new javax.swing.JLabel();
        addToCListChkBox = new javax.swing.JCheckBox();
        allowButton = new javax.swing.JRadioButton();
        blockButton = new javax.swing.JRadioButton();
        jButton1 = new javax.swing.JButton();
        groupComboBox = new javax.swing.JComboBox();

        topLabel.setFont(new java.awt.Font("Verdana", 0, 10));
        topLabel.setText("email has added you to his/her contactlist.");

        addToCListChkBox.setFont(new java.awt.Font("Verdana", 0, 10));
        addToCListChkBox.setSelected(true);
        addToCListChkBox.setText("Add to my contactlist");
        addToCListChkBox.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                addToCListChkBoxPropertyChange(evt);
            }
        });

        allowButton.setFont(new java.awt.Font("Verdana", 0, 10));
        allowButton.setSelected(true);
        allowButton.setText("Allow to start a conversation, and see your status");

        blockButton.setFont(new java.awt.Font("Verdana", 0, 10));
        blockButton.setText("Block to start a conversation, or see your status");

        jButton1.setFont(new java.awt.Font("Verdana", 0, 10));
        jButton1.setText("Ok");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okPressed(evt);
            }
        });

        groupComboBox.setFont(new java.awt.Font("Verdana", 0, 10));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(addToCListChkBox)
                            .addComponent(blockButton)
                            .addComponent(allowButton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton1)
                            .addComponent(groupComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(topLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(topLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addToCListChkBox)
                    .addComponent(groupComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(allowButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(blockButton)
                    .addComponent(jButton1))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void okPressed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okPressed
        System.out.println(acd.email);
        if (allowButton.isSelected()) {
            acd.notificationserver.removeContactFromBlockList(acd.email);
            acd.notificationserver.addContactToAllowList(acd.email, acd.email);
        } else {
            acd.notificationserver.removeContactFromAllowList(acd.email);
            acd.notificationserver.addContactToBlockList(acd.email, acd.email);
        }
        if (addToCListChkBox.isSelected()) {
            acd.notificationserver.addContactToForwardList(acd.email, acd.email, Integer.toString(groupComboBox.getSelectedIndex()));
        }
        acd.dispose();
    }//GEN-LAST:event_okPressed

    private void addToCListChkBoxPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_addToCListChkBoxPropertyChange
        if (addToCListChkBox.isSelected()) {
            groupComboBox.setEnabled(true);
        } else {
            groupComboBox.setEnabled(false);
        }
    }//GEN-LAST:event_addToCListChkBoxPropertyChange
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox addToCListChkBox;
    private javax.swing.JRadioButton allowButton;
    private javax.swing.JRadioButton blockButton;
    private javax.swing.JComboBox groupComboBox;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel topLabel;
    // End of variables declaration//GEN-END:variables
    
}
