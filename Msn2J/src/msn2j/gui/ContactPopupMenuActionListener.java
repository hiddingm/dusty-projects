package msn2j.gui;

import messengerconnector.Contact;
import messengerconnector.Group;
import java.awt.Desktop;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.net.URI;
import javax.swing.JOptionPane;

public class ContactPopupMenuActionListener implements ActionListener {
    
    private MainFrame cList;
    private Contact contact = null;
    private int selectedIndex;
    
    public ContactPopupMenuActionListener(MainFrame cList, Contact contact, int selectedIndex) {
        this.cList = cList;
        this.contact = contact;
        this.selectedIndex = selectedIndex;
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("sendmsg")) {
            cList.openConversation(contact);
        } else if (e.getActionCommand().equals("dl")) {
            //Custom button text
            Object[] options = {"Delete", "Delete And Block", "Cancel"};
            int n = JOptionPane.showOptionDialog(cList, "Do you really want to delete this contact?", "", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
            if (n == 0) {
                String groupNum = "";
                for (int i = 0; i < selectedIndex; i++) {
                    if (cList.contactListList.getModel().getElementAt(i) instanceof Group) {
                        Group cGroup = (Group)cList.contactListList.getModel().getElementAt(i);
                        groupNum = cGroup.getGroupID();
                    }
                }
                cList.notificationserver.removeContactFromForwardList(contact.getEmail(), groupNum);
            } else if (n == 1) {
                String groupNum = "";
                for (int i = 0; i < selectedIndex; i++) {
                    if (cList.contactListList.getModel().getElementAt(i) instanceof Group) {
                        Group cGroup = (Group)cList.contactListList.getModel().getElementAt(i);
                        groupNum = cGroup.getGroupID();
                    }
                }
                cList.notificationserver.removeContactFromForwardList(contact.getEmail(), groupNum);
                cList.notificationserver.removeContactFromAllowList(contact.getEmail());
                cList.notificationserver.addContactToBlockList(contact.getEmail(), contact.getDisplayName());
            }
        } else if (e.getActionCommand().equals("blck")) {
            cList.notificationserver.removeContactFromAllowList(contact.getEmail());
            cList.notificationserver.addContactToBlockList(contact.getEmail(), contact.getDisplayName());
        } else if (e.getActionCommand().equals("unblck")) {
            cList.notificationserver.removeContactFromBlockList(contact.getEmail());
            cList.notificationserver.addContactToAllowList(contact.getEmail(), contact.getDisplayName());
        } else if (e.getActionCommand().equals("sendmsg")) {
            cList.openConversation(contact);
        } else if (e.getActionCommand().equals("vprof")) {
            if (Desktop.isDesktopSupported()) {
                Desktop desktop = Desktop.getDesktop();
                if (desktop.isSupported(Desktop.Action.BROWSE)) {
                    try {
                        desktop.browse(new URI("http://members.msn.com/default.msnw?mem=" + contact.getEmail()));
                    } catch (Exception ex) { }
                }
            }
        }
    }
    
}
