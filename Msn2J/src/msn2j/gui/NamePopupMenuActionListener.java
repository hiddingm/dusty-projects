package msn2j.gui;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NamePopupMenuActionListener implements ActionListener {

    private MainFrame cList;
    
    public NamePopupMenuActionListener(MainFrame cList) {
        this.cList = cList;
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("chndn"))
            new ChangeNameFrame(cList).setVisible(true);
        if (e.getActionCommand().equals("chnnln"))
            cList.changeStatus("NLN");
        if (e.getActionCommand().equals("chnbsy"))
            cList.changeStatus("BSY");
        if (e.getActionCommand().equals("chnbrb"))
            cList.changeStatus("BRB");
        if (e.getActionCommand().equals("chnawy"))
            cList.changeStatus("AWY");
        if (e.getActionCommand().equals("chnphn"))
            cList.changeStatus("PHN");
        if (e.getActionCommand().equals("chnlun"))
            cList.changeStatus("LUN");
        if (e.getActionCommand().equals("chnhdn"))
            cList.changeStatus("HDN");
    }
    
}