package msn2j.gui;

import messengerconnector.Contact;
import messengerconnector.Group;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

public class GroupPopupMenuActionListener implements ActionListener {

    private MainFrame cList = null;
    private Group selectedGroup = null;
    
    public GroupPopupMenuActionListener(MainFrame cList, Group selectedGroup) {
        this.cList = cList;
        this.selectedGroup = selectedGroup;
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("rmgroup")) {
            int n = JOptionPane.showConfirmDialog(cList, "Really delete this group?", "Confirm delete", JOptionPane.YES_NO_OPTION);
            if (n == 0) {
                Contact[] contacts = cList.contactList.contacts;
                boolean cantRem = false;
                for (int y = 0; y < cList.contactList.contactNumbers; y++) {
                    String groups[] = contacts[y].getGroups();
                    for (int k = 0; k < groups.length; k++) {
                        if (groups[k].equals(selectedGroup.getGroupID()))
                            cantRem = true;
                    }
                }
                if (cantRem == false)
                    cList.notificationserver.removeGroup(selectedGroup.getGroupID());
                else
                    JOptionPane.showMessageDialog(cList,"Group not empty","Error",JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals("rngroup")) {
            
        }
    }
    
}