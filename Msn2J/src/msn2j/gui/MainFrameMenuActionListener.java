package msn2j.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrameMenuActionListener implements ActionListener {
    
    private MainFrame cList;
    
    public MainFrameMenuActionListener(MainFrame cList) {
        this.cList = cList;
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("logout"))
            cList.logout();
        if (e.getActionCommand().equals("quit"))
            cList.quitAndCloseConnection();
        if (e.getActionCommand().equals("chnnln"))
            cList.changeStatus("NLN");
        if (e.getActionCommand().equals("chnbsy"))
            cList.changeStatus("BSY");
        if (e.getActionCommand().equals("chnbrb"))
            cList.changeStatus("BRB");
        if (e.getActionCommand().equals("chnawy"))
            cList.changeStatus("AWY");
        if (e.getActionCommand().equals("chnphn"))
            cList.changeStatus("PHN");
        if (e.getActionCommand().equals("chnlun"))
            cList.changeStatus("LUN");
        if (e.getActionCommand().equals("chnhdn"))
            cList.changeStatus("HDN");
        if (e.getActionCommand().equals("chnname"))
            new ChangeNameFrame(cList).setVisible(true);
        if (e.getActionCommand().equals("quit"))
            cList.quitAndCloseConnection();
        if (e.getActionCommand().equals("addcontact"))
            new NewContactFrame(cList).setVisible(true);
        if (e.getActionCommand().equals("remcontact"))
            new RemContactFrame(cList).setVisible(true);
        if (e.getActionCommand().equals("addgroup"))
            new NewGroupFrame(cList).setVisible(true);
        if (e.getActionCommand().equals("remgroup"))
            new RemGroupFrame(cList).setVisible(true);
    }
    
}
