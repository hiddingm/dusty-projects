package msn2j.gui;

import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;

public class NamePopupMenu  extends JPopupMenu {

    public NamePopupMenu(MainFrame cList) {
        NamePopupMenuActionListener listener = new NamePopupMenuActionListener(cList);
        
        JMenuItem menuItem = new JMenuItem("Change Display Name");
        menuItem.setActionCommand("chndn");
        menuItem.addActionListener(listener);
        add(menuItem);
        
        addSeparator();
        
        menuItem = new JMenuItem("Online");
        menuItem.setActionCommand("chnnln");
        menuItem.addActionListener(listener);
        add(menuItem);
        
        menuItem = new JMenuItem("Busy");
        menuItem.setActionCommand("chnbsy");
        menuItem.addActionListener(listener);
        add(menuItem);
        
        menuItem = new JMenuItem("Be Right Back");
        menuItem.setActionCommand("chnbrb");
        menuItem.addActionListener(listener);
        add(menuItem);
        
        menuItem = new JMenuItem("Away");
        menuItem.setActionCommand("chnawy");
        menuItem.addActionListener(listener);
        add(menuItem);
        
        
        menuItem = new JMenuItem("On the Phone");
        menuItem.setActionCommand("chnphn");
        menuItem.addActionListener(listener);
        add(menuItem);
        
        menuItem = new JMenuItem("Out to Lunch");
        menuItem.setActionCommand("chnlun");
        menuItem.addActionListener(listener);
        add(menuItem);
        
        menuItem = new JMenuItem("Hidden");
        menuItem.setActionCommand("chnhdn");
        menuItem.addActionListener(listener);
        add(menuItem);
    }
    
}
