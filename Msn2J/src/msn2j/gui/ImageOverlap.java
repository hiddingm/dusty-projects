package msn2j.gui;

import java.awt.image.BufferedImage;
import java.awt.Graphics;
import javax.swing.ImageIcon;

public class ImageOverlap {
    
    public ImageOverlap() {
    }
    
    public static ImageIcon returnOverlapImageIcon(ImageIcon first, ImageIcon second, int x, int y, int width, int height) {
        ImageIcon overlap = new ImageIcon();
        overlap.setImage(new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB));
        Graphics og = overlap.getImage().getGraphics();
        og.drawImage(first.getImage(), 0, 0, null);
        og.drawImage(second.getImage(), x, y, null);
        return overlap;
    }
    
}
