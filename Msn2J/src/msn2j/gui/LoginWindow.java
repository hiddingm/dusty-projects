package msn2j.gui;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import javax.swing.JOptionPane;
import messengerconnector.LoginListener;
import messengerconnector.NotificationserverHandler;
import msn2j.popup.PopupMaker;

public class LoginWindow extends javax.swing.JFrame implements LoginListener {
    
    private NotificationserverHandler notificationserver;
    
    public LoginWindow() {
        initComponents();
    }
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        loginPicture = new msn2j.gui.LoginPicture();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        emailField = new javax.swing.JTextField();
        passField = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        statusCombo = new javax.swing.JComboBox();
        emailCombo = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        loginPicture.setPreferredSize(new java.awt.Dimension(96, 96));
        javax.swing.GroupLayout loginPictureLayout = new javax.swing.GroupLayout(loginPicture);
        loginPicture.setLayout(loginPictureLayout);
        loginPictureLayout.setHorizontalGroup(
            loginPictureLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 96, Short.MAX_VALUE)
        );
        loginPictureLayout.setVerticalGroup(
            loginPictureLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 96, Short.MAX_VALUE)
        );

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 12));
        jLabel1.setText("Email");

        jLabel2.setFont(new java.awt.Font("Verdana", 0, 12));
        jLabel2.setText("Password");

        emailField.setFont(new java.awt.Font("Verdana", 0, 10));

        passField.setFont(new java.awt.Font("Verdana", 0, 10));

        jLabel3.setFont(new java.awt.Font("Verdana", 0, 12));
        jLabel3.setText("Status");

        statusCombo.setFont(new java.awt.Font("Verdana", 0, 10));
        statusCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Online", "Busy", "Be Right Back", "Away", "On the Phone", "Out to Lunch", "Hidden" }));
        statusCombo.setActionCommand("statusboxchanged");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(passField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(statusCombo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(emailField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(15, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(emailField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(passField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        emailCombo.setActionCommand("emailboxchanged");
        emailCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                emailCombochangeEmailTextField(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Verdana", 0, 10));
        jButton1.setText("Add");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addEmailToCombo(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Verdana", 0, 10));
        jButton2.setText("Remove");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeEmailFromCombo(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Verdana", 0, 10));
        jButton3.setText("Login");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginButton(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(57, Short.MAX_VALUE)
                .addComponent(loginPicture, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(57, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(61, Short.MAX_VALUE)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(61, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(27, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(emailCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(loginPicture, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(emailCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void loginButton(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginButton
        if (emailCorrect(emailField.getText())) {
            notificationserver = new NotificationserverHandler();
            notificationserver.addLoginListener(this);
            notificationserver.login(emailField.getText(), passField.getPassword());
        } else {
            PopupMaker.newPopup("The given email is not correct");
        }
    }//GEN-LAST:event_loginButton

    private void removeEmailFromCombo(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeEmailFromCombo
        emailCombo.removeItem(emailCombo.getSelectedItem());
        saveCombo();
    }//GEN-LAST:event_removeEmailFromCombo

    private void addEmailToCombo(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addEmailToCombo
        if (emailField.getText() != null && emailCorrect(emailField.getText()) && notInCombo(emailField.getText())) {
            emailCombo.addItem(emailField.getText());
            emailCombo.setSelectedItem(emailField.getText());
        }
        saveCombo();
    }//GEN-LAST:event_addEmailToCombo

    private void emailCombochangeEmailTextField(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_emailCombochangeEmailTextField
        emailField.setText((String)evt.getItem());
    }//GEN-LAST:event_emailCombochangeEmailTextField

    private boolean emailCorrect(String email) {
        //Thank you: http://www.devx.com/tips/Tip/28334
        //Set the email pattern string
        Pattern p = Pattern.compile(".+@.+\\.[a-z]+");

        //Match the given string with the pattern
        Matcher m = p.matcher(email);

        //check whether match is found 
        boolean matchFound = m.matches();

        return matchFound;

    }
    
    private boolean notInCombo(String email) {
        boolean returnB = true;
        for (int i = 0; i < emailCombo.getItemCount(); i++) {
            if (emailCombo.getItemAt(i).equals(email))
                returnB = false;
        }
        return returnB;
    }
    
    private void saveCombo() {
        String printString = "";
        for (int i = 0; i < emailCombo.getItemCount(); i++) {
            printString += emailCombo.getItemAt(i);
            if (i < emailCombo.getItemCount())
                printString += ",";
        }
        PrintWriter outputStream = null;
        try {
            File file = new File("config/email.txt");
            // Create file if it does not exist
            file.createNewFile();
            outputStream = new PrintWriter(new FileWriter("config/email.txt"));
            outputStream.print(printString);
            outputStream.flush();
            
        } catch (IOException ioe) { 
        } finally {
            if (outputStream != null)
                outputStream.close();
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox emailCombo;
    private javax.swing.JTextField emailField;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private msn2j.gui.LoginPicture loginPicture;
    private javax.swing.JPasswordField passField;
    private javax.swing.JComboBox statusCombo;
    // End of variables declaration//GEN-END:variables
    
    
    //All functions off the LoginListener interface
    public void errorReceived(String errorNumber) {
        int errorNum = Integer.parseInt(errorNumber);
        if (errorNum == 500) {
            JOptionPane.showMessageDialog(null,"You have received error number 500.\nThis means the msn servers are down, or your account has been banned from the network.", "A problem has been received, while logging in", javax.swing.JOptionPane.ERROR_MESSAGE);
        } else if (errorNum == 601) {
            JOptionPane.showMessageDialog(null,"You have received error number 601.\nThis means the msn servers are temporary unavailable.", "A problem has been received, while logging in", javax.swing.JOptionPane.ERROR_MESSAGE);
        } else if (errorNum == 910) {
            JOptionPane.showMessageDialog(null,"You have received error number 910.\nThis means the msn servers are busy.", "A problem has been received, while logging in", javax.swing.JOptionPane.ERROR_MESSAGE);
        } else if (errorNum == 911) {
            JOptionPane.showMessageDialog(null,"You have received error number 911.\nThis means the account you have given is incorrect..", "A problem has been received, while logging in", javax.swing.JOptionPane.ERROR_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null,"You have received an unknown error number " + errorNum + "\nYou may find your problem here: http://www.hypothetic.org/docs/msn/reference/error_list.php", "A problem has been received, while logging in", javax.swing.JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
    public void exceptionThrowed(Exception ex) {
        JOptionPane.showMessageDialog(null,"Msn2J couldn't connect to the msn server.\nRun the program from the command line for more information. ", "A problem has been received, while logging in", javax.swing.JOptionPane.ERROR_MESSAGE);
    }
    
    public void protocolNotSupported(String msnProtocol) {
       JOptionPane.showMessageDialog(null,"This program is outdated, check for a newer version or stop using this msn client.", "Program can't continue", javax.swing.JOptionPane.ERROR_MESSAGE);
    }
    
    public void recommendedClientReceived(String url) {
        
    }
    
    public void successfulLogin() {
        String status = "";
        if (statusCombo.getSelectedItem().equals("Online"))
            status = "NLN";
        if (statusCombo.getSelectedItem().equals("Busy"))
            status = "BSY";
        if (statusCombo.getSelectedItem().equals("Be Right Back"))
            status = "BRB";
        if (statusCombo.getSelectedItem().equals("Away"))
            status = "AWY";
        if (statusCombo.getSelectedItem().equals("On the Phone"))
            status = "PHN";
        if (statusCombo.getSelectedItem().equals("Out to Lunch"))
            status = "LUN";
        if (statusCombo.getSelectedItem().equals("Hidden"))
            status = "HDN";
        MainFrame cll = new MainFrame(notificationserver, status);
        notificationserver.addContactListListener(cll);
        cll.setVisible(true);
        dispose();
    }
    
    public void wrongPassword() {
        JOptionPane.showMessageDialog(null,"The password you provided is incorrect.", "Wrong password!", javax.swing.JOptionPane.ERROR_MESSAGE);
    }
}
