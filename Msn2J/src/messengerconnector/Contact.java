package messengerconnector;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import messengerconnector.stringeditor.Slicer;

public class Contact {
    
    private String nickname;
    private String lists;
    private String email;
    private String status = "";
    private String groups[];
    
    public Contact(String email, String nickname, String lists, String groups) {
        try {
            this.nickname = URLDecoder.decode(nickname, "UTF-8");
        } catch (UnsupportedEncodingException uee) { }
        this.lists = lists;
        this.email = email;
        this.groups = Slicer.sliceInPieces(groups, ",");
    }
    
    public String getStatus() {
        return status;
    }
    
    public String getEmail() {
        return email;
    }
    
    public String getDisplayName() {
        return nickname;
    }
    
    public String[] getGroups() {
        return groups;
    }
    
    public String getLists() {
        return lists;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    public void setName(String newName) {
        try {
            nickname = URLDecoder.decode(newName, "UTF-8");
        } catch (UnsupportedEncodingException uee) { }
    }
    
    public void setLists(String lists) {
        this.lists = lists;
    }
    
    public void setGroups(String groups) {
        this.groups = Slicer.sliceInPieces(groups, ",");
    }
    
    public String toString() {
        return nickname;
    }
    
}
