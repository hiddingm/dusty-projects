package messengerconnector.stringeditor;

public class Slicer {
    
    public Slicer() {
    }
    
    public static String[] sliceInPieces(String inputString, String b) {
        
        String tempString = inputString;
        byte count;
        
        for (count = 1; tempString.indexOf(b) != -1; count++)
            tempString = tempString.substring(tempString.indexOf(b) + 1, tempString.length());
        
        String[] returnS;
        returnS = new String[count];
        tempString = inputString;
        count = 0;
        
        while (tempString.indexOf(b) != -1) {
            returnS[count] = tempString.substring(0, tempString.indexOf(b));
            tempString = tempString.substring(tempString.indexOf(b) + 1, tempString.length());
            count++;
        }
        
        returnS[count] = tempString;
        
        return returnS;
    }
    
}
