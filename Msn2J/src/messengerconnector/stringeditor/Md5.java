package messengerconnector.stringeditor;
import java.security.*;
import java.math.*;

public class Md5 {

    //Thank you: http://snippets.dzone.com/posts/show/3686
    
    public Md5() {
    }
    
    public static String getMD5(String s) {
        MessageDigest m = null;
        try {
            m=MessageDigest.getInstance("MD5");
        } catch (Exception e) { }
        m.update(s.getBytes(),0,s.length());
        return new BigInteger(1,m.digest()).toString(16);
    } 
    
}
