package messengerconnector;

public class ContactList {

    public Group[] groups;
    public byte groupNumbers = 0;
    public Contact[] contacts;
    public int contactNumbers = 0;
    
    public ContactList() {
        
    }
    
    public void addContact(String email, String nickname, String lists, String groups) {
        contacts[contactNumbers] = new Contact(email, nickname, lists, groups);
        contactNumbers++;
    }
    
    public void addGroup(String groupName, String groupID) {
        groups[groupNumbers] = new Group(groupName, groupID);
        groupNumbers++;
    }
    
}