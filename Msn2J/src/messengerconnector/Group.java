package messengerconnector;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class Group {

    private String groupName = null;
    private String groupID = null;
    
    public Group(String groupName, String groupID) {
        try {
            this.groupName = URLDecoder.decode(groupName, "UTF-8");
        } catch (UnsupportedEncodingException uee) { }
        this.groupID = groupID;
    }
    
    public String getGroupName() {
        return groupName;
    }
    
    public String getGroupID() {
        return groupID;
    }
    
    public String toString() {
        return groupName;
    }
    
}
