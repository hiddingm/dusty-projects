package messengerconnector;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.MalformedURLException;
import java.util.Timer;
import java.util.TimerTask;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import messengerconnector.stringeditor.Md5;
import messengerconnector.stringeditor.Slicer;

public class MessengerProtocol {
    
    //Variable declarations
    public static String MESSENGERPROTOCOLS = "MSNP9";
    private static String LOCALEID = "0x0409";
    private static String OSTYPE = "Java";
    private static String OSVERSION = System.getProperty("java.version");
    private static String ARCHITECTURE = System.getProperty("os.arch");
    private static String CLIENTNAME = "Msn2J";
    private static String CLIENTVERSION = "0.2";
    private static String EXECUTABLENAME = "0";
    private String accountEmail = null;
    private char[] accountPassword = null;
    public volatile long triggerID = 0;
    private Socket connection = null;
    private PrintWriter out = null;
    private BufferedReader in = null;
    private NotificationserverHandler notificationSH;
    private LoginListener loginInterface = null;
    public ContactListListener contactListInterface = null;
    public ContactList contactList = null;
    public Contact inviteContact = null;
    private int contactCount = 0;
    private int currentCC = 0;
    private int emailCount = 0;
    private Timer pingTimer = null;
    private String gtc, blp = null;

    public MessengerProtocol(NotificationserverHandler notificationSH) {
        this.notificationSH = notificationSH;
    }
    
    public void addLoginListener(LoginListener loginInterface) {
        this.loginInterface = loginInterface;
    }
    
    public void addConnection(Socket connection, PrintWriter out, BufferedReader in, String accountEmail, char[] accountPassword) {
        //Add sockets, input and output
        this.connection = connection;
        this.out = out;
        this.in = in;
        //Add the email and password
        this.accountEmail = accountEmail;
        this.accountPassword = accountPassword;
    }
    
    public void addContactListListener(ContactListListener contactListInterface) {
        this.contactListInterface = contactListInterface;
    }
    
    private void closeConnectionAndNotifyCLL(String error) {
        if (loginInterface != null)
                loginInterface.errorReceived(error);
            try {
                out.close();                
                in.close();
                connection.close();
        } catch (IOException ioe) { }
    }
    
    public void processData(String inputString) {
        //Slice the string, into individual commands
        String[] commands = Slicer.sliceInPieces(inputString, " ");
        
        //Check if the protocol is supported, this function isn't really necessary
        if (commands[0].equals("VER")) {
            
            String[] supportedProtocols = Slicer.sliceInPieces(MESSENGERPROTOCOLS, " ");
            supportedLoop:
            for(int i = 2; i < commands.length; i++) {
                for(int k = 0; k < supportedProtocols.length; k++) {
                    if(supportedProtocols[k].equals(commands[i])) {
                        out.print("CVR " + triggerID + " " + LOCALEID + " " + OSTYPE + " " + OSVERSION + " " + ARCHITECTURE + " " + CLIENTNAME + " " + CLIENTVERSION + " " + EXECUTABLENAME  + " " + accountEmail + "\r\n");
                        out.flush();
                        break supportedLoop;
                    }
                }
            }
            if (commands[2].equals("CVR0")) {
                if (loginInterface != null) {
                    loginInterface.protocolNotSupported(MESSENGERPROTOCOLS);
                }
            }
            triggerID++;
            
        } else if (commands[0].equals("CVR")) {
            
            //The response to the CVR command
            loginInterface.recommendedClientReceived(commands[5]);
            out.print("USR " + triggerID + " TWN I " + accountEmail + "\r\n");
            out.flush();
            triggerID++;
            
        } else if (commands[0].equals("XFR") && commands[2].equals("SB") != true) {
            
            //Connection is being transfered
            notificationSH.resetConnection(commands[3].substring(0, commands[3].indexOf(":")), Integer.parseInt(commands[3].substring(commands[3].indexOf(":") + 1)));
        
        } else if (commands[0].equals("XFR") && commands[2].equals("SB")) {
            
            //Connection information about the switchboard received
            //The invite email String, has been set by the NotificationserverHandler
            SwitchboardHandler switchboardH = new SwitchboardHandler(commands[3].substring(0, commands[3].indexOf(":")), Integer.parseInt(commands[3].substring(commands[3].indexOf(":") + 1)), contactListInterface);
            switchboardH.sendUSR(accountEmail, commands[5]);
            switchboardH.invite(inviteContact);
            triggerID++;
            inviteContact = null;
            
        } else if (commands[0].equals("GTC")) {
            gtc = commands[1];
        } else if (commands[0].equals("BLP")) {
            blp = commands[1];
        } else if (commands[0].equals("SYN")) {
            
            //The response to the syn command, commands[3] is the number of contacts, that will be send
            contactCount = Integer.parseInt(commands[3]);
            Contact me = contactList.contacts[0];
            contactList.contacts = new Contact[Integer.parseInt(commands[3]) + 1];
            contactList.contacts[0] = me;
            contactList.groups = new Group[Integer.parseInt(commands[4])];
            
        } else if (commands[0].equals("USR") && commands[2].equals("TWN")) {

            //Tweener authentication, to long to describe
            try {
               
                SSLSocketFactory sslFact = (SSLSocketFactory)SSLSocketFactory.getDefault();
                SSLSocket sslNexus = (SSLSocket)sslFact.createSocket("nexus.passport.com", 443);

                PrintWriter nexusOut = new PrintWriter(sslNexus.getOutputStream(), true);
                BufferedReader nexusIn = new BufferedReader(new InputStreamReader(sslNexus.getInputStream()));

                nexusOut.println("GET /rdr/pprdr.asp HTTP/1.0\r\n\r\n");

                String newConnectionString = null;
                
                while((inputString = nexusIn.readLine()) != null) {
                    if (inputString.indexOf("DALogin=") != -1) {
                        newConnectionString = inputString.substring(inputString.indexOf("DALogin=") + 8, inputString.indexOf("/login2.srf"));
                    }
                }
                    
                nexusOut.close();                
                nexusIn.close();
                sslNexus.close();
                                    
                sslNexus = (SSLSocket)sslFact.createSocket(newConnectionString, 443);
                nexusOut = new PrintWriter(sslNexus.getOutputStream(), true);
                nexusIn = new BufferedReader(new InputStreamReader(sslNexus.getInputStream()));

                String outString = "GET /login2.srf HTTP/1.1\r\n";
                outString += "Authorization: Passport1.4 OrgVerb=GET,OrgURL=http%3A%2F%2Fmessenger%2Emsn%2Ecom,sign-in=" + accountEmail + ",pwd=" + String.copyValueOf(accountPassword) + "," + commands[4];
                outString += "User-Agent: MSMSGS\r\n";
                outString += "Host: login.passport.com\r\n";
                outString += "Connection: Keep-Alive\r\n";
                outString += "Cache-Control: no-cache\r\n\r\n";
                
                nexusOut.print(outString);
                nexusOut.flush();
                
                while((inputString = nexusIn.readLine()) != null) {
                    if (inputString.indexOf("Location: ") != -1) {
                        newConnectionString = inputString.substring(inputString.indexOf("Location: ") + 10, inputString.indexOf(" \r\n"));
                    }
                    if (inputString.indexOf("Authentication") != -1) {
                        newConnectionString = inputString.substring(inputString.indexOf("'t=") + 1, inputString.indexOf("',ru="));
                        out.print("USR " + triggerID + " TWN S " + newConnectionString + "\r\n");
                        out.flush();
                        triggerID++;
                        System.out.println(newConnectionString);
                        break;
                    }
                    if (inputString.indexOf("HTTP/1.1 401 Unauthorized") != -1) {
                        notificationSH.closeConnection(0);
                        loginInterface.wrongPassword();
                    }
                    System.out.println(inputString);
                }
                
                

            } catch (MalformedURLException mue) { 
                System.out.println("Er heeft een MalformedURLException opgetreden");
            } catch (IOException ioe) { 
                System.out.println("Er heeft een IOException opgetreden");
            }
            
            accountPassword = null;
            
        } else if (commands[0].equals("CHL")) {
            
            //Create a response to the challenge string
            out.print("QRY " + triggerID + " PROD0076ENE8*@AW 32" + "\r\n");
            out.flush();
            out.print(Md5.getMD5(commands[2] + "CEQJ8}OE0!WTSWII"));
            out.flush();
            triggerID++;
            
        } else if (commands[0].equals("LSG")) {
            
            //Add a group to the contactlist
            contactList.addGroup(commands[2], commands[1]);
            contactListInterface.refreshContactList(contactList);
            
        } else if (commands[0].equals("LST")) {
            
            //Add a contact to the contactlist
            if (commands.length == 5)
                contactList.addContact(commands[1], commands[2], commands[3], commands[4]);
            else
                contactList.addContact(commands[1], commands[2], commands[3], "");  
            
            //Check if all contacts have been send
            currentCC++;
            if (currentCC == contactCount) {
                contactListInterface.refreshContactList(contactList);
                contactListInterface.clientSynchronised();
                contactCount = 0;
                currentCC = 0;
            }
            
            //Check if only on reverse list
            if (commands[3].equals("8")) {
                if (gtc.equals("A") || blp.equals("BL")) {
                    //Notify contactListInterface, you have been add!
                    contactListInterface.reverseListAdd(commands[1], commands[2]);
                } else {
                    //Add to allow list
                    notificationSH.addContactToAllowList(commands[1], commands[2]);
                }
            }
            
        } else if (commands[0].equals("USR") && commands[2].equals("OK")) {
            
            //If all went right
            if (loginInterface != null)
                loginInterface.successfulLogin();
            loginInterface = null;
            //Create the contactlist and add a new contact, you
            contactList = new ContactList();
            contactList.contacts = new Contact[1];
            contactList.addContact(commands[3], commands[4], "", "");
            //Tell the contactListInterface to refresh the contactList
            contactListInterface.refreshContactList(contactList);
            //Start sending pings to the server
            out.print("PNG\r\n");
            out.flush();
            
        } else if (commands[0].equals("ILN")) {
            
            //Someone was already online, before the client logged on
            for (int i = 0; i < contactList.contactNumbers; i++) {
                if (contactList.contacts[i].getEmail().equals(commands[3])) {
                    Contact contact = contactList.contacts[i];
                    contact.setName(commands[4]);
                    contact.setStatus(commands[2]);
                    contactListInterface.refreshContact(contact);
                }
            }
            
        } else if (commands[0].equals("NLN")) {
            
            //Someone changed his status
            for (int i = 0; i < contactList.contactNumbers; i++) {
                if (contactList.contacts[i].getEmail().equals(commands[2])) {
                    contactList.contacts[i].setStatus(commands[1]);
                    contactList.contacts[i].setName(commands[3]);
                    contactListInterface.refreshContact(contactList.contacts[i]);
                }
            } 
            
        } else if (commands[0].equals("FLN")) {
            
            //Someone logged off
            for (int i = 0; i < contactList.contactNumbers; i++) {
                if (contactList.contacts[i].getEmail().equals(commands[1])) {
                    contactList.contacts[i].setStatus("");
                    contactListInterface.refreshContact(contactList.contacts[i]);
                }
            } 
            
        } else if (commands[0].equals("RNG")) {
            
            //Client is being invited to a switchboard session
            String swissID = commands[1];
            String swissAuth = commands[4];
            SwitchboardHandler switchboardH = new SwitchboardHandler(commands[2].substring(0, commands[2].indexOf(":")), Integer.parseInt(commands[2].substring(commands[2].indexOf(":") + 1)), contactListInterface);
            switchboardH.answerInvitation(contactList.contacts[0].getEmail(), swissAuth, swissID, triggerID);
            triggerID++;
            
        } else if (commands[0].equals("MSG")) {
            
            char[] allReceived = new char[Integer.parseInt(commands[3])];
            try {
                in.read(allReceived, 0, Integer.parseInt(commands[3]));
            } catch (IOException ioe) { }
            String intoString = String.valueOf(allReceived);
            //Email notification handling will go here
            if (intoString.indexOf("Content-Type: text/x-msmsgsinitialemailnotification") != -1) {
                int somePlace, emailCountInbox = 0, emailCountFolders = 0;
                if ((somePlace = intoString.indexOf("Inbox-Unread:")) != -1) {
                    String intoIntoString = intoString.substring(somePlace + 14);
                    int indexOf = intoIntoString.indexOf("\r\n");
                    emailCountInbox = Integer.parseInt(intoIntoString.substring(0, indexOf));
                }
                if ((somePlace = intoString.indexOf("Folders-Unread:")) != -1) {
                    String intoIntoString = intoString.substring(somePlace + 16);
                    int indexOf = intoIntoString.indexOf("\r\n");
                    emailCountFolders = Integer.parseInt(intoIntoString.substring(0, indexOf));
                    emailCount = emailCountInbox + emailCountFolders;
                    contactListInterface.newEmail(emailCount);
                }
            }
            //Realtime email notifications
            if (intoString.indexOf("Content-Type: text/x-msmsgsemailnotification") != -1) {
                String fromAdress, intoIntoString;
                int somePlace;
                //Receive the sender's email
                somePlace = intoString.indexOf("From-Addr:");
                intoIntoString = intoString.substring(somePlace + 11);
                int indexOf = intoIntoString.indexOf("\r\n");
                fromAdress = intoIntoString.substring(0, indexOf);
                //Notify the contactListInterface
                contactListInterface.emailReceived(fromAdress);
                contactListInterface.changeEmailCount(++emailCount);
            }
            //Email is being removed
            if (intoString.indexOf("Content-Type: text/x-msmsgsactivemailnotification") != -1) {
                String sourceFolder, destFolder, intoIntoString;
                int somePlace, mDelta;
                //Receive the source folder
                somePlace = intoString.indexOf("Src-Folder:");
                intoIntoString = intoString.substring(somePlace + 12);
                int indexOf = intoIntoString.indexOf("\r\n");
                sourceFolder = intoIntoString.substring(0, indexOf);
                //Receive the Dest-Folder
                somePlace = intoString.indexOf("Dest-Folder:");
                intoIntoString = intoString.substring(somePlace + 13);
                indexOf = intoIntoString.indexOf("\r\n");
                destFolder = intoIntoString.substring(0, indexOf);
                //Receive the message delta field
                somePlace = intoString.indexOf("Message-Delta:");
                intoIntoString = intoString.substring(somePlace + 15);
                indexOf = intoIntoString.indexOf("\r\n");
                mDelta = Integer.parseInt(intoIntoString.substring(0, indexOf));
                if (sourceFolder.equals(destFolder) != true) {
                    //Something moved to trash
                    if (destFolder.equals("trAsH")) {
                        if (sourceFolder.equals("HM_BuLkMail_") != true) {
                            //Email moved to trash. emailCount
                            contactListInterface.changeEmailCount(emailCount - mDelta);
                        }
                    }
                    if (destFolder.equals("HM_BuLkMail_")) {
                        //Email moved to junkmail. emailCount -1
                        contactListInterface.changeEmailCount(emailCount - mDelta);
                    }
                    if (sourceFolder.equals("trAsH")) {
                        if (destFolder.equals("HM_BuLkMail_") != true) {
                            //Unread email from trAsh moved to somewhere else but not junkmail emailCount +1
                            contactListInterface.changeEmailCount(emailCount + mDelta);
                        }
                    }
                }
                if (sourceFolder.equals(destFolder)) {
                    //If not a message in junkmail
                    if (sourceFolder.equals("HM_BuLkMail_") != true) {
                        //If you've read an email, emailCount -1
                        contactListInterface.changeEmailCount(emailCount - mDelta);
                    }
                }
                System.out.println("SourceFolder: " + sourceFolder);
                System.out.println("DestFolder: " + destFolder);
                System.out.println("mDelta: " + mDelta);
            }
           
            
        } else if (commands[0].equals("ADD") && commands[2].equals("RL")) {
            if (gtc.equals("A") || blp.equals("BL")) {
                //Notify contactListInterface, you have been add!
                contactListInterface.reverseListAdd(commands[4], commands[5]);
            } else {
                //Add to allow list
                notificationSH.addContactToAllowList(commands[4], commands[5]);
            }
        } else if (commands[0].equals("REM") && commands[2].equals("RL")) { 
            for (int i = 0; i < contactList.contactNumbers; i++) {
                if (contactList.contacts[i].getEmail().equals(commands[4])) {
                    int lists = Integer.parseInt(contactList.contacts[i].getLists());
                    lists -= 8;
                    contactList.contacts[i].setLists(Integer.toString(lists));
                }
            }
        } else if (commands[0].equals("REA")) {
            
            //Client has succesfully changed his name
            contactList.contacts[0].setName(commands[4]);
            contactListInterface.hasChangedName((byte)1);
            
        } else if (commands[0].equals("OUT") && commands[1].equals("OTH")) {
            
            //Email has logged on, on another location
            contactListInterface.connectionClosed(1);
            
        } else if (commands[0].equals("QNG")) {
            Integer timeToWait = Integer.parseInt(commands[1]);
            pingTimer = new Timer();
            pingTimer.schedule(new TimerTask() {
                public void run() {
                    out.print("PNG\r\n");
                    out.flush();
                }
            }, timeToWait * 1000);
        } else if (commands[0].equals("216")) {
            //If the display name isn't changed correctly
            contactListInterface.hasChangedName((byte)2);
        } else if (commands[0].equals("500")) {
            
            closeConnectionAndNotifyCLL(commands[0]);
            System.out.println("Service temporary unavailable");
        
        } else if (commands[0].equals("601")) {
           
            closeConnectionAndNotifyCLL(commands[0]);
            System.out.println("Service temporary unavailable");
        
        } else if (commands[0].equals("910")) {
            
            closeConnectionAndNotifyCLL(commands[0]);
            System.out.println("Server is to busy");
        
        } else if (commands[0].equals("911")) {
            
            closeConnectionAndNotifyCLL(commands[0]);
            System.out.println("The account name is invalid");
        
        } else if (commands[0].equals("913")) {
            
            //If XFR is send while status is set to HDN
            contactListInterface.errorReceived(913);
       
        } else if (commands[0].equals("921")) {
            
            closeConnectionAndNotifyCLL(commands[0]);
            System.out.println("Server is to busy");
       
        }
        
        System.out.println(inputString);
    }
    
}
