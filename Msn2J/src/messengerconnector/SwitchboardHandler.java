package messengerconnector;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class SwitchboardHandler implements Runnable {

    //Variable declarations
    private Socket connection = null;
    public PrintWriter out = null;
    private BufferedReader in = null;
    private SwitchboardProtocol swp = null;
    private Thread runner = null;
    public boolean clientUp = false;
    private ContactListListener contactListInterface = null;
    public long triggerID = 0;
    
    public SwitchboardHandler(String address, int port, ContactListListener contactListInterface) {
        
        this.contactListInterface = contactListInterface;
        
        try {
            connection = new Socket(address, port);
            out = new PrintWriter(connection.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        } catch (UnknownHostException uhe) { 
            System.out.println("Er heeft een UnknownHostExceptionn opgetreden");
            System.exit(1);
        } catch (IOException e) { 
            System.out.println("Er heeft een IOException opgetreden");
            System.exit(1);
        }
        swp = new SwitchboardProtocol(contactListInterface, in, this);
       
        clientUp = true;
        runner = new Thread(this);
        runner.start();
        
        triggerID++;
        
       
    }
    
    public void addChatListener(ChatListener cl) {
        swp.addChatListener(cl);
    }
    
    public void answerInvitation(String myEmail, String swissAuth, String swissID, long triggerID) {
        out.print("ANS " + triggerID + " " + myEmail + " " + swissAuth + " " + swissID + "\r\n");
        out.flush();
    }
    
    public boolean clientIsUp() {
        return clientUp;
    }
    
    public void closeConnection() {
        out.print("OUT\r\n");
        out.flush();
        clientUp = false;
    }
    
    public void invite(Contact inviteContact) {
        out.print("CAL " + triggerID + " " + inviteContact.getEmail() + "\r\n");
        out.flush();
        try {
            in.readLine();
            in.readLine();
        } catch (IOException ioe) { }
        Contact[] arrayC = new Contact[1];
        arrayC[0] = inviteContact;
        contactListInterface.newChatWindow(arrayC, this);
    }
    
    public void sayText(String all) {
        out.print("MSG " + triggerID + " A " + all.length() + "\r\n" + all);
        out.flush();
        triggerID++;
    }
    
    public void sendD(String all) {
        out.print("MSG " + triggerID + " D " + all.length() + "\r\n" + all);
        out.flush();
        triggerID++;
    }
    
    public void sendTyping(String email) {
        System.out.println();
        String sendString = new String();
        sendString += "MIME-Version: 1.0\r\n";
        sendString += "Content-Type: text/x-msmsgscontrol\r\n";
        sendString += "TypingUser: " + email + "\r\n";
        sendString += "\r\n\r\n";
        out.print("MSG " + triggerID + " U " + sendString.length() + "\r\n" + sendString);
        out.flush();
        System.out.print("MSG " + triggerID + " U " + sendString.length() + "\r\n" + sendString);
        triggerID++;
    }
    
    public void sendUSR(String email, String auth) {
        out.print("USR " + triggerID + " " + email + " " + auth + "\r\n");
        out.flush();
    }
    
    public void run() {
        
        String readString = null;
        
        try {
            while (clientIsUp()) {
                while ((readString = in.readLine()) != null) {
                    swp.processData(readString);
                    System.out.println("jsdfhksdfhdfksjhsdfjkfhsdk:              " + readString);
                }
            }
            
        } catch (IOException ioe) { 
            System.out.println("Er heeft een IOException opgetreden");
        }
        

    }
    
}
