package messengerconnector;

public interface ContactListListener {
    public void clientSynchronised();
    public void connectionClosed(int error);
    public void errorReceived(int error);
    public void hasChangedName(byte status);
    public void newChatWindow(Contact[] participants, SwitchboardHandler swl);
    public void newEmail(int emailCount);
    public void emailReceived(String fromAddress);
    public void changeEmailCount(int emailCount);
    public void refreshContact(Contact contact);
    public void refreshContactList(ContactList contactList);
    public void reverseListAdd(String email, String nick);
}
