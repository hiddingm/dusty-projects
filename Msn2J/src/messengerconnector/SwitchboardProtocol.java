package messengerconnector;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.Random;
import messengerconnector.stringeditor.Slicer;

public class SwitchboardProtocol {
    
    private int participantNumber = 0;
    private int currentPNumber = 1;
    private Contact[] participants;
    private ContactListListener contactListInterface;
    private BufferedReader in;
    private ChatListener cl;
    private SwitchboardHandler swl;
    private int avaterSendingStatus = 0;
    
    public SwitchboardProtocol(ContactListListener contactListInterface, BufferedReader in, SwitchboardHandler swl) {
        this.contactListInterface = contactListInterface;
        this.in = in;
        this.swl = swl;
    }
       
    public void addChatListener(ChatListener cl) {
        this.cl = cl;
    }
    
    public void processData(String inputString) {
        
        String[] commands = Slicer.sliceInPieces(inputString, " ");
        
        if (commands[0].equals("IRO")) {
            //The IRO command contains information about a person already in the switchboard session
            participantNumber = Integer.parseInt(commands[3]);
            currentPNumber = Integer.parseInt(commands[2]);
            
            if (participants == null)
                participants = new Contact[participantNumber];
            //Create the new contacts
            participants[currentPNumber - 1] = new Contact(commands[4], commands[5], "", "");
            currentPNumber++;
            
        } else if (commands[0].equals("ANS")) {
            //Everybody is send, create the chatwindow
            contactListInterface.newChatWindow(participants, swl);
            
        } else if (commands[0].equals("MSG")) {
            //If a message is received
            String content = "";
            int charInt = 0;
            char oneChar = '0';
            while (content.indexOf("\r\n\r\n") == -1) {
                try {
                    charInt = in.read();
                    if (charInt != -1)
                        oneChar = (char)charInt;
                } catch (IOException ioe) { ioe.printStackTrace(); }
                content += oneChar;
            }
            //System.out.println(content);
            //If it really is a message!
            if (content.indexOf("text/plain") != -1) {
                int stillLeft = Integer.parseInt(commands[3]) - content.length();
                char[] character = new char[stillLeft];
                try {
                    in.read(character, 0, stillLeft);
                } catch (IOException ioe) { }
                content += new String(character);
                System.out.println("Say: " + content.substring(content.indexOf("\r\n\r\n") + 4));
                cl.messageReceived(inputString, content);
            }
            //Or maybe an avater invite (this is tricky!)!!!
            if (content.indexOf("application/x-msnmsgrp2p") != -1) {
                //The first invite request is received
                if (avaterSendingStatus == 0) {
                    String to = content.substring(content.indexOf("To: <") + 13, content.indexOf(">", content.indexOf("To: <")));
                    String from = content.substring(content.indexOf("From: ") + 15, content.indexOf(">", content.indexOf("From: ")));
                    int contentLength = Integer.parseInt(content.substring(content.indexOf("Content-Length: ") + 16, content.indexOf("\r\n", content.indexOf("Content-Length: "))));
                    //If it really is an avater request
                    
                    if (content.indexOf("EUF-GUID: {A4268EEC-FEC5-49E5-95C3-F126696BDBF6}") != -1) {
                        //If the content length is larger than zero
                        if (contentLength > 0) {
                            String rBinaryHeader = content.substring(content.indexOf("\r\n\r\n") + 4, content.indexOf("INVITE"));
                            
                            /*
                            //First lets create the acknowledgement
                            String ackString = "";
                            ackString += "MIME-Version: 1.0\r\n";
                            ackString += "Content-Type: application/x-msnmsgrp2p\r\n";
                            ackString += "P2P-Dest: " + to + "\r\n\r\n";
                            //Read the binary header
                            String rBinaryHeader = content.substring(content.indexOf("\r\n\r\n") + 4, content.indexOf("INVITE"));
                            byte[] rByteArray = new byte[48];
                            for (int i = 0; i < 48; i++)
                                rByteArray[i] = rBinaryHeader.substring(i, i).getBytes()[0];
                            
                            System.out.println("Theirs: " + Hex.toHex(rByteArray));
                            //Create our binary header
                            byte[] binaryHeader = new byte[48];
                            for (int i = 0; i < 48; i++) {
                                binaryHeader[i] = 0;
                            }
                            Random generator = new Random();
                            int randomInteger = generator.nextInt();
                            byte[] RIBA = LittleEndian.intToLEDByteArray(randomInteger);
                            binaryHeader[4] = RIBA[0]; binaryHeader[5] = RIBA[1]; binaryHeader[6] = RIBA[2]; binaryHeader[7] = RIBA[3];
                            binaryHeader[16] = rByteArray[16];
                            binaryHeader[17] = rByteArray[17];
                            binaryHeader[18] = rByteArray[18];
                            binaryHeader[19] = rByteArray[19];
                            binaryHeader[20] = rByteArray[20];
                            binaryHeader[21] = rByteArray[21];
                            binaryHeader[22] = rByteArray[22];
                            binaryHeader[23] = rByteArray[23];
                            //Flag field 6, must be 2
                            binaryHeader[28] = 2;
                            //Seventh Field
                            binaryHeader[32] = rByteArray[4];
                            binaryHeader[33] = rByteArray[5];
                            binaryHeader[34] = rByteArray[6];
                            binaryHeader[35] = rByteArray[7];
                            //Eight field
                            binaryHeader[36] = rByteArray[32];
                            binaryHeader[37] = rByteArray[33];
                            binaryHeader[38] = rByteArray[34];
                            binaryHeader[39] = rByteArray[35];
                            //Ninth Field
                            binaryHeader[40] = rByteArray[16];
                            binaryHeader[41] = rByteArray[17];
                            binaryHeader[42] = rByteArray[18];
                            binaryHeader[43] = rByteArray[19];
                            binaryHeader[44] = rByteArray[20];
                            binaryHeader[45] = rByteArray[21];
                            binaryHeader[46] = rByteArray[22];
                            binaryHeader[47] = rByteArray[23];
                            //System.out.println("Mine: " + Hex.toHex(binaryHeader));
                            //System.out.println(ackString + Hex.toHex(binaryHeader) + Hex.toHex(new byte[]{0, 0, 0, 0}));
                            try {
                                ackString += new String(binaryHeader, "utf-8");
                                ackString += new String(new byte[]{0, 0, 0, 0}, "utf-8");
                            } catch (Exception e) { }
                            //System.out.println(ackString);
                            swl.sendD(ackString);
                            System.out.println(content + "\r\n\r\n\r\n" + ackString);
                            
                            
                            
                            //Wohoo...... lets send another very boring message!
                            ackString = "";
                            ackString += "MIME-Version: 1.0\r\n";
                            ackString += "Content-Type: application/x-msnmsgrp2p\r\n";
                            ackString += "P2P-Dest: " + to + "\r\n\r\n";
                            String msnSlpData = "";
                            msnSlpData += "MSNSLP/1.0 200 OK\r\n";
                            msnSlpData += "To: <msnmsgr:" + from + ">\r\n";
                            msnSlpData += "From: <msnmsgr:" + to + ">\r\n";
                            String via = content.substring(content.indexOf("Via:"), content.indexOf("\r\nCSeq") + 2);
                            msnSlpData += via;
                            msnSlpData += "CSeq: 1\r\n";
                            String callID = content.substring(content.indexOf("Call-ID:"), content.indexOf("\r\nMax") + 2);
                            msnSlpData += callID;
                            msnSlpData += "Max-Forwards: 0\r\n";
                            msnSlpData += "Content-Type: application/x-msnmsgr-sessionreqbody\r\n";
                            String sessionID = content.substring(content.indexOf("SessionID:"), content.indexOf("\r\nAppID") + 2);
                            String msgBody = "\r\n" + sessionID + "\r\n";
                            msnSlpData += "Content-Length: " + msgBody.length() + "\r\n";
                            msnSlpData += msgBody;
                            byte[] fiveNullB = new byte[]{0, 0, 0, 0, 0};
                            try {
                                msnSlpData += new String(fiveNullB, "utf-8");
                            } catch (Exception e) { }
                            for (int i = 0; i < 48; i++) {
                                binaryHeader[i] = 0;
                            }
                            RIBA = LittleEndian.intToLEDByteArray(randomInteger - 3); //BaseID - 3
                            binaryHeader[4] = RIBA[0]; binaryHeader[5] = RIBA[1]; binaryHeader[6] = RIBA[2]; binaryHeader[7] = RIBA[3];
                            int msnSlpDataLength = msnSlpData.length();
                            byte[] mSDLBA = LittleEndian.intToLEDByteArray(msnSlpDataLength);
                            binaryHeader[16] = mSDLBA[0];
                            binaryHeader[17] = mSDLBA[1];
                            binaryHeader[18] = mSDLBA[2];
                            binaryHeader[19] = mSDLBA[3];
                            binaryHeader[24] = mSDLBA[0];
                            binaryHeader[25] = mSDLBA[1];
                            binaryHeader[26] = mSDLBA[2];
                            binaryHeader[27] = mSDLBA[3];
                            randomInteger = generator.nextInt();
                            byte[] RB = LittleEndian.intToLEDByteArray(randomInteger);
                            binaryHeader[32] = RB[0];
                            binaryHeader[33] = RB[1];
                            binaryHeader[34] = RB[2];
                            binaryHeader[35] = RB[3];
                            //System.out.println(ackString + Hex.toHex(binaryHeader) + msnSlpData);
                            try {
                                ackString += new String(binaryHeader, "utf-8");
                            } catch (Exception e) { }
                            ackString += msnSlpData;
                            //swl.sendD(ackString);
                            */
                        }
        
                    }
                    
                }

            }
            
        } else if (commands[0].equals("BYE")) {
            //If the BYE command is sent, check if it is because of time or because a participant left
            if(commands.length == 3) {
                cl.participantOffline(commands[1], true);
                swl.clientUp = false;
            } else {
                cl.participantOffline(commands[1], false);
                swl.clientUp = false;
            }
            
        } else if (commands[0].equals("JOI")) {
            //If a contact joins the conversation
            cl.contactJoin(commands[1]);
        }
        
        
    }
    
}
