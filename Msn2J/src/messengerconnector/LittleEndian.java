package messengerconnector;

import java.math.BigInteger;

public class LittleEndian {
    
    public LittleEndian() {
        
    }
    
    public static BigInteger LEDByteArrayToBigInteger(byte[] b) {
        byte[] bRev = reverseByteArray(b);
        return new BigInteger(bRev);
    }
    
    public static byte[] reverseByteArray(byte[] b) {
        int left  = 0;          // index of leftmost element
        int right = b.length-1; // index of rightmost element

        while (left < right) {
            // exchange the left and right elements
            byte temp = b[left]; 
            b[left]  = b[right]; 
            b[right] = temp;

            // move the bounds toward the center
            left++;
            right--;
        }
        return b;
    }
    
    public static byte[] intToLEDByteArray (final int integer) {
        int byteNum = (40 - Integer.numberOfLeadingZeros (integer < 0 ? ~integer : integer)) / 8;
        byte[] byteArray = new byte[4];

        for (int n = 0; n < byteNum; n++)
                byteArray[3 - n] = (byte) (integer >>> (n * 8));

        return (byteArray);
    }
    
}
