package messengerconnector;

public interface ChatListener {
    public void contactJoin(String email);
    public void messageReceived(String command, String content);
    public void participantOffline(String email, boolean idle);
}
