package messengerconnector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class NotificationserverHandler extends Thread {
    
    //Variable declarations
    private String accountEmail = null;
    private char[] accountPassword = null;
    private Socket connection = null;
    private BufferedReader in = null;
    private LoginListener loginInterface = null;
    private MessengerProtocol NFProtocol = new MessengerProtocol(this);
    private PrintWriter out = null;
    private Thread runner = null;
    private ContactListListener contactListInterface;
    
    public NotificationserverHandler() {

    }
    
    public void addGroup(String groupName) {
        ContactList contactList = NFProtocol.contactList;
        Group[] groups = new Group[contactList.groupNumbers + 1];
        for (int i = 0; i < contactList.groupNumbers; i++) {
            groups[i] = contactList.groups[i];
        }
        groups[groups.length - 1] = new Group(groupName, Integer.toString(groups.length));
        contactList.groups = groups;
        contactList.groupNumbers++;
        out.print("ADG " + NFProtocol.triggerID++ + " " + encodeNormal(groupName) + " 0\r\n");
        out.flush();
        NFProtocol.contactListInterface.refreshContactList(contactList);
    }

    public boolean addContactToForwardList(String email, String nick, String group) {
        int placeInContactList = -1;
        ContactList contactList = NFProtocol.contactList;
        //I Is 1 for a reason!
        for (int i = 1; i < contactList.contactNumbers; i++) {
            if (contactList.contacts[i].getEmail().equals(email)) {
                placeInContactList = i;
                //Check all of the contacts groups, if the contact is trying to be add to a group in wich he already is, return false
                String[] groups = contactList.contacts[placeInContactList].getGroups();
                int groupSTI = Integer.parseInt(group);
                int cGroup;
                for (int k = 0; k < groups.length; k++) {
                    if (!groups[k].equals(""))
                        cGroup = Integer.parseInt(groups[k]);
                    else
                        cGroup = -1;
                    if (cGroup == groupSTI)
                        return false;

                }
            }
        }
        //If placeInContactList is bigger than zero, the contact already exists in the contactList
        if (placeInContactList > 0) {
            Contact theContact = contactList.contacts[placeInContactList];
            //Get the lists in a int, for bitwise AND with 1, if this returns 0, the contact hasn't yet been added to the forward list!
            int lists = Integer.parseInt(theContact.getLists());
            if ((lists & 1) == 0)
                theContact.setLists(Integer.toString(lists + 1));
            String[] groups = theContact.getGroups();
            String groupString = "";
            boolean dontAdd = false;
            for (int i = 0; i < groups.length; i++) {
                if (!groups[i].equals("")) {
                    if (Integer.parseInt(groups[i]) > 0 && group.equals("0")) 
                        dontAdd = true;
                    groupString += groups[i] + ",";
                }
            }
            groupString += group;
            if (groups.length == 1 && groups[0].equals("0"))
                groupString = group;
            if (!dontAdd)
                theContact.setGroups(groupString);
            groups = theContact.getGroups();
        } else {
            Contact[] newContacts = new Contact[contactList.contactNumbers + 1];
            for (int i = 0; i < contactList.contactNumbers; i++) {
                newContacts[i] = contactList.contacts[i];
            }
            newContacts[newContacts.length - 1] = new Contact(email, nick, Integer.toString(1), group);
            contactList.contactNumbers++;
            contactList.contacts = newContacts;
        }
        out.print("ADD " + NFProtocol.triggerID + " FL " + email + " " + encodeNormal(nick) + " " + group + "\r\n");
        out.flush();
        NFProtocol.contactListInterface.refreshContactList(contactList);
        NFProtocol.triggerID++;
        return true;
    }
    
    public boolean addContactToAllowList(String email, String nick) {
        int placeInContactList = -1;
        ContactList contactList = NFProtocol.contactList;
        //I Is 1 for a reason!
        for (int i = 1; i < contactList.contactNumbers; i++) {
            if (contactList.contacts[i].getEmail().equals(email)) {
                placeInContactList = i;
            }
        }
        //If placeInContactList is bigger than zero, the contact already exists in the contactList
        if (placeInContactList > 0) {
            //Get the lists in a int, for bitwise AND with 2, if this returns 0, the contact hasn't yet been add to the allow list!
            int lists = Integer.parseInt(contactList.contacts[placeInContactList].getLists());
            if ((lists & 4) == 4)
                return false;
            if ((lists & 2) == 2)
                return false;
            contactList.contacts[placeInContactList].setLists(Integer.toString(lists + 2));
        } else {
            Contact[] newContacts = new Contact[contactList.contactNumbers + 1];
            for (int i = 0; i < contactList.contactNumbers; i++) {
                newContacts[i] = contactList.contacts[i];
            }
            newContacts[newContacts.length - 1] = new Contact(email, nick, Integer.toString(2), "");
            contactList.contactNumbers++;
            contactList.contacts = newContacts;
        }
        out.print("ADD " + NFProtocol.triggerID + " AL " + email + " " + encodeNormal(nick) + "\r\n");
        out.flush();
        NFProtocol.contactListInterface.refreshContactList(contactList);
        NFProtocol.triggerID++;
        return true;
    }
    
    public boolean addContactToBlockList(String email, String nick) {
        int placeInContactList = -1;
        ContactList contactList = NFProtocol.contactList;
        //I Is 1 for a reason!
        for (int i = 1; i < contactList.contactNumbers; i++) {
            if (contactList.contacts[i].getEmail().equals(email)) {
                placeInContactList = i;
            }
        }
        //If placeInContactList is bigger than zero, the contact already exists in the contactList
        if (placeInContactList > 0) {
            //Get the lists in a int, for bitwise AND with 4, if this returns 0, the contact hasn't yet been add to the block list!
            int lists = Integer.parseInt(contactList.contacts[placeInContactList].getLists());
            if ((lists & 2) == 2)
                return false;
            if ((lists & 4) == 4)
                return false;
            contactList.contacts[placeInContactList].setLists(Integer.toString(lists + 4));
        } else {
            Contact[] newContacts = new Contact[contactList.contactNumbers + 1];
            for (int i = 0; i < contactList.contactNumbers; i++) {
                newContacts[i] = contactList.contacts[i];
            }
            newContacts[newContacts.length - 1] = new Contact(email, nick, Integer.toString(2), "");
            contactList.contactNumbers++;
            contactList.contacts = newContacts;
        }
        out.print("ADD " + NFProtocol.triggerID + " BL " + email + " " + encodeNormal(nick) + "\r\n");
        out.flush();
        NFProtocol.contactListInterface.refreshContactList(contactList);
        NFProtocol.triggerID++;
        return true;
    }
        
    public void addContactListListener(ContactListListener contactListInterface) {
        NFProtocol.addContactListListener(contactListInterface);
        loginInterface = null;
    }
    
    public void addLoginListener(LoginListener loginInterface) {
        if (this.loginInterface == null)
            this.loginInterface = loginInterface;
    }
    
    public void changeName(String name) {
        //Function to change display name
        out.println("REA " + NFProtocol.triggerID + " " + accountEmail + " " + encodeNormal(name));
    }
        
    public void changeStatus(String status) {
        //Change the status
        out.println("CHG " + NFProtocol.triggerID + " " + status + " 0");
        //out.println("CHG " + NFProtocol.triggerID + " " + status + " 268435456 " + createMsnObject(new java.io.File("msn2j.tmp"), 3));
        NFProtocol.triggerID++;
    }
    
    public void closeConnection(int logout) {
        //Close the connection, if logout is 1, send the OUT command to the notification server
        runner = null;
        if (logout == 1)
            out.println("OUT\r\n");
        out.close();
        try {
            in.close();
            connection.close();
        } catch (IOException ioe) { }
    }
    
    private String createMsnObject(File objFile, int type) {
        //The file size variable
        long size = objFile.length();
        //The location variable
        String location = objFile.getName();
        //Get the file inputstream for the file sha1 hash
        FileInputStream fileInput = null;
        try {
            fileInput = new FileInputStream(objFile);
        } catch (FileNotFoundException fnfe) { }
        //The variabla for holding all of the file's bytes
        byte[] fileBytes = new byte[(int)size];
        //Read all the file's bytes in the fileBytes variable
        try {
            for (int i = 0; i < size; i++) {
                int cByte = fileInput.read();
                if (cByte != -1)
                    fileBytes[i] = (byte)cByte;
            }
        } catch (IOException ioe) { }
        //Create the messagedigest object for the sha1 hash
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException nsae) { }
        //The sha1 hash
        md.update(fileBytes);
        //The digest variable now holds the sha1 hash
        byte[] digest = md.digest();
        //The sha1d variable which contains the base64 encoded sha1 hash
        String sha1d = Base64.encodeBytes(digest);
        //The sha1c field wich holds alot off information!
        byte[] sha1c = null;
        try {
            sha1c = ("Creator" + accountEmail + "Size" + size + "Type" + type + "Location" + location + "FriendlyAAA=SHA1D" + sha1d).getBytes("UTF-8");
        } catch (UnsupportedEncodingException uee) { }
        md.update(sha1c);
        //The digest variable now holds the sha1 hash
        digest = md.digest();
        //The sha1cString variable which contains the base64 encoded sha1 hash
        String sha1cString = Base64.encodeBytes(digest);
        String msnObject = "<msnobj Creator=\"" + accountEmail + "\" Size=\"" + size + "\" Type=\"" + type + "\" Location=\"" + location + "\" Friendly=\"AAA=\" SHA1D=\"" + sha1d + "\" SHA1C=\"" + sha1cString + "\"/>";
        return encodeNormal(msnObject);
    }
    
    private String encodeNormal(String encode) {
        //Get the URLEncoded display name
        String encoded;
        try {
            encoded = URLEncoder.encode(encode, "UTF8");
        } catch (Exception e) {
            return null;
        }
        String tempEncoded = "";
        //Because java doesn't change a space character into %20, but into +, this has to be changed manually
        for (int i = 0; i < encoded.length(); i++) {
            if (encoded.charAt(i) == '+') {
                tempEncoded = tempEncoded + "%20";
            } else {
                tempEncoded = tempEncoded + encoded.charAt(i);
            }
        }
        encoded = tempEncoded;
        tempEncoded = "";
        //Java doesn't replace a . at all
        for (int i = 0; i < encoded.length(); i++) {
            if (encoded.charAt(i) == '.') {
                tempEncoded = tempEncoded + "%2E";
            } else {
                tempEncoded = tempEncoded + encoded.charAt(i);
            }
        }
        return tempEncoded;
    }
    
    public void login(String accountEmail, char[] accountPassword) {
        if (loginInterface != null) {
            //Save email and password
            this.accountEmail = accountEmail;
            this.accountPassword = accountPassword;
            //Connect to the msn server
            try {
                connection = new Socket("messenger.hotmail.com", 1863);
                out = new PrintWriter(connection.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            } catch (IOException ioe) { 
                loginInterface.exceptionThrowed(ioe);
            }
            //Add the variables to, the notification protocol
            NFProtocol.addLoginListener(loginInterface);
            NFProtocol.addConnection(connection, out, in, accountEmail, accountPassword);
            //Send the first line, to the login
            out.println("VER 0 " + MessengerProtocol.MESSENGERPROTOCOLS + " CVR0");
            //Start the "readline()" Thread
            runner = new Thread(this);
            runner.start();
            
        }
    }

    public boolean removeContactFromForwardList(String email, String group) {
        int placeInContactList = -1;
        ContactList contactList = NFProtocol.contactList;
        //I Is 1 for a reason!
        for (int i = 1; i < contactList.contactNumbers; i++) {
            if (contactList.contacts[i].getEmail().equals(email)) {
                placeInContactList = i;
            }
        }
        if (placeInContactList > 0) {
            //Contact already in contactList
            String[] groups = contactList.contacts[placeInContactList].getGroups();
            if (groups.length == 1) {
                if (!group.equals(groups[0]))
                    return false;
                contactList.contacts[placeInContactList].setGroups("");
                int lists = Integer.parseInt(contactList.contacts[placeInContactList].getLists());
                if ((lists & 1) == 0)
                    return false;
                lists--;
                contactList.contacts[placeInContactList].setLists(Integer.toString(lists));
            } else {
                String groupString = "";
                int extraCount = 0;
                for (int i = 0; i < groups.length - 1; i++) {
                    if (groups[i].equals(group)) {
                        extraCount++;
                    }
                    if (i != groups.length - 2)
                        groupString += groups[i + extraCount] + ",";
                    else
                        groupString += groups[i + extraCount];
                }
                contactList.contacts[placeInContactList].setGroups(groupString);
            }
        } else {
            return false;
        }
        NFProtocol.contactListInterface.refreshContactList(contactList);
        out.print("REM " + NFProtocol.triggerID++ + " FL " + email + " " + group + "\r\n");
        out.flush();
        NFProtocol.triggerID++;
        return true;
    }
    
    public boolean removeContactFromAllowList(String email) {
        int placeInContactList = -1;
        ContactList contactList = NFProtocol.contactList;
        //I Is 1 for a reason!
        for (int i = 1; i < contactList.contactNumbers; i++) {
            if (contactList.contacts[i].getEmail().equals(email)) {
                placeInContactList = i;
            }
        }
        if (placeInContactList > 0) {
            //Contact already in contactList
            int lists = Integer.parseInt(contactList.contacts[placeInContactList].getLists());
            if ((lists & 2) == 0)
                return false;
            lists -= 2;
            if (lists == 0) {
                //Lists is 0, remove contact from the contactList
                int extraCount = 0;
                Contact[] newContacts = new Contact[contactList.contactNumbers - 1];
                for (int i = 1; i < contactList.contactNumbers - 1; i++) {
                    if (contactList.contacts[i + extraCount].getEmail().equals(email) != true) {
                        newContacts[i] = contactList.contacts[i + extraCount];
                    } else {
                        extraCount = 1;
                        newContacts[i] = contactList.contacts[i + extraCount];
                    }
                }
                contactList.contactNumbers--;
                contactList.contacts = newContacts;
            } else {
                //Just change the listValue
                contactList.contacts[placeInContactList].setLists(Integer.toString(lists));
            }
        } else {
            return false;
        }
        NFProtocol.contactListInterface.refreshContactList(contactList);
        out.print("REM " + NFProtocol.triggerID++ + " AL " + email + "\r\n");
        out.flush();
        NFProtocol.triggerID++;
        return true;
    }
    
    public boolean removeContactFromBlockList(String email) {
        int placeInContactList = -1;
        ContactList contactList = NFProtocol.contactList;
        //I Is 1 for a reason!
        for (int i = 1; i < contactList.contactNumbers; i++) {
            if (contactList.contacts[i].getEmail().equals(email)) {
                placeInContactList = i;
            }
        }
        if (placeInContactList > 0) {
            //Contact already in contactList
            int lists = Integer.parseInt(contactList.contacts[placeInContactList].getLists());
            if ((lists & 4) == 0)
                return false;
            lists -= 4;
            if (lists == 0) {
                //Lists is 0, remove contact from the contactList
                int extraCount = 0;
                Contact[] newContacts = new Contact[contactList.contactNumbers - 1];
                for (int i = 1; i < contactList.contactNumbers - 1; i++) {
                    if (contactList.contacts[i + extraCount].getEmail().equals(email) != true) {
                        newContacts[i] = contactList.contacts[i + extraCount];
                    } else {
                        extraCount = 1;
                        newContacts[i] = contactList.contacts[i + extraCount];
                    }
                }
                contactList.contactNumbers--;
                contactList.contacts = newContacts;
            } else {
                //Just change the listValue
                contactList.contacts[placeInContactList].setLists(Integer.toString(lists));
            }
        } else {
            return false;
        }
        NFProtocol.contactListInterface.refreshContactList(contactList);
        out.print("REM " + NFProtocol.triggerID++ + " BL " + email + "\r\n");
        out.flush();
        NFProtocol.triggerID++;
        return true;
    }
    
    public boolean removeGroup(String groupID) {
        ContactList contactList = NFProtocol.contactList;
        if (Integer.parseInt(groupID) >= contactList.groupNumbers || Integer.parseInt(groupID) == 0) {
            System.out.println("doh:)");
            return false;
        }
        
        Contact[] contacts = contactList.contacts;
        for (int i = 0; i < contactList.contactNumbers; i++) {
            String groups[] = contacts[i].getGroups();
            for (int k = 0; k < groups.length; k++) {
                if (groups[k].equals(groupID))
                    return false;
            }
        }
        
        Group[] groups = new Group[contactList.groupNumbers - 1];
        int extraCount = 0;
        for (int i = 0; i < contactList.groups.length - 1; i++) {
            if (i != Integer.parseInt(groupID)) {
                groups[i] = contactList.groups[i + extraCount];
            } else {
                extraCount = 1;
                groups[i] = contactList.groups[i + extraCount];
            }
        }
        contactList.groups = groups;
        contactList.groupNumbers--;
        NFProtocol.contactListInterface.refreshContactList(contactList);
        out.print("RMG " + NFProtocol.triggerID++ + " " + groupID + "\r\n");
        out.flush();
        return true;
    }
    
    public void resetConnection(String ip, int port) {
        //Reset the whole connection
        try {
            //Close the connection
            out.close();
            in.close();
            connection.close();
            
            //Connect to the new Ip address and port
            connection = new Socket(ip, port);
            out = new PrintWriter(connection.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            out.println("VER 0 " + MessengerProtocol.MESSENGERPROTOCOLS + " CVR0");
            
            //Create a new MessengerProtocol and set values, like the login function
            NFProtocol = new MessengerProtocol(this);
            NFProtocol.addLoginListener(loginInterface);
            NFProtocol.addConnection(connection, out, in, accountEmail, accountPassword);
        } catch (IOException ioe) { 
            loginInterface.exceptionThrowed(ioe);
        }
    }
    
    public void run() {
        
        String readString = null;
        //The main loop
        try {
            while ((readString = in.readLine()) != null) {
                //Let the NotificationProtocol class, process the data
                NFProtocol.processData(readString);
            }
            
        } catch (IOException ioe) { 
            if (loginInterface != null) {
                //Notify the loginInterface there has been an IOException
                loginInterface.exceptionThrowed(ioe);
            } else {
                if (contactListInterface != null)
                    contactListInterface.connectionClosed(0);
            }
        }
        

    }
    
    public void openConversation(Contact inviteContact) {
        //A function to start a conversation.
        //The Notification Protocol, will trigger the newChatWindow() function, in the contactListInterface
        NFProtocol.inviteContact = inviteContact;
        out.println("XFR " + NFProtocol.triggerID + " SB");
    }
    
    public void synchroniseClient() {
        //This function will let the server send the contactList
        out.println("SYN " + NFProtocol.triggerID + " 0");
        NFProtocol.triggerID++;
    }
    
}
