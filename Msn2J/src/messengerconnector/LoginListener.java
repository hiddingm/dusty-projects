package messengerconnector;

public interface LoginListener {
    public void errorReceived(String errorNumber);
    public void exceptionThrowed(Exception e);
    public void protocolNotSupported(String msnProtocol);
    public void recommendedClientReceived(String url);
    public void successfulLogin();
    public void wrongPassword();
}