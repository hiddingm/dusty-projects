Within this repository, you'll discover a selection of my high school programming projects.

### LocaQuarium (2011)
LocaQuarium is a Delphi-based project that simulated a heart catheterization machine, earning a national science prize from the KNAW (Dutch Royal Academy of Sciences). 

#### Overview
Since the human body is predominantly composed of water, we devised a system to measure the position of a probe inside an aquarium outfitted with electrodes. This served as an analogy for tracking the position of a catheter inside the heart.

#### Components and Functionality
- **Experimental Setup:** The setup contained a multiplexer, rapidly alternating current through different axes of the aquarium, with each axis following a specific sine-wave frequency.
- **Signal Processing:** The program filters out signals using a band-pass filter, implemented by convoluting its impulse response against the input signal in Fourier space using the FFT.
- **External Libraries:** The code relies on a few small included external libraries and a larger one for audio processing ([ASIO/VST](https://github.com/evolvedexperiment/asiovst)) that must be installed in Delphi for the code to function.
- **Manual Coding:** All DSP code for bandpass filtering, FFT, and OpenGL code for visualization (and everything else in the main Src/ folder) was hand-written by me.

#### Note
The signal was initially fed through the microphone port due to the unavailability of a USB measurement device. A collection of saved measurements can be found in the "Metingen" folder. These measurements can be loaded into the program to recreate the experimental setup. Within the simulation, you'll notice a fish representing the probe as it navigates the aquarium.

### MSN2J (2007 - 2008)

For reasons now unknown to me, it seemed a good idea to code my own MSN Messenger client when I was 14. I got curious about this after some companies were using MSN chatbots that you could interact with. There was an online website where people provided an almost complete reverse engineering of the MSN Messenger protocol, and I then gladly implemented it locally.

This project was a crazy amount of work, split into two parts:

- **MessengerConnector Package:** Handles the protocol, providing a Java interface for interacting with the MSN Messenger servers.
- **MSN2J Package:** Contains all the GUI code, including a contact list with basic functionality and the chat window to initiate and have conversations with friends.

I also managed to make chatbots with the "messengerconnector" package, which could log into my personal email account and provide basic responses. It was my way to immortalize myself on MSN. But alas, the servers went down over the years, and now it's a relic of the past. I'm still very proud of pulling this off at such a young age, and a LOT of time went into this project. It stands as a testament to being young, getting hooked on a tech idea, and pouring hours into making it real.