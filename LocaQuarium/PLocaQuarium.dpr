program PLocaQuarium;

uses
  ExceptionLog,
  Forms,
  UEasyDSP in 'Src\UEasyDSP.pas',
  UfrmLocaQuarium in 'Src\UfrmLocaQuarium.pas' {frmLocaQuarium},
  URLCBandpass in 'Src\URLCBandpass.pas',
  ComplexMath in 'Src\External\ComplexMath\ComplexMath.pas',
  GL in 'Src\External\Delphi3D\GL.pas',
  GLu in 'Src\External\Delphi3D\GLu.pas',
  Glut in 'Src\External\Delphi3D\Glut.pas',
  Textures in 'Src\External\Unit3DS\Textures.pas',
  Unit3DS in 'Src\External\Unit3DS\Unit3DS.pas',
  Vectors in 'Src\External\Unit3DS\Vectors.pas',
  UfrmFilteringSettings in 'Src\UfrmFilteringSettings.pas' {frmFilteringSettings},
  UFilteringSettings in 'Src\UFilteringSettings.pas',
  UfrmRenderingSettings in 'Src\UfrmRenderingSettings.pas' {frmRenderingSettings},
  ULocaQuariumRenderer in 'Src\ULocaQuariumRenderer.pas',
  USettings in 'Src\USettings.pas',
  URenderingSettings in 'Src\URenderingSettings.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'LocaQuarium';
  Application.CreateForm(TfrmLocaQuarium, frmLocaQuarium);
  Application.CreateForm(TfrmFilteringSettings, frmFilteringSettings);
  Application.CreateForm(TfrmRenderingSettings, frmRenderingSettings);
  Application.Run;
end.
