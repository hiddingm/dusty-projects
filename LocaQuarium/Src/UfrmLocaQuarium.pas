﻿unit UfrmLocaQuarium;

{
  De code van LocaQuarium. De belangrijkste delen zijn becommentarieerd.
  In het verslag wordt nog aanvullende toelichting gegeven op de
  ontwerpstrategieën van de code.
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DAV_AsioHost, DAV_Types, DAV_DspBufferedAudioFilePlayer,
  DAV_AudioFile, DAV_AudioFileWAV, DAV_AudioFileAIFF,
  DAV_AudioFileAU, StdCtrls, ExtCtrls, TeeProcs, TeEngine,
  Chart, Series, ComplexMath, UEasyDSP, URLCBandpass, ComCtrls;

type       
  TfrmLocaQuarium = class(TForm)
    AsioHost: TAsioHost;
    GroupBox3: TGroupBox;
    btnChangeFilteringSettings: TButton;
    btnChangeRenderingSettings: TButton;
    GroupBox1: TGroupBox;
    pnlLocaQuarium3D: TPanel;
    btnStartAnalyzing: TButton;
    procedure FormCreate(Sender: TObject);
    procedure AsioHostBufferSwitch32(Sender: TObject; const InBuffer,
      OutBuffer: TDAVArrayOfSingleFixedArray);
    procedure btnStartAnalyzingClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure btnChangeFilteringSettingsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure btnChangeRenderingSettingsClick(Sender: TObject);
    procedure AsioHostSampleRateChanged(Sender: TObject);
  private
    procedure InitOpenGL;
  public
  end;

function SecondsToTimeString(Seconds: Integer): String;

var
  //Variablen voor OpenGL rendering
  h_DCPanel: HDC; //OpenGL Device Context
  h_RC: HGLRC; //GDI Rendering Context

  {
    Een handig stukje code van de ASIO & VST library dat WAV bestandjes kan
    lezen.
  }
  BufferedTestSetupAudioPlayer: TBufferedAudioFilePlayer;
  
  frmLocaQuarium: TfrmLocaQuarium;

implementation

uses
  GL, Math, UfrmFilteringSettings, UFilteringSettings, ULocaQuariumRenderer,
  UfrmRenderingSettings, USettings;

{$R *.dfm}

function SecondsToTimeString(Seconds: Integer): String;
var
  Hours, Minutes: Integer;
begin
  Hours := Trunc(Seconds / 3600);
  Minutes := Trunc(Seconds / 60) mod 60;
  Seconds := Seconds mod 60;

  if Hours > 0 then
    Result := Format('%d', [Hours]) + ':' +
              Format('%.2d', [Minutes]) + ':' +
              Format('%.2d', [Seconds])
  else
    Result := Format('%d', [Minutes]) + ':' +
              Format('%.2d', [Seconds])
end;

procedure TfrmLocaQuarium.AsioHostBufferSwitch32(Sender: TObject;
  const InBuffer, OutBuffer: TDAVArrayOfSingleFixedArray);

  function CalculateMaxAmplitude(Signal: TComplexNumberArray): Single;
  var
    I: Integer;
  begin
    Result := 0;
    for I := 0 to Length(Signal) - 1 do
    begin
      if (Signal[I].R > Result) then
      begin
        Result := Signal[I].R;
      end;
    end;
  end;

var
  AudioFileCurrentPosSeconds: Integer;
  AudioFileTotalTimeSeconds: Integer;
  FileAudio: TDAVArrayOfSingleDynArray;

  I: Integer;

  {
    Naar deze variabelen wordt het signaal van de microfooningang gekopieerd.
    Voor elke filter wordt een aparte variabele gebruikt.
  }
  ComplexInputSignal, ComplexInputSignalRLC1, ComplexInputSignalRLC2,
    ComplexInputSignalRLC3: TComplexNumberArray;

  CurrentPointAmplitude: Single;
  CurrentSpectrumToDrawLength: Integer;

  SignalMaxAmplitude: Single;

  PeakMaxAmplitudeFrequency,
    PeakSecondMaxAmplitudeFrequency,
      PeakThirdMaxAmplitudeFrequency: Single;

  HundredHertzSampleWidth: Integer;
begin
  {
    Voor elk gefilterd signaal wordt een aparte array toegewezen in het
    geheugen.
  }
  SetLength(ComplexInputSignal, ASIOHost.BufferSize);
  SetLength(ComplexInputSignalRLC1, ASIOHost.BufferSize);
  SetLength(ComplexInputSignalRLC2, ASIOHost.BufferSize);
  SetLength(ComplexInputSignalRLC3, ASIOHost.BufferSize);

  if SimulateTestSetup then
  begin
    SetLength(FileAudio, 2);
    SetLength(FileAudio[0], ASIOHost.BufferSize);
    SetLength(FileAudio[1], ASIOHost.BufferSize);

    //Haalt samples op uit de buffer.
    BufferedTestSetupAudioPlayer.GetSamples(
      TDAVArrayOfSingleFixedArray(FileAudio), ASIOHost.Buffersize);

    with frmFilteringSettings do
    begin
      AudioFileCurrentPosSeconds :=
        Trunc(BufferedTestSetupAudioPlayer.FCurrentPosition /
          ASIOHost.SampleRate);
      AudioFileTotalTimeSeconds :=
        Trunc(BufferedTestSetupAudioPlayer.AudioFile.TotalTime);

      lblTestSetupFilePlayed.Caption :=
        SecondsToTimeString(AudioFileCurrentPosSeconds) + ' / ' +
        SecondsToTimeString(AudioFileTotalTimeSeconds);

      pgbTestSetupFilePlayed.Position := AudioFileCurrentPosSeconds;
    end;

    //Vul invoersignalen met samples van audiobestand.
    for I := 0 to ASIOHost.BufferSize - 1 do
    begin
      ComplexInputSignal[I].R := FileAudio[0][I];
      ComplexInputSignalRLC1[I].R := FileAudio[0][I];
      ComplexInputSignalRLC2[I].R := FileAudio[0][I];
      ComplexInputSignalRLC3[I].R := FileAudio[0][I];
      ComplexInputSignal[I].I := 0;
      ComplexInputSignalRLC1[I].I := 0;
      ComplexInputSignalRLC2[I].I := 0;
      ComplexInputSignalRLC3[I].I := 0;
    end;
  end else
  begin
    //Vul invoersignalen met samples van de microfoon.
    for I := 0 to ASIOHost.BufferSize - 1 do
    begin
      ComplexInputSignal[I].R := InBuffer[0][I];
      ComplexInputSignalRLC1[I].R := InBuffer[0][I];
      ComplexInputSignalRLC2[I].R := InBuffer[0][I];
      ComplexInputSignalRLC3[I].R := InBuffer[0][I];
      ComplexInputSignal[I].I := 0;
      ComplexInputSignalRLC1[I].I := 0;
      ComplexInputSignalRLC2[I].I := 0;
      ComplexInputSignalRLC3[I].I := 0;
    end;
  end;

  //Filter de signalen
  RLC1.RunThroughFilter(ComplexInputSignalRLC1);
  RLC2.RunThroughFilter(ComplexInputSignalRLC2);
  RLC3.RunThroughFilter(ComplexInputSignalRLC3);

  {
    De amplitudes van de signalen berekenen. De signalen worden iets gemiddeld
    zodat de gemeten waardes iets stabieler blijven.
  }
  RLC1OutputAmplitude := (3 * RLC1OutputAmplitude +
    CalculateMaxAmplitude(ComplexInputSignalRLC1)) / 4;
  RLC2OutputAmplitude := (3 * RLC2OutputAmplitude +
    CalculateMaxAmplitude(ComplexInputSignalRLC2)) / 4;
  RLC3OutputAmplitude := (3 * RLC3OutputAmplitude +
    CalculateMaxAmplitude(ComplexInputSignalRLC3)) / 4;

  with frmFilteringSettings do
  begin
    if chkListenToAudioInput.Checked then
    begin
      for I := 0 to ASIOHost.BufferSize - 1 do
      begin
        if rgpSourceToAnalyse.Items[rgpSourceToAnalyse.ItemIndex] =
          'RLC1' then
        begin
          OutBuffer[0][I] := ComplexInputSignalRLC1[I].R;
          OutBuffer[1][I] := ComplexInputSignalRLC1[I].R;
        end else if rgpSourceToAnalyse.Items[rgpSourceToAnalyse.ItemIndex] =
          'RLC2' then
        begin
          OutBuffer[0][I] := ComplexInputSignalRLC2[I].R;
          OutBuffer[1][I] := ComplexInputSignalRLC2[I].R;
        end else  if rgpSourceToAnalyse.Items[rgpSourceToAnalyse.ItemIndex] =
          'RLC3' then
        begin
          OutBuffer[0][I] := ComplexInputSignalRLC3[I].R;
          OutBuffer[1][I] := ComplexInputSignalRLC3[I].R;
        end else if rgpSourceToAnalyse.Items[rgpSourceToAnalyse.ItemIndex] =
          'Lijningang' then
        begin
          OutBuffer[0][I] := ComplexInputSignal[I].R;
          OutBuffer[1][I] := ComplexInputSignal[I].R;
        end;
      end;
    end else
    begin
      for I := 0 to ASIOHost.BufferSize - 1 do
      begin
        OutBuffer[0][I] := 0;
        OutBuffer[1][I] := 0;
      end;
    end;
  end;

  {
    Naar de CurrentSpectrumToDraw variabele worden telkens samples geschreven
    Als CurrentSpectrumToDraw 4069 samples bevat wordt deze overgeschreven op
    BufferedSpectrumToDraw en wordt de lengte van CurrentSpectrumToDraw weer op
    nul gezet.
  }
  with frmFilteringSettings do
  begin
    CurrentSpectrumToDrawLength := Length(SpectrumToDrawBuffer);

    if CurrentSpectrumToDrawLength + ASIOHost.BufferSize > 4096 then
      SetLength(SpectrumToDrawBuffer, 4096)
    else
      SetLength(SpectrumToDrawBuffer, CurrentSpectrumToDrawLength +
        ASIOHost.BufferSize);

    for I := CurrentSpectrumToDrawLength to
      Length(SpectrumToDrawBuffer) - 1 do
    begin
      if rgpSourceToAnalyse.Items[rgpSourceToAnalyse.ItemIndex] =
        'RLC1' then
        SpectrumToDrawBuffer[I] := ComplexInputSignalRLC1[I -
          CurrentSpectrumToDrawLength]
      else if rgpSourceToAnalyse.Items[rgpSourceToAnalyse.ItemIndex] =
        'RLC2' then
        SpectrumToDrawBuffer[I] := ComplexInputSignalRLC2[I -
          CurrentSpectrumToDrawLength]
      else if rgpSourceToAnalyse.Items[rgpSourceToAnalyse.ItemIndex] =
        'RLC3' then
        SpectrumToDrawBuffer[I] := ComplexInputSignalRLC3[I -
          CurrentSpectrumToDrawLength]
      else if rgpSourceToAnalyse.Items[rgpSourceToAnalyse.ItemIndex] =
        'Lijningang' then
        SpectrumToDrawBuffer[I] := ComplexInputSignal[I -
          CurrentSpectrumToDrawLength];
    end;

    if Length(SpectrumToDrawBuffer) = 4096 then
    begin
      EnterCriticalSection(SpectrumToDrawCriticalSection);

      for I := 0 to Length(SpectrumToDrawBuffer) - 1 do
        SpectrumToDraw[I] := SpectrumToDrawBuffer[I];

      LeaveCriticalSection(SpectrumToDrawCriticalSection);

      PostMessage(frmFilteringSettings.Handle,
        WM_UpdateAndRepaintInputSpectrum, 0, 0);

      {
        De code om drie pieken te herkennen uit het spectrum. We gebruiken
        een simpel maar effectief stukje code. We lopen het spectrum drie keer
        door op zoek naar de hoogste piek piek. Voor de tweede keer slaan we
        het gebied van 100 hertz rond de eerste piek over bij het zoeken.
        Voor de tweede piek slaan we het gebied van 100 hertz rond zowel de
        eerste als tweede piek over.
      }
      FFT(SpectrumToDrawBuffer);

      HundredHertzSampleWidth := Trunc(100 * 4096 / ASIOHost.SampleRate);

      SignalMaxAmplitude := 0;
      PeakMaxAmplitudeFrequency := 0;
      PeakSecondMaxAmplitudeFrequency := 0;
      PeakThirdMaxAmplitudeFrequency := 0;
      for I := 0 to (Length(SpectrumToDrawBuffer) div 2) - 1 do
      begin
        CurrentPointAmplitude := TComplexMath.ComplexAbs(
          SpectrumToDrawBuffer[I]);
        if CurrentPointAmplitude > SignalMaxAmplitude then
        begin
          SignalMaxAmplitude := CurrentPointAmplitude;
          PeakMaxAmplitudeFrequency := I;
        end;
      end;

      SignalMaxAmplitude := 0;
      for I := 0 to (Length(SpectrumToDrawBuffer) div 2) - 1 do
      begin
        if Abs(I - PeakMaxAmplitudeFrequency) < HundredHertzSampleWidth then
          Continue;
        CurrentPointAmplitude := TComplexMath.ComplexAbs(
          SpectrumToDrawBuffer[I]);
        if CurrentPointAmplitude > SignalMaxAmplitude then
        begin
          SignalMaxAmplitude := CurrentPointAmplitude;
          PeakSecondMaxAmplitudeFrequency := I;
        end;
      end;

      SignalMaxAmplitude := 0;
      for I := 0 to (Length(SpectrumToDrawBuffer) div 2) - 1 do
      begin
        if (Abs(I - PeakMaxAmplitudeFrequency) < HundredHertzSampleWidth) or
           (Abs(I - PeakSecondMaxAmplitudeFrequency) < HundredHertzSampleWidth)
           then
          Continue;
        CurrentPointAmplitude := TComplexMath.ComplexAbs(
          SpectrumToDrawBuffer[I]);
        if CurrentPointAmplitude > SignalMaxAmplitude then
        begin
          SignalMaxAmplitude := CurrentPointAmplitude;
          PeakThirdMaxAmplitudeFrequency := I;
        end;
      end;

      SetLength(SpectrumToDrawBuffer, 0);

      PeakMaxAmplitudeFrequency := PeakMaxAmplitudeFrequency *
        (ASIOHost.SampleRate / 4096);
      PeakSecondMaxAmplitudeFrequency := PeakSecondMaxAmplitudeFrequency *
        (ASIOHost.SampleRate / 4096);
      PeakThirdMaxAmplitudeFrequency := PeakThirdMaxAmplitudeFrequency *
        (ASIOHost.SampleRate / 4096);

      {
        We willen dat de FrequencySpectrumPeakxFrequency variabelen altijd van
        lage naar hoge frequentie lopen.
      }
      FrequencySpectrumPeak1Frequency :=
        (1 * FrequencySpectrumPeak1Frequency +
          Min(Min(PeakMaxAmplitudeFrequency,
            PeakSecondMaxAmplitudeFrequency),
              PeakThirdMaxAmplitudeFrequency)) / 2;

      FrequencySpectrumPeak2Frequency :=
        (1 * FrequencySpectrumPeak2Frequency +
          PeakMaxAmplitudeFrequency + PeakSecondMaxAmplitudeFrequency +
          PeakThirdMaxAmplitudeFrequency - (Min(Min(PeakMaxAmplitudeFrequency,
          PeakSecondMaxAmplitudeFrequency), PeakThirdMaxAmplitudeFrequency) +
          Max(Max(PeakMaxAmplitudeFrequency, PeakSecondMaxAmplitudeFrequency),
          PeakThirdMaxAmplitudeFrequency))
        ) / 2;

      FrequencySpectrumPeak3Frequency :=
        (1 * FrequencySpectrumPeak3Frequency +
          Max(Max(PeakMaxAmplitudeFrequency, PeakSecondMaxAmplitudeFrequency),
            PeakThirdMaxAmplitudeFrequency)) / 2;
    end;
  end;
end;

procedure TfrmLocaQuarium.AsioHostSampleRateChanged(Sender: TObject);
begin
  BufferedTestSetupAudioPlayer.SampleRate := ASIOHost.SampleRate;
end;

procedure TfrmLocaQuarium.btnChangeFilteringSettingsClick(Sender: TObject);
begin
  frmFilteringSettings.Show;
end;

procedure TfrmLocaQuarium.btnStartAnalyzingClick(Sender: TObject);
begin
  if btnStartAnalyzing.Caption = 'Start LocaQuarium' then
  begin
    {
      Het control panel mag disabled worden. De simulatie mogelijkheid moet
      enabled worden. De timer die de gemeten waardes in frmFilteringSettings
      bijwerkt moet ook enabled worden.
    }
    with frmFilteringSettings do
    begin
      btnControlPanel.Enabled := False;

      chkSimulateTestSetup.Enabled := True;
      chkSimulateTestSetup.OnClick(Nil);

      tmrUpdateRLCValues.Enabled := True;
    end;

    {
      De filters moeten de samplerate van de ASIOHost weten, omdat anders
      de impulsrespons niet kan worden berekend.
    }
    RLC1.DSPSampleRate := Round(ASIOHost.SampleRate);
    RLC2.DSPSampleRate := Round(ASIOHost.SampleRate);
    RLC3.DSPSampleRate := Round(ASIOHost.SampleRate);

    {
      Buffer voor tekenen invoersignaal resetten.
    }
    SetLength(SpectrumToDrawBuffer, 0);

    {
      Tenslotte kan het ASIOHost component worden geactiveerd en kan de
      rendering thread resumed worden.
    }
    ASIOHost.Active := True;
    LocaQuariumRenderer.Resume;

    btnStartAnalyzing.Caption := 'Pauzeer LocaQuarium';
  end else
  begin
    {
      ASIOHost en rendering deactiveren.
    }
    LocaQuariumRenderer.Suspend;
    ASIOHost.Active := False;

    with frmFilteringSettings do
    begin
      btnControlPanel.Enabled := True;

      chkSimulateTestSetup.Enabled := False;
      chkSimulateTestSetup.OnClick(Nil);
    end;

    btnStartAnalyzing.Caption := 'Start LocaQuarium';
  end;
end;

procedure TfrmLocaQuarium.btnChangeRenderingSettingsClick(Sender: TObject);
begin
  frmRenderingSettings.Show;
end;

procedure TfrmLocaQuarium.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if btnStartAnalyzing.Caption = 'Pauzeer LocaQuarium' then
    btnStartAnalyzing.Click;

  SaveSettings;
end;

procedure TfrmLocaQuarium.FormCreate(Sender: TObject);
begin
  {
    Initialiseren van variabelen en het laden van instellingen van de filters
    uit het LocaQuarium.INI bestand.
  }
  RLC1 := TRLCBandpass.Create(1, 1, 512, 44100);
  RLC2 := TRLCBandpass.Create(1, 1, 512, 44100);
  RLC3 := TRLCBandpass.Create(1, 1, 512, 44100);

  SimulateTestSetup := False;

  BufferedTestSetupAudioPlayer := TBufferedAudioFilePlayer.Create;
  with BufferedTestSetupAudioPlayer do
  begin
    Pitch := 0;
    Interpolation := biBSpline6Point5thOrder;
    BufferSize := 65536;
    BlockSize  := 4096
  end;

  SetLength(SpectrumToDrawBuffer, 0);
  SetLength(SpectrumToDraw, 4096);
  InitializeCriticalSection(SpectrumToDrawCriticalSection);

  LoadSettings;

  //Het initialiseren van OpenGL.
  LocaQuariumRenderer := TLocaQuariumRenderer.Create(True);
  try
    InitOpenGL;
  except on E: Exception do
    begin
      Application.MessageBox(PChar(E.Message), 'Fout bij initializeren OpenGL',
        MB_ICONERROR);
      Application.MessageBox('OpenGL kan niet worden geinitializeerd. ' +
        'De mogelijkheden voor een 3D visualisatie zijn uitgeschakeld.',
        'Fout bij initializeren OpenGL', MB_ICONASTERISK);
    end;
  end; 
end;

procedure TfrmLocaQuarium.FormDestroy(Sender: TObject);
begin
  RLC1.Free;
  RLC2.Free;
  RLC3.Free;

  DeleteCriticalSection(SpectrumToDrawCriticalSection);

  LocaQuariumRenderer.Terminate;
  LocaQuariumRenderer.Free;
end;

procedure TfrmLocaQuarium.FormMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  //Inzoomen via het scrollwiel van de muis.
  with LocaQuariumRenderer do
  begin
    AquariumScalingFactor := AquariumScalingFactor / 1.2;
  end;
end;

procedure TfrmLocaQuarium.FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
  //Uitzoomen via het scrollwiel van de muis.
  with LocaQuariumRenderer do
  begin
    AquariumScalingFactor := AquariumScalingFactor * 1.2;
  end;
end;

procedure TfrmLocaQuarium.FormResize(Sender: TObject);
begin
  LocaQuariumRenderer.InvokeResizePerspective;
end;

procedure TfrmLocaQuarium.FormShow(Sender: TObject);
begin
  with frmFilteringSettings do
  begin
    DriverCombo.Items := ASIOHost.DriverList;
    DriverCombo.ItemIndex := 0;
    DriverCombo.OnChange(DriverCombo);
  end;
end;

function wglGetProcAddress(proc: PChar): Pointer; stdcall; external
  'OpenGL32.dll';

{
  Deze functie controleert of een WGL extension beschikbaar is.
}
function WGLExtensionIsSupported(Extension: String): Boolean;
var
  wglGetExtString: function(hdc: HDC): PChar; stdcall;
  Supported: PChar;
begin
  wglGetExtString := nil;
  Supported := nil;

  wglGetExtString := wglGetProcAddress('wglGetExtensionsStringARB');

  if Assigned(wglGetExtString) then
    Supported := wglGetExtString(wglGetCurrentDC);

  if Supported = Nil then
    Supported := glGetString(GL_EXTENSIONS);

  if Supported = Nil then
  begin
    Result := False;
    Exit;
  end;

  if Pos(Extension, Supported) = 0 then
  begin
    Result := False;
    Exit;
  end;

  Result := True;
end;

{
  Deze functie initialiseert de OpenGL rendering.
}
procedure TfrmLocaQuarium.InitOpenGL;
type
  TGLLongArray = array[0..21] of GLInt;
  PGLLongArray = ^TGLLongArray;
  
var
  fAttributes: Array of GLfloat;
  iAttributes: TGLLongArray;
  MultiSamplingIsSupported: Boolean;
  MultiSamplingPixelFormat: GLUInt;
  NumPixelFormats: UInt;
  PFD: PixelFormatDescriptor;
  PixelFormat: GLUInt;
  PixelFormatValid: Boolean;
  TmpWindow: TForm;
  wglChoosePixelFormatARB: function(hDC: HDC; const piAttribIList: PGLLongArray;
    const pfAttribFList: PGLfloat; nMaxFormats: GLuint; piFormats:
      PGLint; nNumFormats: PGLuint): BOOL; stdcall;

const
  CHECK_FOR_MULTISAMPLE = true;
  WGL_SAMPLE_BUFFERS_ARB = $2041;
  WGL_SAMPLES_ARB	= $2042;
  WGL_DRAW_TO_WINDOW_ARB = $2001;
  WGL_SUPPORT_OPENGL_ARB = $2010;
  WGL_ACCELERATION_ARB = $2003;
  WGL_FULL_ACCELERATION_ARB = $2027;
  WGL_COLOR_BITS_ARB = $2014;
  WGL_ALPHA_BITS_ARB = $201B;
  WGL_DEPTH_BITS_ARB = $2022;
  WGL_STENCIL_BITS_ARB = $2023;
  WGL_DOUBLE_BUFFER_ARB = $2011;
  
begin
  {
    We moeten enkele stappen doorlopen om OpenGL te initializeren:

    1. We moeten de handle van de device context verkrijgen van de plek waarop
       we gaan renderen.

    Een device context is een Windows-datastructuur met informatie over de
    kenmerken van een apparaat zoals een beeldscherm of een printer.
    Alle aanroepen naar tekencommando's worden gemaakt door middel van een
    device-context-object, dat de Windows-API's inkapselt voor het tekenen van
    lijnen, vormen en tekst. (Zie MSDN voor meer informatie)

    2. We moeten een pixelformaat kiezen voor de device context

    Een pixel formaat bevat een beschrijving van de manier waarop kleuren worden
    weergegeven op een tekengebied. Een pixelformaat wordt bijgehouden in het
    PixelFormatDescriptor record. De functie ChoosePixelFormat geeft een
    integer terug. Elk door de videohardware ondersteund pixelformaat heeft een
    nummer en het nummer van ons pixelformaat verwijst naar een pixelformaat
    die aan de beschrijving van onze PIXELFORMATDESCRIPTOR voldoet. Mits het
    pixelformaat beschikbaar is proberen we het te zetten door de functie
    SetPixelFormat aan te roepen.

    3. We moeten een OpenGL rendering context verkrijgen door middel van
       de functie wglCreateContext

    Dit zijn in principe alle stappen benodigd om OpenGL te initialiseren.
    Wanneer wij echter iets willen weergeven moeten we eerst de OpenGL rendering
    activeren op de huidige thread. Dit kan door middel van een aanroep
    naar wglMakeCurrent.

    Wij willen echter OpenGL initialiseren met de WGL_ARB_multisample extensie
    die voor anti-aliasing zorgt, zodat het resultaat van de rendering er wat
    mooier uit ziet.

    Niet voor alle videokaarten is deze multisampling extensie beschikbaar.
    Wanneer we de extensie gebruiken moeten we OpenGL met een pixelformaat
    initialiseren die mogelijkheid tot multisampling biedt. Hiervoor moeten we
    dus eerst controleren of de extensie beschikbaar is.

    We kunnen echter pas controleren of de extensie beschikbaar is als we
    OpenGL hebben geinitialiseerd. We moeten dus OpenGL tweemaal initialiseren.
    Eerst moeten we een tijdelijk OpenGL venster creëren. Als we OpenGL
    activeren op de handle van dit venster kunnen we daarna controleren of
    de extensie beschikbaar is.

    Hierna sluiten we OpenGL weer op dit tijdelijke venster en
    verwijderen we het venster uit het geheugen. Nu kunnen we OpenGL
    initialiseren op pnlLocaQuarium3D, met mits beschikbaar de multisampling
    extensie geactiveerd.

    We roepen voor de tweede initializatie van OpenGL niet wglMakeCurrent aan
    omdat de rendering uiteindelijk zal worden afgehandeld door de
    LocaQuariumRenderer thread in ULocaQuariumRenderer.
  }

  TmpWindow := TForm.Create(Nil);

  try
    h_DCPanel := GetDC(TmpWindow.Handle);
    if h_DCPanel = 0 then
    begin
      Raise Exception.Create(
        'Er is een fout opgetreden bij het laden van de DC.');
    end;

    with PFD do
    begin
      nSize := SizeOf(PIXELFORMATDESCRIPTOR);
      nVersion := 1;
      dwFlags := PFD_DRAW_TO_WINDOW
        or PFD_SUPPORT_OPENGL
        or PFD_DOUBLEBUFFER;
      iPixelType := PFD_TYPE_RGBA;
      cColorBits := 24;
      cRedBits := 0;
      cRedShift := 0;
      cGreenBits := 0;
      cBlueBits := 0;
      cBlueShift := 0;
      cAlphaBits := 0;
      cAlphaShift := 0;
      cAccumBits := 0;
      cAccumRedBits := 0;
      cAccumGreenBits := 0;
      cAccumBlueBits := 0;
      cAccumAlphaBits := 0;
      cDepthBits := 16;
      cStencilBits := 0;
      cAuxBuffers := 0;
      iLayerType := PFD_MAIN_PLANE;
      bReserved := 0;
      dwLayerMask := 0;
      dwVisibleMask := 0;
      dwDamageMask := 0;
    end;

    PixelFormat := ChoosePixelFormat(h_DCPanel, @PFD);

    if (PixelFormat = 0) then
    begin
      Raise Exception.Create(
        'Er is een fout opgetreden bij het kiezen van een pixel formaat.');
      Exit;
    end;

    if (not SetPixelFormat(h_DCPanel, PixelFormat, @PFD)) then
    begin
      Raise Exception.Create(
        'Er is een fout opgetreden bij het zetten van het pixel formaat.');
      Exit;
    end;

    h_RC := wglCreateContext(h_DCPanel);
    if (h_RC = 0) then
    begin
      Raise Exception.Create(
        'Er is een fout opgetreden bij de aanmaak van de OpenGL rendering ' +
        'context.');
      Exit;
    end;

    if (not wglMakeCurrent(h_DCPanel, h_RC)) then
      begin
        Raise Exception.Create(
          'Er is een fout opgetreden bij het activeren van de OpenGL rendering ' +
          'context.');
        Exit;
      end;

    MultiSamplingIsSupported := True;
    wglChoosePixelFormatARB := wglGetProcAddress('wglChoosePixelFormatARB');
    if WGLExtensionIsSupported('WGL_ARB_multisample') and
       Assigned(wglChoosePixelFormatARB) then
    begin
      SetLength(fAttributes,2);
      fAttributes[0] := 0;
      fAttributes[1] := 0;

      iAttributes[0] := WGL_DRAW_TO_WINDOW_ARB;
      iAttributes[1] := 1;
      iAttributes[2] := WGL_SUPPORT_OPENGL_ARB;
      iAttributes[3] := 1;
      iAttributes[4] := WGL_ACCELERATION_ARB;
      iAttributes[5] := WGL_FULL_ACCELERATION_ARB;
      iAttributes[6] := WGL_COLOR_BITS_ARB;
      iAttributes[7] := 24;
      iAttributes[8] := WGL_ALPHA_BITS_ARB;
      iAttributes[9] := 8;
      iAttributes[10] := WGL_DEPTH_BITS_ARB;
      iAttributes[11] := 16;
      iAttributes[12] := WGL_STENCIL_BITS_ARB;
      iAttributes[13] := 0;
      iAttributes[14] := WGL_DOUBLE_BUFFER_ARB;
      iAttributes[15] := 1;
      iAttributes[16] := WGL_SAMPLE_BUFFERS_ARB;
      iAttributes[17] := 1;
      iAttributes[18] := WGL_SAMPLES_ARB;
      {
        Deze integer geeft aan van welke graad van multisampling we gebruik
        willen maken. We proberen eerst 4x multisampling.
      }
      iAttributes[19] := 4;
      iAttributes[20] := 0;
      iAttributes[21] := 0;

      PixelFormatValid := wglChoosePixelFormatARB(h_DCPanel, @iAttributes,
        @fAttributes, 1, @MultiSamplingPixelFormat, @NumPixelFormats);
      if not (PixelFormatValid and (NumPixelFormats >= 1)) then
      begin
        {
          Multisampling was, tenminste op de 4x graad, niet beschikbaar. We
          proberen nu 2x multisampling te initialiseren.
        }

        iAttributes[19] := 2;
        PixelFormatValid := wglChoosePixelFormatARB(h_DCPanel, @iAttributes,
          @fAttributes, 1, @MultiSamplingPixelFormat, @NumPixelFormats);

        if PixelFormatValid and (NumPixelFormats >= 1) then
          MultiSamplingIsSupported := True
        else
          MultiSamplingIsSupported := False;
      end;

      {
        Mits multisampling ondersteund is kunnen we het pixelformaat veranderen
        naar een pixelformaat die antialiasing ondersteund.
      }
      if MultiSamplingIsSupported then
        PixelFormat := MultiSamplingPixelFormat;

      SetLength(fAttributes, 0);

    end else
      MultiSamplingIsSupported := False;

    wglDeleteContext(h_RC);
    ReleaseDC(TmpWindow.Handle, h_DCPanel);
  finally
    TmpWindow.Free;
  end;

  //We kunnen OpenGL nu op pnlLocaQuarium3D initialiseren
  h_DCPanel := GetDC(pnlLocaQuarium3D.Handle);
  if h_DCPanel = 0 then
  begin
    Raise Exception.Create(
      'Er is een fout opgetreden bij het laden van de DC.');
  end;

  if (not SetPixelFormat(h_DCPanel, PixelFormat, @PFD)) then
  begin
    Raise Exception.Create(
      'Er is een fout opgetreden bij het zetten van het pixel formaat.');
    Exit;
  end;

  h_RC := wglCreateContext(h_DCPanel);
  if (h_RC = 0) then
  begin
    Raise Exception.Create(
      'Er is een fout opgetreden bij de aanmaak van de OpenGL rendering ' +
      'context.');
    Exit;
  end;
end;

end.
