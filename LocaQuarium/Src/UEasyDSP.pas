unit UEasyDSP;

interface

uses
  ComplexMath, Math, SysUtils;

type
  //De TConvolutionEngine klasse is een klasse die door enig object kan worden
  //gebruikt om een signaal te convolueren met een impulse response.
  TConvolutionEngine = class(TObject)
  private
    //Hier wordt de tijdelijke overlap van de vorige convolutie in bewaard,
    //volgens de convolution overlap-add methode.
    ConvolutedSignalOverlay: TComplexNumberArray;
    lImpulseResponse: TComplexNumberArray;
  public
    procedure ConvoluteSignalOverlapAdd(var Signal: TComplexNumberArray);
    procedure SetImpulseResponse(ImpulseResponse: TComplexNumberArray);
    property ImpulseResponse: TComplexNumberArray read lImpulseResponse
      write SetImpulseResponse;
  end;

//Het FFT algoritme dat meermaals op verscheidene plekken in de code wordt
//aangeroepen
procedure FFT(Signal: TComplexNumberArray; Inverse: Boolean = False);

implementation

procedure TConvolutionEngine.ConvoluteSignalOverlapAdd(
  var Signal: TComplexNumberArray);
var
  I: Integer;
  ImpulseResponseCopy: TComplexNumberArray;
  SignalLength: Integer;
  SignalPaddedWithZeros: TComplexNumberArray;
begin
  //Het uiteindelijke signaal van de convolutie is lengte impulse response +
  //lengte invoer - 1. Daarom willen wij altijd een impulse response hebben van
  //lengte 2^x + 1. Het invoersignaal moet dan de lengte hebben 2^x.
  SignalLength := Length(Signal);

  //Omdat we de code niet al te ingewikkeld hebben willen maken hebben we er
  //voor gekozen er altijd van uit te gaan dat de lengte van het invoer signaal
  //gelijk is aan de lengte van de impulse response - 1. Aangezien de lengte
  //van de impulse response altijd een macht van 2 is plus ��n kunnen we op
  //die manier gemakkelijk de signalen met nullen padden en het FFT convolution
  //overlap-add algoritme gebruiken.
  if (SignalLength <> (Length(lImpulseResponse) / 2)) then
    Raise Exception.Create('Signal length must be the length of the impulse ' +
      'response minus one!');

  SetLength(SignalPaddedWithZeros, SignalLength * 2);
  for I := 0 to SignalLength - 1 do
    SignalPaddedWithZeros[I] := Signal[I];

  //Het invoersignaal wordt met nullen gepad om ruimte te maken voor het
  //uiteindelijk berekende uitvoersignaal
  for I := SignalLength to SignalLength * 2 - 1 do
  begin
    SignalPaddedWithZeros[I].R := 0;
    SignalPaddedWithZeros[I].I := 0;
  end;

  FFT(SignalPaddedWithZeros);

  //Frequentie spectra van de impulse response en het invoersignaal worden
  //vermenigvuldigd met elkaar.
  for I := 0 to 2 * SignalLength - 1 do
    SignalPaddedWithZeros[I] :=
      TComplexMath.Multiply(
        SignalPaddedWithZeros[I],
        lImpulseResponse[I]
      );

  FFT(SignalPaddedWithZeros, True);

  //Hetgeen de vorige keer niet op het uitvoersignaal paste, zich bevindend in
  //ConvolutedSignalOverlay, wordt bij de huidige output opgeteld.
  for I := 0 to SignalLength - 1 do
    Signal[I] := TComplexMath.Add(SignalPaddedWithZeros[I],
      ConvolutedSignalOverlay[I]);

  //Hetgeen niet op het uitvoersignaal past, wordt gekopieerd naar
  //ConvolutedSignalOverlay
  for I := SignalLength to SignalLength * 2 - 1 do
    ConvolutedSignalOverlay[I - SignalLength] := SignalPaddedWithZeros[I];

  SetLength(SignalPaddedWithZeros, 0);
end;

procedure TConvolutionEngine.SetImpulseResponse(
  ImpulseResponse: TComplexNumberArray);
var
  I: Integer;
  SignalLength: Integer;
begin
  SignalLength := Length(ImpulseResponse);

  //Lengte moet een macht van 2 zijn, plus ��n
  if (Frac(Log2(SignalLength - 1)) = 0) and (SignalLength > 0) then
  begin
    self.lImpulseResponse := ImpulseResponse;
    SetLength(self.lImpulseResponse, 2 * (SignalLength - 1));

    for I := SignalLength to (SignalLength - 1) * 2 - 1 do
    begin
      self.lImpulseResponse[I].R := 0;
      self.lImpulseResponse[I].I := 0;
    end;

    FFT(self.lImpulseResponse);

    //De lengte van ConvolutedSignalOverlay is nu bekend
    SetLength(ConvolutedSignalOverlay, SignalLength - 1);

    for I := 0 to SignalLength - 1 do
    begin
      ConvolutedSignalOverlay[I].R := 0;
      ConvolutedSignalOverlay[I].I := 0;
    end;
  end else
    Raise Exception.Create('Impulse response must be power of two plus one!');
end;

procedure FFT(Signal: TComplexNumberArray; Inverse: Boolean = False);

//Onze FFT procedure. We hebben geprobeerd het FFT algoritme zo duidelijk
//mogelijk in code weer te geven.

function ReverseBits (Input, SignalLength: Integer): Integer;
var
  I: Integer;
begin
  Result := 0;

  for I := 0 to Round(Log2(SignalLength)) - 1 do
  begin
    Result := (Result shl 1) or (Input and $01);
    Input := Input shr 1;
  end;
end;

var
  ExponentialPowerResult: TComplexNumber;
  I, Y, Z: Integer;
  IReversed: Integer;
  TempComplex, TempRightSignal: TComplexNumber;
begin
  if Length(Signal) = 0 then
    Exit;

  //Als de inverse wordt genomen krijgen we niet -iwt maar iwt, dus
  //we vermenigvuldigen het imaginaire deel van het signaal met -1.
  if Inverse then
  begin
    for I := 0 to Length(Signal) - 1 do
    begin
      Signal[I].I := -Signal[I].I;
    end;
  end;

  //De sortering op even en oneven. Hier zijn veel snellere algoritmen voor,
  //maar deze zijn ook veel onduidelijker
  for I := 0 to Length(Signal) - 1 do
  begin
    IReversed := ReverseBits(I, Length(Signal));

    if IReversed > I then
    begin
      TempComplex := Signal[I];
      Signal[I] := Signal[IReversed];
      Signal[IReversed] := TempComplex;
    end;
  end;

  //Het uitvoeren van de butterfly structuren
  I := 1;
  while I < Length(Signal) do
  begin
    Y := 0;
    while Y < I do
    begin
      ExponentialPowerResult.R := Cos(Pi * Y / I);
      ExponentialPowerResult.I := -1.0 * Sin(Pi * Y / I);

      Z := 0;
      while Z < Length(Signal) do
      begin
        TempRightSignal := TComplexMath.Multiply(Signal[Z + Y + I],
          ExponentialPowerResult);
        Signal[Z + Y + I] := TComplexMath.Add(
          TComplexMath.Multiply(TempRightSignal, -1), Signal[Z + Y]);
        Signal[Z + Y] := TComplexMath.Add(Signal[Z + Y], TempRightSignal);

        Z := Z + I * 2;
      end;

      Y := Y + 1;
    end;

    I := I shl 1;
  end;

  //Als we de forward FFT nemen moeten we de schaalconstante 1/N hanteren.
  if not Inverse then
  begin
    for I := 0 to Length(Signal) - 1 do
    begin
      Signal[I] := TComplexMath.Multiply(Signal[I], 1 / Length(Signal));
    end;
  end;
end;

end.
