unit UFilteringSettings;

interface

uses
  ComplexMath, URLCBandpass;

type
  TAquariumAxis = (None, X, Y, Z);
  TFilterNum = (FNRLC1, FNRLC2, FNRLC3);

var
  //Variabelen voor het aquarium.
  AquariumDepth, AquariumHeight, AquariumWidth: Single;

  //De drie bandpassfilter objecten.
  RLC1, RLC2, RLC3: TRLCBandpass;

  //Variabelen voor de assen van de filters.
  RLC1Axis, RLC2Axis, RLC3Axis: TAquariumAxis;

  //Variabelen voor de ijking van de filters.
  RLC1SignalMinAmplitude, RLC2SignalMinAmplitude,
    RLC3SignalMinAmplitude: Single;
  RLC1SignalMaxAmplitude, RLC2SignalMaxAmplitude,
    RLC3SignalMaxAmplitude: Single;

  {
    De berekende amplituden. Deze worden berekend in de thread die de audio
    input verkrijgt, en worden daarna getoond door de respectieve labels
    op de correcte waarden te zetten in functie UpdateFiltersOutputAmplitudes.
  }
  RLC1OutputAmplitude,
    RLC2OutputAmplitude,
      RLC3OutputAmplitude: Single;

  {
    Pieken gezocht in het frequentie spectrum.
  }
  FrequencySpectrumPeak1Frequency,
    FrequencySpectrumPeak2Frequency,
      FrequencySpectrumPeak3Frequency: Single;

  function AxisIsAssigned(Axis: TAquariumAxis): Boolean;
  function CalcAmplitudeRMS(var Signal: TComplexNumberArray): Double;
  function CalcDistance(ARMS, AMin, AMax, AxisWidth: Single): Single;
  function CalcElectrodeDistanceRLC1: Single;
  function CalcElectrodeDistanceRLC2: Single;
  function CalcElectrodeDistanceRLC3: Single;
  function CalcElectrodeDistanceX: Single;
  function CalcElectrodeDistanceY: Single;
  function CalcElectrodeDistanceZ: Single;
  function GetOutputAmplitudeRMS(Filter: TFilterNum): Single;
  procedure SetSignalMaxAmplitude(SignalMaxAmplitude: Single;
    Filter: TFilterNum);
  procedure SetSignalMinAmplitude(SignalMinAmplitude: Single;
    Filter: TFilterNum);
  function StringToFilter(FilterNum: String): TFilterNum;

implementation

uses
  UfrmLocaQuarium;

function AxisIsAssigned(Axis: TAquariumAxis): Boolean;
begin
  if ((RLC1Axis = Axis) and (RLC1SignalMaxAmplitude > 0)) or
     ((RLC2Axis = Axis) and (RLC2SignalMaxAmplitude > 0)) or
     ((RLC3Axis = Axis) and (RLC3SignalMaxAmplitude > 0)) then
    Result := True
  else
    Result := False;
end;

function CalcAmplitudeRMS(var Signal: TComplexNumberArray): Double;
var
  I: Integer;
begin
  Result := 0;

  for I := 0 to Length(Signal) - 1 do
    Result := Result + Sqr(Signal[I].R);

  Result := Sqrt(Result / Length(Signal));
end;

function CalcDistance(ARMS, AMin, AMax, AxisWidth: Single): Single;
begin
  if ARMS < AMin then
    ARMS := AMin
  else if ARMS > AMax then
    ARMS := AMax;

  Result := (ARMS - AMin) / (AMax - AMin) * AxisWidth;
end;

function CalcElectrodeDistanceRLC1: Single;
var
  AxisWidth: Single;
begin
  if RLC1Axis = X then
    AxisWidth := AquariumWidth
  else if RLC1Axis = Y then
    AxisWidth := AquariumHeight
  else if RLC1Axis = Z then
    AxisWidth := AquariumDepth
  else if RLC1Axis = None then
  begin
    Result := 0;
    Exit;
  end;

  Result := CalcDistance(RLC1OutputAmplitude,
    RLC1SignalMinAmplitude, RLC1SignalMaxAmplitude, AxisWidth);
end;

function CalcElectrodeDistanceRLC2: Single;
var
  AxisWidth: Single;
begin
  if RLC2Axis = X then
    AxisWidth := AquariumWidth
  else if RLC2Axis = Y then
    AxisWidth := AquariumHeight
  else if RLC2Axis = Z then
    AxisWidth := AquariumDepth
  else if RLC2Axis = None then
  begin
    Result := 0;
    Exit;
  end;

  Result := CalcDistance(RLC2OutputAmplitude,
    RLC2SignalMinAmplitude, RLC2SignalMaxAmplitude, AxisWidth);
end;

function CalcElectrodeDistanceRLC3: Single;
var
  AxisWidth: Single;
begin
  if RLC3Axis = X then
    AxisWidth := AquariumWidth
  else if RLC3Axis = Y then
    AxisWidth := AquariumHeight
  else if RLC3Axis = Z then
    AxisWidth := AquariumDepth
  else if RLC3Axis = None then
  begin
    Result := 0;
    Exit;
  end;

  Result := CalcDistance(RLC3OutputAmplitude,
    RLC3SignalMinAmplitude, RLC3SignalMaxAmplitude, AxisWidth);
end;

function CalcElectrodeDistanceX: Single;
var
  XARMS, XAMin, XAMax: Single;
begin
  if RLC1Axis = X then
  begin
    XARMS := RLC1OutputAmplitude;
    XAMin := RLC1SignalMinAmplitude;
    XAMax := RLC1SignalMaxAmplitude;
  end else if RLC2Axis = X then
  begin
    XARMS := RLC2OutputAmplitude;
    XAMin := RLC2SignalMinAmplitude;
    XAMax := RLC2SignalMaxAmplitude;
  end else if RLC3Axis = X then
  begin
    XARMS := RLC3OutputAmplitude;
    XAMin := RLC3SignalMinAmplitude;
    XAMax := RLC3SignalMaxAmplitude;
  end else
  begin
    Result := 0;
    Exit;
  end;

  Result := CalcDistance(XARMS, XAMin, XAMax, AquariumWidth);
end;

function CalcElectrodeDistanceY: Single;
var
  YARMS, YAMin, YAMax: Single;
begin
  if RLC1Axis = Y then
  begin
    YARMS := RLC1OutputAmplitude;
    YAMin := RLC1SignalMinAmplitude;
    YAMax := RLC1SignalMaxAmplitude;
  end else if RLC2Axis = Y then
  begin
    YARMS := RLC2OutputAmplitude;
    YAMin := RLC2SignalMinAmplitude;
    YAMax := RLC2SignalMaxAmplitude;
  end else if RLC3Axis = Y then
  begin
    YARMS := RLC3OutputAmplitude;
    YAMin := RLC3SignalMinAmplitude;
    YAMax := RLC3SignalMaxAmplitude;
  end else
  begin
    Result := 0;
    Exit;
  end;

  Result := CalcDistance(YARMS, YAMin, YAMax, AquariumHeight);
end;


function CalcElectrodeDistanceZ: Single;
var
  ZARMS, ZAMin, ZAMax: Single;
begin
  if RLC1Axis = Z then
  begin
    ZARMS := RLC1OutputAmplitude;
    ZAMin := RLC1SignalMinAmplitude;
    ZAMax := RLC1SignalMaxAmplitude;
  end else if RLC2Axis = Z then
  begin
    ZARMS := RLC2OutputAmplitude;
    ZAMin := RLC2SignalMinAmplitude;
    ZAMax := RLC2SignalMaxAmplitude;
  end else if RLC3Axis = Z then
  begin
    ZARMS := RLC3OutputAmplitude;
    ZAMin := RLC3SignalMinAmplitude;
    ZAMax := RLC3SignalMaxAmplitude;
  end else
  begin
    Result := 0;
    Exit;
  end;

  Result := CalcDistance(ZARMS, ZAMin, ZAMax, AquariumDepth);
end;

function GetOutputAmplitudeRMS(Filter: TFilterNum): Single;
begin
  if Filter = FNRLC1 then
    Result := RLC1OutputAmplitude
  else if Filter = FNRLC2 then
    Result := RLC2OutputAmplitude
  else if Filter = FNRLC3 then
    Result := RLC3OutputAmplitude;
end;

procedure SetSignalMaxAmplitude(SignalMaxAmplitude: Single;
  Filter: TFilterNum);
begin
  if Filter = FNRLC1 then
    RLC1SignalMaxAmplitude := SignalMaxAmplitude
  else if Filter = FNRLC2 then
    RLC2SignalMaxAmplitude := SignalMaxAmplitude
  else if Filter = FNRLC3 then
    RLC3SignalMaxAmplitude := SignalMaxAmplitude;
end;

procedure SetSignalMinAmplitude(SignalMinAmplitude: Single;
  Filter: TFilterNum);
begin
  if Filter = FNRLC1 then
    RLC1SignalMinAmplitude := SignalMinAmplitude
  else if Filter = FNRLC2 then
    RLC2SignalMinAmplitude := SignalMinAmplitude
  else if Filter = FNRLC3 then
    RLC3SignalMinAmplitude := SignalMinAmplitude;
end;

function StringToFilter(FilterNum: String): TFilterNum;
begin
  if FilterNum = 'RLC1' then
    Result := FNRLC1
  else if FilterNum = 'RLC2' then
    Result := FNRLC2
  else if FilterNum = 'RLC3' then
    Result := FNRLC3;
end;

end.
