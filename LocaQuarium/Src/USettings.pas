unit USettings;

interface

  procedure LoadSettings;
  procedure SaveSettings;

implementation

uses
  Forms, INIFiles, SysUtils, UfrmFilteringSettings, UFilteringSettings,
    UfrmLocaQuarium, URenderingSettings;

procedure LoadSettings;

function StringToAxis(Axis: String): TAquariumAxis;
begin                                                                                   
  if Axis = 'X' then
    Result := X
  else if Axis = 'Y' then
    Result := Y
  else if Axis = 'Z' then
    Result := Z
  else
    Result := None;
end;

var
  AudioDriver: String;

  LocaQuariumSettingsINI: TINIFile;
  INIRLC1Axis, INIRLC2Axis, INIRLC3Axis: String;
begin
  try
    LocaQuariumSettingsINI := TIniFile.Create(
      ExtractFilePath(Application.ExeName) +
      'LocaQuarium.INI');



    AquariumDepth := LocaQuariumSettingsINI.ReadInteger('Aquarium',
      'AquariumDepth', 40);
    AquariumHeight := LocaQuariumSettingsINI.ReadInteger('Aquarium',
      'AquariumHeight', 27);
    AquariumWidth := LocaQuariumSettingsINI.ReadInteger('Aquarium',
      'AquariumWidth', 20);

    ShowAxes := LocaQuariumSettingsINI.ReadBool('Rendering', 'ShowAxes', True);
    ShowDistancesInLeftCorner := LocaQuariumSettingsINI.ReadBool('Rendering',
      'ShowDistancesInLeftCorner', True);
    ShowFPS := LocaQuariumSettingsINI.ReadBool('Rendering', 'ShowFPS', False);
    ShowPositionIndicators := LocaQuariumSettingsINI.ReadBool('Rendering',
      'ShowPositionIndicators', True);
    ShowSkybox := LocaQuariumSettingsINI.ReadBool('Rendering', 'ShowSkybox',
      True);

    INIRLC1Axis := LocaQuariumSettingsINI.ReadString('Filtering',
      'RLC1Axis', '');
    INIRLC2Axis := LocaQuariumSettingsINI.ReadString('Filtering',
      'RLC2Axis', '');
    INIRLC3Axis := LocaQuariumSettingsINI.ReadString('Filtering',
      'RLC3Axis', '');

    RLC1Axis := StringToAxis(INIRLC1Axis);
    RLC2Axis := StringToAxis(INIRLC2Axis);
    RLC3Axis := StringToAxis(INIRLC3Axis);

    RLC1.Frequency := LocaQuariumSettingsINI.ReadFloat('Filtering',
      'RLC1Frequency', 1);
    RLC2.Frequency := LocaQuariumSettingsINI.ReadFloat('Filtering',
      'RLC2Frequency', 1);
    RLC3.Frequency := LocaQuariumSettingsINI.ReadFloat('Filtering',
      'RLC3Frequency', 1);

    RLC1.QualityFactor := LocaQuariumSettingsINI.ReadFloat('Filtering',
      'RLC1QFactor', 1);
    RLC2.QualityFactor := LocaQuariumSettingsINI.ReadFloat('Filtering',
      'RLC2QFactor', 1);
    RLC3.QualityFactor := LocaQuariumSettingsINI.ReadFloat('Filtering',
      'RLC3QFactor', 1);

    RLC1SignalMinAmplitude := LocaQuariumSettingsINI.ReadFloat('Filtering',
      'RLC1MinAmplitude', 0);
    RLC2SignalMinAmplitude := LocaQuariumSettingsINI.ReadFloat('Filtering',
      'RLC2MinAmplitude', 0);
    RLC3SignalMinAmplitude := LocaQuariumSettingsINI.ReadFloat('Filtering',
      'RLC3MinAmplitude', 0);

    RLC1SignalMaxAmplitude := LocaQuariumSettingsINI.ReadFloat('Filtering',
      'RLC1MaxAmplitude', 0);
    RLC2SignalMaxAmplitude := LocaQuariumSettingsINI.ReadFloat('Filtering',
      'RLC2MaxAmplitude', 0);
    RLC3SignalMaxAmplitude := LocaQuariumSettingsINI.ReadFloat('Filtering',
      'RLC3MaxAmplitude', 0);

    LocaQuariumSettingsINI.Free;
  except
  end;
end;

procedure SaveSettings;

function AxisToString(Axis: TAquariumAxis): String;
begin
  if Axis = X then
    Result := 'X'
  else if Axis = Y then
    Result := 'Y'
  else if Axis = Z then
    Result := 'Z'
  else
    Result := 'None';
end;

var
  LocaQuariumSettingsINI: TINIFile;
begin
  try
    LocaQuariumSettingsINI := TIniFile.Create(
      ExtractFilePath(Application.ExeName) +
      'LocaQuarium.INI');

    //LocaQuariumSettingsINI.WriteString('Audio', 'AudioDriver',
      //frmLocaQuarium.);

    LocaQuariumSettingsINI.WriteInteger('Aquarium', 'AquariumDepth',
      Trunc(AquariumDepth));
    LocaQuariumSettingsINI.WriteInteger('Aquarium', 'AquariumHeight',
      Trunc(AquariumHeight));
    LocaQuariumSettingsINI.WriteInteger('Aquarium', 'AquariumWidth',
      Trunc(AquariumWidth));

    LocaQuariumSettingsINI.WriteBool('Rendering', 'ShowAxes', ShowAxes);
    LocaQuariumSettingsINI.WriteBool('Rendering', 'ShowDistancesInLeftCorner',
      ShowDistancesInLeftCorner);
    LocaQuariumSettingsINI.WriteBool('Rendering', 'ShowFPS', ShowFPS);
    LocaQuariumSettingsINI.WriteBool('Rendering', 'ShowPositionIndicators',
      ShowPositionIndicators);
    LocaQuariumSettingsINI.WriteBool('Rendering', 'ShowSkybox', ShowSkybox);

    LocaQuariumSettingsINI.WriteString('Filtering', 'RLC1Axis',
      AxisToString(RLC1Axis));
    LocaQuariumSettingsINI.WriteString('Filtering', 'RLC2Axis',
      AxisToString(RLC2Axis));
    LocaQuariumSettingsINI.WriteString('Filtering', 'RLC3Axis',
      AxisToString(RLC3Axis));

    LocaQuariumSettingsINI.WriteFloat('Filtering', 'RLC1Frequency',
      RLC1.Frequency);
    LocaQuariumSettingsINI.WriteFloat('Filtering', 'RLC2Frequency',
      RLC2.Frequency);
    LocaQuariumSettingsINI.WriteFloat('Filtering', 'RLC3Frequency',
      RLC3.Frequency);

    LocaQuariumSettingsINI.WriteFloat('Filtering', 'RLC1QFactor',
      RLC1.QualityFactor);
    LocaQuariumSettingsINI.WriteFloat('Filtering', 'RLC2QFactor',
      RLC2.QualityFactor);
    LocaQuariumSettingsINI.WriteFloat('Filtering', 'RLC3QFactor',
      RLC3.QualityFactor);

    LocaQuariumSettingsINI.WriteFloat('Filtering', 'RLC1MinAmplitude',
      RLC1SignalMinAmplitude);
    LocaQuariumSettingsINI.WriteFloat('Filtering', 'RLC2MinAmplitude',
      RLC2SignalMinAmplitude);
    LocaQuariumSettingsINI.WriteFloat('Filtering', 'RLC3MinAmplitude',
      RLC3SignalMinAmplitude);

    LocaQuariumSettingsINI.WriteFloat('Filtering', 'RLC1MaxAmplitude',
      RLC1SignalMaxAmplitude);
    LocaQuariumSettingsINI.WriteFloat('Filtering', 'RLC2MaxAmplitude',
      RLC2SignalMaxAmplitude);
    LocaQuariumSettingsINI.WriteFloat('Filtering', 'RLC3MaxAmplitude',
      RLC3SignalMaxAmplitude);

    LocaQuariumSettingsINI.Free;
  except
  end;
end;

end.
