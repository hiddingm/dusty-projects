﻿unit URLCBandpass;

interface

uses
  ComplexMath, UEasyDSP, Windows;

type
  //De RLC bandpass klasse. Deze berekend aan de hand van zijn variabelen
  //telkens een impulse response, die de klasse dan weer doorgeeft aan zijn
  //ConvolutionEngine.
  TRLCBandpass = Class(TObject)
  private
    //De ConvolutionEngine die de klasse gebruikt
    ConvolutionEngine: TConvolutionEngine;
    //Van deze critical section wordt gebruik gemaakt wanneer de impulse
    //response moet worden benaderd. Bedenk namelijk dat RunThroughFilter
    //wordt aangeroepen in een andere thread dan die van de GUI.
    FilterCriticalSection: TRTLCriticalSection;
    lAngularFrequency, lQualityFactor: Double;
    lDSPBufferSize, lDSPSampleRate: Integer;
    //RLCImpulseResponse: TComplexNumberArray;
    R, L, C: Double;
    function GetFrequency: Double;
    procedure ReCalculateRLCImpulseResponse;
    procedure SetDSPBufferSize(DSPBufferSize: Integer);
    procedure SetDSPSampleRate(DSPSampleRate: Integer);
    procedure SetFilterProperties(AngularFrequency, QualityFactor: Double;
      DSPBufferSize, DSPSampleRate: Integer);
    procedure SetFrequency(Frequency: Double);
    procedure SetQualityFactor(QualityFactor: Double);
  public
    RLCImpulseResponse: TComplexNumberArray;
    //De properties herleiden naar functies die weer herleiden tot een aanroep
    //naar SetFilterProperties.
    property DSPBufferSize: Integer read lDSPBufferSize write SetDSPBufferSize;
    property DSPSampleRate: Integer read lDSPSampleRate write SetDSPSampleRate;
    property Frequency: Double read GetFrequency write SetFrequency;
    property QualityFactor: Double read lQualityFactor write SetQualityFactor;
    constructor Create(Frequency, QualityFactor: Double; DSPBufferSize,
      DSPSampleRate: Integer);
    destructor Free;
    procedure RunThroughFilter(var Signal: TComplexNumberArray);
  end;

implementation

uses
  Dialogs, Math, SysUtils;

constructor TRLCBandpass.Create(Frequency, QualityFactor: Double; DSPBufferSize,
  DSPSampleRate: Integer);
begin
  InitializeCriticalSection(FilterCriticalSection);

  ConvolutionEngine := TConvolutionEngine.Create;

  SetFilterProperties(Frequency * 2 * Pi, QualityFactor, DSPBufferSize,
    DSPSampleRate);
end;

destructor TRLCBandpass.Free;
begin
  ConvolutionEngine.Free;

  DeleteCriticalSection(FilterCriticalSection);
end;

function TRLCBandpass.GetFrequency: Double;
begin
  Result := lAngularFrequency / (2 * Pi);
end;

procedure TRLCBandpass.SetDSPBufferSize(DSPBufferSize: Integer);
begin
  SetFilterProperties(lAngularFrequency, lQualityFactor, DSPBufferSize,
    lDSPSampleRate);
end;

procedure TRLCBandpass.SetDSPSampleRate(DSPSampleRate: Integer);
begin
  SetFilterProperties(lAngularFrequency, lQualityFactor, lDSPBufferSize,
    DSPSampleRate);
end;

procedure TRLCBandpass.SetFilterProperties(AngularFrequency,
  QualityFactor: Double; DSPBufferSize, DSPSampleRate: Integer);
begin
  EnterCriticalSection(FilterCriticalSection);

  if AngularFrequency <= 0 then
    AngularFrequency := 1;

  lAngularFrequency := AngularFrequency;

  if QualityFactor <= 0 then
    QualityFactor := 1;

  lQualityFactor := QualityFactor;

  if DSPBufferSize < 0 then
    DSPBufferSize := 1024;

  lDSPBufferSize := DSPBufferSize;

  if DSPSampleRate < 0 then
    DSPSampleRate := 44100;

  lDSPSampleRate := DSPSampleRate;

  ReCalculateRLCImpulseResponse;

  ConvolutionEngine.SetImpulseResponse(RLCImpulseResponse);

  LeaveCriticalSection(FilterCriticalSection);
end;

procedure TRLCBandpass.SetFrequency(Frequency: Double);
begin
  SetFilterProperties(Frequency * 2 * Pi, lQualityFactor, lDSPBufferSize,
    lDSPSampleRate);
end;

procedure TRLCBandpass.SetQualityFactor(QualityFactor: Double);
begin
  SetFilterProperties(lAngularFrequency, QualityFactor, lDSPBufferSize,
    lDSPSampleRate);
end;

procedure TRLCBandpass.ReCalculateRLCImpulseResponse;

function ReturnComplex(Real: Double): TComplexNumber;
begin
  Result.R := Real;
  Result.I := 0;
end;

var
  AngularFrequency: TComplexNumber;
  Attenuation: TComplexNumber;
  A1, A2, S1, S2: TComplexNumber;
  I: Integer;
begin
  SetLength(RLCImpulseResponse, DSPBufferSize + 1);

  //Vr=RC〖〖(A〗_1 S_1 e〗^(s_1 t)+〖A_2 S_2 e〗^(s_2 t))

  R := lAngularFrequency / lQualityFactor;
  L := 1;
  C := 1 / (Power(R, 2) * Power(lQualityFactor, 2));

  //Attenuation = R/2L
  Attenuation :=
    TComplexMath.Divide(
      ReturnComplex(R),
      TComplexMath.Multiply(
        ReturnComplex(L),
        ReturnComplex(2)
      )
    );

  //AngularFrequency = 1/√(LC)
  AngularFrequency :=
    TComplexMath.Divide(
      ReturnComplex(1),
      TComplexMath.SquareRoot(
        TComplexMath.Multiply(
          ReturnComplex(L),
          ReturnComplex(C)
        )
      )
    );

  //s1=-a+√(a²-ω_0^2)
  S1 :=
    TComplexMath.Add(
      TComplexMath.Multiply(Attenuation, ReturnComplex(-1)),
      TComplexMath.SquareRoot(
        TComplexMath.Subtract(
          TComplexMath.ComplexPower(Attenuation, ReturnComplex(2)),
          TComplexMath.ComplexPower(AngularFrequency, ReturnComplex(2))
        )
      )
    );

  //s2=-a-√(a²-ω_0^2)
  S2 :=
    TComplexMath.Add(
      TComplexMath.Multiply(Attenuation, ReturnComplex(-1)),
      TComplexMath.Multiply(
        TComplexMath.SquareRoot(
          TComplexMath.Add(
            TComplexMath.ComplexPower(Attenuation, ReturnComplex(2)),
            TComplexMath.Multiply(
              TComplexMath.ComplexPower(AngularFrequency, ReturnComplex(2)),
              ReturnComplex(-1)
            )
          )
        ),
        ReturnComplex(-1)
      )
    );

  //A_1=-(VsS_2)/(S_1-S_2 )-Vs
  A1 :=
    TComplexMath.Subtract(
      TComplexMath.Multiply(
        TComplexMath.Divide(
          S1,
          TComplexMath.Subtract(
            S2,
            S1
          )
        ),
        ReturnComplex(-1)
      ),
      ReturnComplex(1)
    );

  //A_2=(VsS_2)/(S_1-S_2 )
  A2 :=
    TComplexMath.Divide(
      S1,
      TComplexMath.Subtract(
        S2,
        S1
      )
    );

  {
    Let op niet dat wij niet DSPBufferSize - 1 gebruiken, omdat de lengte van de
    impulsrespons één groter moet zijn dan de lengte van de input naar de
    ConvolutionEngine klasse.
  }
  for I := 0 to DSPBufferSize do
  begin
    RLCImpulseResponse[I] :=
      TComplexMath.Multiply(
        TComplexMath.Multiply(ReturnComplex(R), ReturnComplex(C)),
        TComplexMath.Add(
          TComplexMath.Multiply(
            TComplexMath.Multiply(
              A1,
              TComplexMath.ComplexPower(S1, ReturnComplex(2))
            ),
            TComplexMath.ComplexExp(
              TComplexMath.Multiply(S1, ReturnComplex(I / lDSPSampleRate))
            )
          ),
          TComplexMath.Multiply(
            TComplexMath.Multiply(
              A2,
              TComplexMath.ComplexPower(S2, ReturnComplex(2))
            ),
            TComplexMath.ComplexExp(
              TComplexMath.Multiply(S2, ReturnComplex(I / lDSPSampleRate))
            )
          )
        )
      );
  end;
end;

procedure TRLCBandpass.RunThroughFilter(var Signal: TComplexNumberArray);
var
  I, Y: Integer;
  N: Integer;
  tSignalPart: TComplexNumberArray;
begin
  //De code die het invoersignaal naar de ConvoluteSignalOverlapAdd methode van
  //de ConvolutionEngine stuurt. Het invoersignaal wordt opgesplitst in delen
  //met de lengte van de buffer van de impulse response. Op die manier kan
  //elke macht van 2 worden geconvolueerd, zolang deze maar groter is dan
  //de waarde van lDSPBufferSize.

  SetLength(tSignalPart, lDSPBufferSize);

  N := Length(Signal) div lDSPBufferSize;

  for I := 0 to N - 1 do
  begin
    for Y := 0 to lDSPBufferSize - 1 do
      tSignalPart[Y] := Signal[I * lDSPBufferSize + Y];

    EnterCriticalSection(FilterCriticalSection);

    ConvolutionEngine.ConvoluteSignalOverlapAdd(tSignalPart);

    LeaveCriticalSection(FilterCriticalSection);

    for Y := 0 to lDSPBufferSize - 1 do
      Signal[I * lDSPBufferSize + Y] := tSignalPart[Y];
  end;

  SetLength(tSignalPart, 0);
end;

end.
