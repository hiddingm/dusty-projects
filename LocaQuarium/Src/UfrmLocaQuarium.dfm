object frmLocaQuarium: TfrmLocaQuarium
  Left = 0
  Top = 0
  Caption = 'LocaQuarium'
  ClientHeight = 405
  ClientWidth = 459
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnMouseWheelDown = FormMouseWheelDown
  OnMouseWheelUp = FormMouseWheelUp
  OnResize = FormResize
  OnShow = FormShow
  DesignSize = (
    459
    405)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox3: TGroupBox
    Left = 8
    Top = 335
    Width = 443
    Height = 62
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'Handelingen'
    TabOrder = 0
    DesignSize = (
      443
      62)
    object btnChangeFilteringSettings: TButton
      Left = 294
      Top = 21
      Width = 136
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Filtering instellen'
      TabOrder = 0
      OnClick = btnChangeFilteringSettingsClick
    end
    object btnChangeRenderingSettings: TButton
      Left = 152
      Top = 21
      Width = 136
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Visualisatie instellingen'
      TabOrder = 1
      OnClick = btnChangeRenderingSettingsClick
    end
    object btnStartAnalyzing: TButton
      Left = 13
      Top = 24
      Width = 133
      Height = 22
      Anchors = [akRight, akBottom]
      Caption = 'Start LocaQuarium'
      TabOrder = 2
      OnClick = btnStartAnalyzingClick
    end
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 0
    Width = 443
    Height = 329
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Visualisatie'
    TabOrder = 1
    DesignSize = (
      443
      329)
    object pnlLocaQuarium3D: TPanel
      Left = 8
      Top = 16
      Width = 426
      Height = 302
      Cursor = crHandPoint
      Anchors = [akLeft, akTop, akRight, akBottom]
      FullRepaint = False
      TabOrder = 0
    end
  end
  object AsioHost: TAsioHost
    AsioTime.Speed = 1.000000000000000000
    AsioTime.SampleRate = 44100.000000000000000000
    AsioTime.Flags = [atSystemTimeValid, atSamplePositionValid, atSampleRateValid, atSpeedValid]
    SampleRate = 44100.000000000000000000
    OnBufferSwitch32 = AsioHostBufferSwitch32
    OnSampleRateChanged = AsioHostSampleRateChanged
  end
end
