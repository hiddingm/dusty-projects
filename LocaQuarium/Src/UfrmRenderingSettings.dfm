object frmRenderingSettings: TfrmRenderingSettings
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'LocaQuarium Visualisatie Instellingen'
  ClientHeight = 114
  ClientWidth = 433
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 417
    Height = 97
    Caption = 'Instellingen rendering'
    TabOrder = 0
    object chkShowAxes: TCheckBox
      Left = 16
      Top = 23
      Width = 97
      Height = 17
      Caption = 'Toon assen'
      TabOrder = 0
      OnClick = chkShowAxesClick
    end
    object chkShowSkybox: TCheckBox
      Left = 200
      Top = 23
      Width = 97
      Height = 17
      Caption = 'Toon skybox'
      TabOrder = 1
      OnClick = chkShowSkyboxClick
    end
    object chkShowPositionIndicators: TCheckBox
      Left = 16
      Top = 46
      Width = 130
      Height = 17
      Caption = 'Toon positie indicators'
      TabOrder = 2
      OnClick = chkShowPositionIndicatorsClick
    end
    object chkShowDistancesInLeftCorner: TCheckBox
      Left = 200
      Top = 46
      Width = 137
      Height = 17
      Caption = 'Afstanden in linkerhoek'
      TabOrder = 3
      OnClick = chkShowDistancesInLeftCornerClick
    end
    object chkShowFPS: TCheckBox
      Left = 200
      Top = 69
      Width = 130
      Height = 17
      Caption = 'Toon FPS'
      TabOrder = 4
      OnClick = chkShowFPSClick
    end
  end
end
