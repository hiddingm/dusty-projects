object frmFilteringSettings: TfrmFilteringSettings
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Meetapparatuur instellen'
  ClientHeight = 686
  ClientWidth = 554
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox3: TGroupBox
    Left = 9
    Top = 343
    Width = 537
    Height = 334
    Caption = 'Instellingen filters'
    TabOrder = 0
    object tbcFilters: TTabControl
      Left = 8
      Top = 23
      Width = 512
      Height = 298
      TabOrder = 0
      Tabs.Strings = (
        'RLC1'
        'RLC2'
        'RLC3')
      TabIndex = 0
      OnChange = tbcFiltersChange
      object GroupBox7: TGroupBox
        Left = 16
        Top = 35
        Width = 481
        Height = 151
        Caption = 'Filter instellingen'
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 63
          Width = 59
          Height = 13
          Caption = 'Frequentie: '
        end
        object lblFrequency: TLabel
          Left = 73
          Top = 63
          Width = 18
          Height = 13
          Caption = '1Hz'
        end
        object Label3: TLabel
          Left = 16
          Top = 127
          Width = 46
          Height = 13
          Caption = 'Q Factor:'
        end
        object lblQFactor: TLabel
          Left = 81
          Top = 127
          Width = 6
          Height = 13
          Caption = '1'
        end
        object trbFrequency: TTrackBar
          Left = 3
          Top = 24
          Width = 383
          Height = 33
          Max = 20500
          Min = 1
          Frequency = 500
          Position = 1
          ShowSelRange = False
          TabOrder = 0
          OnChange = trbFrequencyChange
        end
        object trbQFactor: TTrackBar
          Left = 3
          Top = 88
          Width = 430
          Height = 33
          Max = 1000
          Min = 1
          Frequency = 25
          Position = 1
          ShowSelRange = False
          TabOrder = 1
          OnChange = trbQFactorChange
        end
        object edtFrequency: TEdit
          Left = 385
          Top = 32
          Width = 39
          Height = 21
          TabOrder = 2
          OnChange = edtFrequencyChange
          OnKeyPress = edtQFactorFrequencyKeyPress
        end
        object edtQFactor: TEdit
          Left = 430
          Top = 96
          Width = 39
          Height = 21
          TabOrder = 3
          OnChange = edtQFactorChange
          OnKeyPress = edtQFactorFrequencyKeyPress
        end
        object btnGetFrequencyFromPeak: TButton
          Left = 430
          Top = 29
          Width = 36
          Height = 25
          Caption = 'Piek 1'
          TabOrder = 4
          OnClick = btnGetFrequencyFromPeakClick
        end
      end
      object GroupBox8: TGroupBox
        Left = 16
        Top = 192
        Width = 481
        Height = 98
        Caption = 'IJking'
        TabOrder = 1
        object Label2: TLabel
          Left = 16
          Top = 24
          Width = 46
          Height = 13
          Caption = 'Dimensie:'
        end
        object Label5: TLabel
          Left = 183
          Top = 24
          Width = 142
          Height = 13
          Caption = 'Maximaal gemeten amplitude:'
        end
        object Label8: TLabel
          Left = 183
          Top = 70
          Width = 117
          Height = 13
          Caption = 'Gemeten signaalsterkte:'
        end
        object lblRLCOutputAmplitude: TLabel
          Left = 306
          Top = 70
          Width = 6
          Height = 13
          Caption = '0'
        end
        object Label9: TLabel
          Left = 16
          Top = 70
          Width = 95
          Height = 13
          Caption = 'Berekende afstand:'
        end
        object lblRLCOutputDerivedDistance: TLabel
          Left = 117
          Top = 69
          Width = 30
          Height = 16
          Caption = '0 cm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label13: TLabel
          Left = 331
          Top = 24
          Width = 138
          Height = 13
          Caption = 'Minimaal gemeten amplitude:'
        end
        object cmbFilterAxis: TComboBox
          Left = 16
          Top = 43
          Width = 154
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 0
          Text = 'Geen'
          OnChange = cmbFilterAxisChange
          Items.Strings = (
            'Geen'
            'X'
            'Y'
            'Z')
        end
        object edtCalibrationMaxAmplitude: TEdit
          Left = 183
          Top = 43
          Width = 58
          Height = 21
          TabOrder = 1
          OnChange = edtCalibrationMaxAmplitudeChange
        end
        object btnDetectCalibrationMaxAmplitude: TButton
          Left = 247
          Top = 39
          Width = 70
          Height = 25
          Caption = 'Aflezen'
          TabOrder = 2
          OnClick = btnDetectCalibrationMaxAmplitudeClick
        end
        object btnDetectCalibrationMinAmplitude: TButton
          Left = 391
          Top = 39
          Width = 70
          Height = 25
          Caption = 'Aflezen'
          TabOrder = 3
          OnClick = btnDetectCalibrationMinAmplitudeClick
        end
        object edtCalibrationMinAmplitude: TEdit
          Left = 331
          Top = 43
          Width = 54
          Height = 21
          TabOrder = 4
          OnChange = edtCalibrationMinAmplitudeChange
        end
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 143
    Width = 217
    Height = 194
    Caption = 'Afmetingen aquarium'
    TabOrder = 1
    object Label4: TLabel
      Left = 16
      Top = 24
      Width = 74
      Height = 13
      Caption = 'Breedte (X-as):'
    end
    object Label6: TLabel
      Left = 16
      Top = 51
      Width = 67
      Height = 13
      Caption = 'Diepte (Z-as):'
    end
    object Label7: TLabel
      Left = 16
      Top = 78
      Width = 71
      Height = 13
      Caption = 'Hoogte (Y-as):'
    end
    object Label10: TLabel
      Left = 180
      Top = 24
      Width = 21
      Height = 13
      Caption = '(cm)'
    end
    object Label11: TLabel
      Left = 180
      Top = 51
      Width = 21
      Height = 13
      Caption = '(cm)'
    end
    object Label12: TLabel
      Left = 180
      Top = 78
      Width = 21
      Height = 13
      Caption = '(cm)'
    end
    object lblRLC1Axis: TLabel
      Left = 15
      Top = 106
      Width = 66
      Height = 13
      Caption = 'Afstand x-as:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblXAxisDistance: TLabel
      Left = 104
      Top = 106
      Width = 51
      Height = 13
      Caption = '22.52 cm'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRLC2Axis: TLabel
      Left = 15
      Top = 125
      Width = 66
      Height = 13
      Caption = 'Afstand y-as:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblYAxisDistance: TLabel
      Left = 104
      Top = 125
      Width = 51
      Height = 13
      Caption = '22.52 cm'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRLC3Axis: TLabel
      Left = 15
      Top = 144
      Width = 65
      Height = 13
      Caption = 'Afstand z-as:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblZAxisDistance: TLabel
      Left = 104
      Top = 144
      Width = 51
      Height = 13
      Caption = '22.52 cm'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtAquariumWidth: TEdit
      Left = 104
      Top = 21
      Width = 70
      Height = 21
      TabOrder = 0
      OnChange = edtAquariumWidthChange
    end
    object edtAquariumDepth: TEdit
      Left = 104
      Top = 48
      Width = 70
      Height = 21
      TabOrder = 1
      OnChange = edtAquariumDepthChange
    end
    object edtAquariumHeight: TEdit
      Left = 104
      Top = 75
      Width = 70
      Height = 21
      TabOrder = 2
      OnChange = edtAquariumHeightChange
    end
  end
  object GroupBox1: TGroupBox
    Left = 231
    Top = 143
    Width = 314
    Height = 194
    Caption = 'Signaalanalyse'
    TabOrder = 2
    object Shape1: TShape
      Left = 16
      Top = 24
      Width = 252
      Height = 73
    end
    object imgSignalAnalysis: TImage
      Left = 17
      Top = 25
      Width = 250
      Height = 71
    end
    object lblScaleAmount: TLabel
      Left = 258
      Top = 103
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = '1000,00'
    end
    object lblPeak1Frequency: TLabel
      Left = 17
      Top = 169
      Width = 70
      Height = 13
      Caption = 'Piek 1: 1000hz'
    end
    object lblPeak2Frequency: TLabel
      Left = 117
      Top = 169
      Width = 70
      Height = 13
      Caption = 'Piek 2: 1000hz'
    end
    object lblPeak3Frequency: TLabel
      Left = 225
      Top = 169
      Width = 70
      Height = 13
      Caption = 'Piek 3: 1000hz'
    end
    object rgpSourceToAnalyse: TRadioGroup
      Left = 16
      Top = 103
      Width = 209
      Height = 52
      Caption = 'Bron'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Lijningang'
        'RLC1'
        'RLC2'
        'RLC3')
      TabOrder = 0
    end
    object chkSignalAnalysisFFT: TCheckBox
      Left = 231
      Top = 138
      Width = 74
      Height = 17
      Caption = 'Spectrum'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object trbScaleGraph: TTrackBar
      Left = 273
      Top = 9
      Width = 31
      Height = 88
      Max = 1000
      Orientation = trVertical
      Frequency = 100
      TabOrder = 2
      OnChange = trbScaleGraphChange
    end
  end
  object GroupBox4: TGroupBox
    Left = 8
    Top = 8
    Width = 538
    Height = 129
    Caption = 'Geluidskaart'
    TabOrder = 3
    object LbDrivername: TLabel
      Left = 15
      Top = 28
      Width = 33
      Height = 13
      Caption = 'Driver:'
    end
    object LbChannels: TLabel
      Left = 16
      Top = 54
      Width = 72
      Height = 13
      Caption = 'Input Channel:'
    end
    object lblTestSetupFilePlayed: TLabel
      Left = 331
      Top = 104
      Width = 54
      Height = 13
      Caption = '0:00 / 0:00'
    end
    object DriverCombo: TComboBox
      Left = 104
      Top = 23
      Width = 329
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      OnChange = DriverComboChange
    end
    object btnControlPanel: TButton
      Left = 439
      Top = 23
      Width = 81
      Height = 21
      Caption = 'Control Panel'
      Enabled = False
      TabOrder = 1
      OnClick = btnControlPanelClick
    end
    object ChannelBox: TComboBox
      Left = 104
      Top = 50
      Width = 416
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
    end
    object chkListenToAudioInput: TCheckBox
      Left = 416
      Top = 77
      Width = 115
      Height = 17
      Caption = 'Invoer beluisteren'
      TabOrder = 3
    end
    object chkSimulateTestSetup: TCheckBox
      Left = 16
      Top = 77
      Width = 120
      Height = 17
      Caption = 'Simuleer opstelling'
      TabOrder = 4
      OnClick = chkSimulateTestSetupClick
    end
    object edtTestSetupFilename: TEdit
      Left = 136
      Top = 77
      Width = 189
      Height = 21
      Enabled = False
      TabOrder = 5
      OnChange = edtTestSetupFilenameChange
    end
    object btnStartTestSetup: TButton
      Left = 356
      Top = 77
      Width = 54
      Height = 21
      Caption = 'Start'
      Enabled = False
      TabOrder = 6
      OnClick = btnStartTestSetupClick
    end
    object btnBrowseTestSetupFilename: TButton
      Left = 331
      Top = 77
      Width = 19
      Height = 21
      Caption = '...'
      Enabled = False
      TabOrder = 7
      OnClick = btnBrowseTestSetupFilenameClick
    end
    object pgbTestSetupFilePlayed: TProgressBar
      Left = 136
      Top = 104
      Width = 189
      Height = 17
      TabOrder = 8
    end
  end
  object tmrUpdateRLCValues: TTimer
    Interval = 40
    OnTimer = tmrUpdateRLCValuesTimer
    Left = 8
    Top = 8
  end
end
