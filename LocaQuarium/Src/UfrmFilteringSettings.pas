unit UfrmFilteringSettings;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, ComplexMath;

const
  WM_UpdateAndRepaintInputSpectrum = WM_USER + 1;

type
  TTimedCallFunction = function: Single;

  TfrmFilteringSettings = class(TForm)
    GroupBox3: TGroupBox;
    tbcFilters: TTabControl;
    GroupBox7: TGroupBox;
    Label1: TLabel;
    lblFrequency: TLabel;
    Label3: TLabel;
    lblQFactor: TLabel;
    trbFrequency: TTrackBar;
    trbQFactor: TTrackBar;
    GroupBox8: TGroupBox;
    Label2: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    lblRLCOutputAmplitude: TLabel;
    Label9: TLabel;
    lblRLCOutputDerivedDistance: TLabel;
    cmbFilterAxis: TComboBox;
    edtCalibrationMaxAmplitude: TEdit;
    btnDetectCalibrationMaxAmplitude: TButton;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    edtAquariumWidth: TEdit;
    edtAquariumDepth: TEdit;
    edtAquariumHeight: TEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    GroupBox1: TGroupBox;
    rgpSourceToAnalyse: TRadioGroup;
    chkSignalAnalysisFFT: TCheckBox;
    GroupBox4: TGroupBox;
    LbDrivername: TLabel;
    LbChannels: TLabel;
    DriverCombo: TComboBox;
    btnControlPanel: TButton;
    ChannelBox: TComboBox;
    lblRLC1Axis: TLabel;
    lblXAxisDistance: TLabel;
    lblRLC2Axis: TLabel;
    lblYAxisDistance: TLabel;
    lblRLC3Axis: TLabel;
    lblZAxisDistance: TLabel;
    btnDetectCalibrationMinAmplitude: TButton;
    edtCalibrationMinAmplitude: TEdit;
    Label13: TLabel;
    tmrUpdateRLCValues: TTimer;
    edtFrequency: TEdit;
    edtQFactor: TEdit;
    chkListenToAudioInput: TCheckBox;
    btnGetFrequencyFromPeak: TButton;
    Shape1: TShape;
    imgSignalAnalysis: TImage;
    lblScaleAmount: TLabel;
    lblPeak1Frequency: TLabel;
    lblPeak2Frequency: TLabel;
    lblPeak3Frequency: TLabel;
    trbScaleGraph: TTrackBar;
    chkSimulateTestSetup: TCheckBox;
    edtTestSetupFilename: TEdit;
    btnStartTestSetup: TButton;
    btnBrowseTestSetupFilename: TButton;
    pgbTestSetupFilePlayed: TProgressBar;
    lblTestSetupFilePlayed: TLabel;
    procedure trbFrequencyChange(Sender: TObject);
    procedure trbQFactorChange(Sender: TObject);
    procedure tbcFiltersChange(Sender: TObject);
    procedure cmbFilterAxisChange(Sender: TObject);
    procedure edtCalibrationMaxAmplitudeChange(Sender: TObject);
    procedure edtAquariumWidthChange(Sender: TObject);
    procedure edtAquariumDepthChange(Sender: TObject);
    procedure edtAquariumHeightChange(Sender: TObject);
    procedure DriverComboChange(Sender: TObject);
    procedure edtCalibrationMinAmplitudeChange(Sender: TObject);
    procedure tmrUpdateRLCValuesTimer(Sender: TObject);
    procedure btnControlPanelClick(Sender: TObject);
    procedure edtFrequencyChange(Sender: TObject);
    procedure edtQFactorChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtQFactorFrequencyKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure btnDetectCalibrationMaxAmplitudeClick(Sender: TObject);
    procedure btnDetectCalibrationMinAmplitudeClick(Sender: TObject);
    procedure trbScaleGraphChange(Sender: TObject);
    procedure btnGetFrequencyFromPeakClick(Sender: TObject);
    procedure chkSimulateTestSetupClick(Sender: TObject);
    procedure btnBrowseTestSetupFilenameClick(Sender: TObject);
    procedure btnStartTestSetupClick(Sender: TObject);
    procedure edtTestSetupFilenameChange(Sender: TObject);
  private
  public
    function CalculateAverageValue(TimedCall: TTimedCallFunction): Single;

    procedure UpdateAndRepaintInputSpectrum(var Msg: TMessage);
      Message WM_UpdateAndRepaintInputSpectrum;
  end;

  function GetOutputAmplitudeRMSCurrentFilter: Single;
  function GetPeakFrequency: Single;

var
  {
    Deze variabelen houden het signaal bij dat getekend wordt bij aanroep
    van de functie UpdateAndRepaintInputSpectrum.
  }
  SpectrumToDraw, SpectrumToDrawBuffer: TComplexNumberArray;
  {
    Een critical section die wordt aangeroepen wanneer een thread toegang tot
    de CurrentSpectrumToDraw variabele nodig heeft.
  }
  SpectrumToDrawCriticalSection: TRTLCriticalSection;

  {
    Aan de hand van deze variabele weet de code of een audio bestand moet
    worden afgespeeld.
  }
  SimulateTestSetup: Boolean;

  frmFilteringSettings: TfrmFilteringSettings;

implementation

uses
  Math, UEasyDSP, UFilteringSettings, UfrmLocaQuarium, ULocaQuariumRenderer;

{$R *.dfm}

procedure TfrmFilteringSettings.btnBrowseTestSetupFilenameClick(
  Sender: TObject);
  
  function ShowOpenDialog(FilterText: String; Options: TOpenOptions): String;
  var
    OpenDialog: TOpenDialog;
  begin
    Try
      OpenDialog := TOpenDialog.Create(Nil);
      OpenDialog.InitialDir := GetCurrentDir;
      OpenDialog.Filter := FilterText;
      OpenDialog.Options := Options;
      if OpenDialog.Execute then
        Result := OpenDialog.FileName
      else
        Result := '';
    finally
      OpenDialog.Free;
    end;
  end;

var
  FileName: String;
begin
  FileName := ShowOpenDialog('*.wav;*.WAV|*.wav;*.WAV', [ofFileMustExist]);

  if FileName <> '' then
    edtTestSetupFilename.Text := FileName;
end;

procedure TfrmFilteringSettings.btnControlPanelClick(Sender: TObject);
begin
  frmLocaQuarium.ASIOHost.ControlPanel;  
end;

function TfrmFilteringSettings.CalculateAverageValue(
  TimedCall: TTimedCallFunction):
  Single;
var
  TicksCurrent, TicksBeforeAveraging, TicksFrequency: Int64;
  NumSamples: Integer;
begin
  QueryPerformanceFrequency(TicksFrequency);
  QueryPerformanceCounter(TicksBeforeAveraging);
  TicksCurrent := TicksBeforeAveraging;
  NumSamples := 0;
  Result := 0;
  while ((TicksCurrent - TicksBeforeAveraging) / TicksFrequency) < 0.5 do
  begin
    Result := Result + TimedCall;
    NumSamples := NumSamples + 1;
    Application.ProcessMessages;
    Sleep(10);
    QueryPerformanceCounter(TicksCurrent);
  end;

  Result := Result / NumSamples;
end;

function GetOutputAmplitudeRMSCurrentFilter: Single;
var
  CurrentFilter: TFilterNum;
begin
  with frmFilteringSettings do
    CurrentFilter := StringToFilter(tbcFilters.Tabs[tbcFilters.TabIndex]);

  Result := GetOutputAmplitudeRMS(CurrentFilter);
end;

procedure TfrmFilteringSettings.btnDetectCalibrationMaxAmplitudeClick(
  Sender: TObject);
var
  SignalAverage: Single;
begin
  SignalAverage := CalculateAverageValue(GetOutputAmplitudeRMSCurrentFilter);

  edtCalibrationMaxAmplitude.Text := FloatToStrF(SignalAverage, ffNumber, 7,
    4);
end;

procedure TfrmFilteringSettings.btnDetectCalibrationMinAmplitudeClick(
  Sender: TObject);
var
  SignalAverage: Single;
begin
  SignalAverage := CalculateAverageValue(GetOutputAmplitudeRMSCurrentFilter);

  edtCalibrationMinAmplitude.Text := FloatToStrF(SignalAverage, ffNumber, 7,
    4);
end;

function GetPeakFrequency: Single;
var
  CurrentFilter: TFilterNum;
begin
  with frmFilteringSettings do
    CurrentFilter := StringToFilter(tbcFilters.Tabs[tbcFilters.TabIndex]);

  if CurrentFilter = FNRLC1 then
    Result := FrequencySpectrumPeak1Frequency
  else if CurrentFilter = FNRLC2 then
    Result := FrequencySpectrumPeak2Frequency
  else if CurrentFilter = FNRLC3 then
    Result := FrequencySpectrumPeak3Frequency;
end;

procedure TfrmFilteringSettings.btnGetFrequencyFromPeakClick(Sender: TObject);
var
  SignalAverage: Single;
begin
  SignalAverage := CalculateAverageValue(GetPeakFrequency);

  trbFrequency.Position := Trunc(SignalAverage);
end;

procedure TfrmFilteringSettings.btnStartTestSetupClick(Sender: TObject);
begin
  try
    if btnStartTestSetup.Caption = 'Start' then
    begin
      chkListenToAudioInput.Checked := True;

      edtTestSetupFilename.Enabled := False;
      btnBrowseTestSetupFilename.Enabled := False;

      SimulateTestSetup := True;
      btnStartTestSetup.Caption := 'Stop';
    end else
    begin
      edtTestSetupFilename.Enabled := True;
      btnBrowseTestSetupFilename.Enabled := True;

      chkListenToAudioInput.Checked := False;

      SimulateTestSetup := False;

      btnStartTestSetup.Caption := 'Start';
    end;
  finally
  end;
end;

procedure TfrmFilteringSettings.chkSimulateTestSetupClick(Sender: TObject);
begin
  if (chkSimulateTestSetup.Checked) and (chkSimulateTestSetup.Enabled) then
  begin
    edtTestSetupFilename.Enabled := True;
    btnBrowseTestSetupFilename.Enabled := True;
    btnStartTestSetup.Enabled := True;
  end else
  begin
    if btnStartTestSetup.Caption = 'Stop' then
      btnStartTestSetup.Click;

    edtTestSetupFilename.Enabled := False;
    btnBrowseTestSetupFilename.Enabled := False;
    btnStartTestSetup.Enabled := False;
  end;
end;

procedure TfrmFilteringSettings.cmbFilterAxisChange(Sender: TObject);
var
  FilterAxis: TAquariumAxis;
begin
  if cmbFilterAxis.Text[1] = 'X' then
    FilterAxis := X
  else if cmbFilterAxis.Text[1] = 'Y' then
    FilterAxis := Y
  else if cmbFilterAxis.Text[1] = 'Z' then
    FilterAxis := Z;

  if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC1' then
    RLC1Axis := FilterAxis
  else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC2' then
    RLC2Axis := FilterAxis
  else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC3' then
    RLC3Axis := FilterAxis;
end;

procedure TfrmFilteringSettings.DriverComboChange(Sender: TObject);
var
  i: Integer;
begin
  btnControlPanel.Enabled := False;
  DriverCombo.ItemIndex := DriverCombo.Items.IndexOf(DriverCombo.Text);
  if DriverCombo.ItemIndex >= 0 then
  begin
    with frmLocaQuarium do
    begin
      ASIOHost.DriverIndex := DriverCombo.ItemIndex;
      ChannelBox.Clear;
      for i := 0 to (ASIOHost.InputChannelCount) - 1 do
        ChannelBox.Items.Add(ASIOHost.InputChannelInfos[i].Name);

      btnControlPanel.Enabled := True;
      ChannelBox.ItemIndex := 0;
    end;
  end;
end;

procedure TfrmFilteringSettings.edtAquariumDepthChange(Sender: TObject);
begin
  AquariumDepth := StrToIntDef(edtAquariumDepth.Text, 0);

  LocaQuariumRenderer.InvokeResizeAquarium;
end;

procedure TfrmFilteringSettings.edtAquariumHeightChange(Sender: TObject);
begin
  AquariumHeight := StrToIntDef(edtAquariumHeight.Text, 0);

  LocaQuariumRenderer.InvokeResizeAquarium;
end;

procedure TfrmFilteringSettings.edtAquariumWidthChange(Sender: TObject);
begin
  AquariumWidth := StrToIntDef(edtAquariumWidth.Text, 0);

  LocaQuariumRenderer.InvokeResizeAquarium;
end;

procedure TfrmFilteringSettings.edtCalibrationMaxAmplitudeChange(
  Sender: TObject);
begin
  if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC1' then
    RLC1SignalMaxAmplitude := StrToFloatDef(edtCalibrationMaxAmplitude.Text, 0)
  else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC2' then
    RLC2SignalMaxAmplitude := StrToFloatDef(edtCalibrationMaxAmplitude.Text, 0)
  else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC3' then
    RLC3SignalMaxAmplitude := StrToFloatDef(edtCalibrationMaxAmplitude.Text, 0);
end;

procedure TfrmFilteringSettings.edtCalibrationMinAmplitudeChange(
  Sender: TObject);
begin
  if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC1' then
    RLC1SignalMinAmplitude := StrToFloatDef(edtCalibrationMinAmplitude.Text, 0)
  else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC2' then
    RLC2SignalMinAmplitude := StrToFloatDef(edtCalibrationMinAmplitude.Text, 0)
  else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC3' then
    RLC3SignalMinAmplitude := StrToFloatDef(edtCalibrationMinAmplitude.Text, 0);
end;

procedure TfrmFilteringSettings.edtFrequencyChange(Sender: TObject);
var
  NewFrequency: Integer;
begin
  NewFrequency := StrToIntDef(edtFrequency.Text, 1);
  if NewFrequency < 1 then
    NewFrequency := 1
  else if NewFrequency > 20500 then
    NewFrequency := 20500;

  trbFrequency.Position := NewFrequency;
  edtFrequency.Text := IntToStr(NewFrequency);
end;

procedure TfrmFilteringSettings.edtQFactorFrequencyKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not ((Key in ['0'..'9']) or (Key = #08)) then
    Key := #0;
end;

procedure TfrmFilteringSettings.edtTestSetupFilenameChange(Sender: TObject);
var
  AudioFileTotalTime: Single;
begin
  if FileExists(edtTestSetupFilename.Text) then
  begin
    try
      BufferedTestSetupAudioPlayer.Reset;
      BufferedTestSetupAudioPlayer.Filename := edtTestSetupFilename.Text;
      pgbTestSetupFilePlayed.Position := 0;

      AudioFileTotalTime := BufferedTestSetupAudioPlayer.AudioFile.TotalTime;

      pgbTestSetupFilePlayed.Max := Trunc(AudioFileTotalTime);
      lblTestSetupFilePlayed.Caption :=
          '0:00 / ' +
          Format('%d', [Trunc(AudioFileTotalTime / 60)]) + ':' +
          Format('%.2d', [Trunc(AudioFileTotalTime -
              Trunc(AudioFileTotalTime / 60) * 60)]);
    except
    end;
  end else
  begin
    btnStartTestSetup.Enabled := False;
  end;
end;

procedure TfrmFilteringSettings.edtQFactorChange(Sender: TObject);
var
  NewQFactor: Integer;
begin
  NewQFactor := StrToIntDef(edtQFactor.Text, 1);
  if NewQFactor < 1 then
    NewQFactor := 1
  else if NewQFactor > 1000 then
    NewQFactor := 1000;

  trbQFactor.Position := NewQFactor;
  edtQFactor.Text := IntToStr(NewQFactor);
end;

procedure TfrmFilteringSettings.FormCreate(Sender: TObject);
begin
  edtAquariumDepth.Text := IntToStr(Trunc(AquariumDepth));
  edtAquariumHeight.Text := IntToStr(Trunc(AquariumHeight));
  edtAquariumWidth.Text := IntToStr(Trunc(AquariumWidth));
end;

procedure TfrmFilteringSettings.FormShow(Sender: TObject);
begin
  tbcFiltersChange(nil);
end;

procedure TfrmFilteringSettings.tbcFiltersChange(Sender: TObject);
var
  AllFilterAxes: Set of TAquariumAxis;
  FilterAxis: TAquariumAxis;
  I: Integer;
  CalibrationMinAmplitude, CalibrationMaxAmplitude: Single;
begin
  cmbFilterAxis.Clear;

  if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC1' then
  begin
    btnGetFrequencyFromPeak.Caption := 'Piek 1';
    trbFrequency.Position := Trunc(RLC1.Frequency);
    trbQFactor.Position := Trunc(RLC1.QualityFactor);
    FilterAxis := RLC1Axis;
    CalibrationMinAmplitude := RLC1SignalMinAmplitude;
    CalibrationMaxAmplitude := RLC1SignalMaxAmplitude;
  end else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC2' then
  begin
    btnGetFrequencyFromPeak.Caption := 'Piek 2';
    trbFrequency.Position := Trunc(RLC2.Frequency);
    trbQFactor.Position := Trunc(RLC2.QualityFactor);
    FilterAxis := RLC2Axis;
    CalibrationMinAmplitude := RLC2SignalMinAmplitude;
    CalibrationMaxAmplitude := RLC2SignalMaxAmplitude;
      FloatToStrF(RLC2SignalMaxAmplitude, ffNumber, 7, 4);
  end else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC3' then
  begin
    btnGetFrequencyFromPeak.Caption := 'Piek 3';
    trbFrequency.Position := Trunc(RLC3.Frequency);
    trbQFactor.Position := Trunc(RLC3.QualityFactor);
    FilterAxis := RLC3Axis;
    CalibrationMinAmplitude := RLC3SignalMinAmplitude;
    CalibrationMaxAmplitude := RLC3SignalMaxAmplitude;
  end;

  edtFrequency.Text := IntToStr(trbFrequency.Position);
  edtQFactor.Text := IntToStr(trbQFactor.Position);

  if CalibrationMaxAmplitude > 0 then
  begin
    edtCalibrationMaxAmplitude.Text :=
      FloatToStrF(CalibrationMaxAmplitude, ffNumber, 7, 4);
  end else
    edtCalibrationMaxAmplitude.Text := '';

  if CalibrationMinAmplitude > 0 then
  begin
    edtCalibrationMinAmplitude.Text :=
      FloatToStrF(CalibrationMinAmplitude, ffNumber, 7, 4);
  end else
    edtCalibrationMinAmplitude.Text := '';

  AllFilterAxes := [RLC1Axis, RLC2Axis, RLC3Axis] - [FilterAxis];

  cmbFilterAxis.Items.Add('Geen');
  if not (X in AllFilterAxes) then
    cmbFilterAxis.Items.Add('X');
  if not (Y in AllFilterAxes) then
    cmbFilterAxis.Items.Add('Y');
  if not (Z in AllFilterAxes) then
    cmbFilterAxis.Items.Add('Z');

  cmbFilterAxis.ItemIndex := 0;
  for I := 0 to cmbFilterAxis.Items.Count - 1 do
  begin
    if (cmbFilterAxis.Items[I] = 'X') and (FilterAxis = X) then
      cmbFilterAxis.ItemIndex := I;
    if (cmbFilterAxis.Items[I] = 'Y') and (FilterAxis = Y) then
      cmbFilterAxis.ItemIndex := I;
    if (cmbFilterAxis.Items[I] = 'Z') and (FilterAxis = Z) then
      cmbFilterAxis.ItemIndex := I;
  end;
end;

procedure TfrmFilteringSettings.tmrUpdateRLCValuesTimer(Sender: TObject);
begin
  if AxisIsAssigned(X) then
    lblXAxisDistance.Caption := FloatToStrF(CalcElectrodeDistanceX,
      ffNumber, 7, 2)
  else
    lblXAxisDistance.Caption := '? cm';
  if AxisIsAssigned(Y) then
    lblYAxisDistance.Caption := FloatToStrF(CalcElectrodeDistanceY,
      ffNumber, 7, 2)
  else
    lblYAxisDistance.Caption := '? cm';
  if AxisIsAssigned(Z) then
    lblZAxisDistance.Caption := FloatToStrF(CalcElectrodeDistanceZ,
      ffNumber, 7, 2)
  else
    lblZAxisDistance.Caption := '? cm';

  if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC1' then
  begin
    lblRLCOutputAmplitude.Caption := FloatToStrF(RLC1OutputAmplitude,
      ffNumber, 7, 4);
    lblRLCOutputDerivedDistance.Caption := FloatToStrF(
      CalcElectrodeDistanceRLC1, ffNumber, 7, 4);
  end else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC2' then
  begin
    lblRLCOutputAmplitude.Caption := FloatToStrF(RLC2OutputAmplitude,
      ffNumber, 7, 4);
    lblRLCOutputDerivedDistance.Caption := FloatToStrF(
      CalcElectrodeDistanceRLC2, ffNumber, 7, 4);
  end else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC3' then
  begin
    lblRLCOutputAmplitude.Caption := FloatToStrF(RLC3OutputAmplitude,
      ffNumber, 7, 4);
    lblRLCOutputDerivedDistance.Caption := FloatToStrF(
      CalcElectrodeDistanceRLC3, ffNumber, 7, 4);
  end;

  lblPeak1Frequency.Caption := 'Piek 1: ' + IntToStr(Trunc(
    FrequencySpectrumPeak1Frequency)) + 'hz';
  lblPeak2Frequency.Caption := 'Piek 2: ' + IntToStr(Trunc(
    FrequencySpectrumPeak2Frequency)) + 'hz';
  lblPeak3Frequency.Caption := 'Piek 3: ' + IntToStr(Trunc(
    FrequencySpectrumPeak3Frequency)) + 'hz';
end;

procedure TfrmFilteringSettings.trbFrequencyChange(Sender: TObject);
begin
  if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC1' then
    RLC1.Frequency := trbFrequency.Position
  else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC2' then
    RLC2.Frequency := trbFrequency.Position
  else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC3' then
    RLC3.Frequency := trbFrequency.Position;

  edtFrequency.Text := IntToStr(trbFrequency.Position);

  lblFrequency.Caption := IntToStr(trbFrequency.Position) + 'Hz';
end;

procedure TfrmFilteringSettings.trbQFactorChange(Sender: TObject);
begin
  if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC1' then
    RLC1.QualityFactor := trbQFactor.Position
  else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC2' then
    RLC2.QualityFactor := trbQFactor.Position
  else if tbcFilters.Tabs[tbcFilters.TabIndex] = 'RLC3' then
    RLC3.QualityFactor := trbQFactor.Position;

  edtQFactor.Text := IntToStr(trbQFactor.Position);

  lblQFactor.Caption := IntToStr(trbQFactor.Position);
end;

procedure TfrmFilteringSettings.trbScaleGraphChange(Sender: TObject);
begin
  if trbScaleGraph.Position < 1000 then
  begin
    lblScaleAmount.Caption :=
      FloatToStrF(Power(
        Power(100000, 1 / 1000),
        1000 - trbScaleGraph.Position
      ) / 100, ffNumber, 7, 2);
  end else
    lblScaleAmount.Caption := 'Auto';
end;

procedure TfrmFilteringSettings.UpdateAndRepaintInputSpectrum(
  var Msg: TMessage);
var
  CurrY: Double;
  TopY: Double;
  I: Integer;

  DoubleBuffer: TBitmap;
begin
  EnterCriticalSection(SpectrumToDrawCriticalSection);

  if chkSignalAnalysisFFT.Checked then
    FFT(SpectrumToDraw);

  if trbScaleGraph.Position < 1000 then
    TopY := 1 / (Power(
        Power(100000, 1 / 1000),
        1000 - trbScaleGraph.Position
      ) / 100)
  else
  begin
    if chkSignalAnalysisFFT.Checked then
    begin
      for I := 0 to imgSignalAnalysis.Width - 1 do
      begin
        SpectrumToDraw[
          Round(
            I * Length(SpectrumToDraw) / imgSignalAnalysis.Width / 2
          )
        ].R := TComplexMath.ComplexAbs(
                    SpectrumToDraw[
                      Round(
                        I * Length(SpectrumToDraw) /
                          imgSignalAnalysis.Width / 2
                      )
                    ]
                  );

        CurrY :=  SpectrumToDraw[
                  Round(
                    I * Length(SpectrumToDraw) /
                      imgSignalAnalysis.Width / 2
                  )
                ].R;

        if CurrY > TopY then
        begin
          Topy := CurrY;
        end;
      end;
    end else
    begin
      for I := 0 to imgSignalAnalysis.Width - 1 do
      begin
        CurrY :=  SpectrumToDraw[
                  Round(
                    I * Length(SpectrumToDraw) /
                      imgSignalAnalysis.Width / 2
                  )
                ].R;

        if CurrY > TopY then
        begin
          Topy := CurrY;
        end;
      end;
    end;
  end;

  DoubleBuffer := TBitmap.Create;
  try
    DoubleBuffer.Height := imgSignalAnalysis.Height;
    DoubleBuffer.Width := imgSignalAnalysis.Width;

    DoubleBuffer.Canvas.Pen.Color := clBlack;
    DoubleBuffer.Canvas.Pen.Width := 2;

    if chkSignalAnalysisFFT.Checked then
    begin
      for I := 0 to imgSignalAnalysis.Width - 1 do
      begin
        DoubleBuffer.Canvas.MoveTo(I, imgSignalAnalysis.Height);
        DoubleBuffer.Canvas.LineTo(I,
          Round(
            imgSignalAnalysis.Height -
              SpectrumToDraw[
                Round(
                  I * Length(SpectrumToDraw) /
                    imgSignalAnalysis.Width / 2
                )
              ].R / TopY * imgSignalAnalysis.Height
          )
        );
      end;
    end else
    begin
      for I := 0 to imgSignalAnalysis.Width - 1 do
      begin
        DoubleBuffer.Canvas.MoveTo(I, imgSignalAnalysis.Height div 2);
        DoubleBuffer.Canvas.LineTo(I,
          Round(
            imgSignalAnalysis.Height / 2 -
              SpectrumToDraw[
                Round(
                  I * Length(SpectrumToDraw) /
                    imgSignalAnalysis.Width / 2
                )
              ].R / TopY * imgSignalAnalysis.Height / 2
          )
        );
      end;
    end;

    LeaveCriticalSection(SpectrumToDrawCriticalSection);

    imgSignalAnalysis.Canvas.Draw(0, 0, DoubleBuffer);
    imgSignalAnalysis.Repaint;

  finally
    DoubleBuffer.Free;
  end;
end;

end.
