unit ULocaQuariumRenderer;

interface

uses
  Classes, GL, GLU, GLUT, Types, Unit3DS, Windows;

type
  TOpenGLColor4f = Array [0..3] of GLFloat;
  POpenGLColor4f = ^TOpenGLColor4f;

  TOpenGLCuboidColors4f = record
    FrontFaceColor, RightFaceColor, BackFaceColor, LeftFaceColor,
      TopFaceColor, BottomFaceColor: TOpenGLColor4f;
  end;

  POpenGLCuboidColors4f =  ^TOpenGLCuboidColors4f;

  TOpenGLCuboidTextures = record
    FrontFaceTexture, RightFaceTexture, BackFaceTexture, LeftFaceTexture,
      TopFaceTexture, BottomFaceTexture: GLUInt;
  end;

  POpenGLCuboidTextures =  ^TOpenGLCuboidTextures;

  TRectangularCuboidFace = (AllFaces, FrontFace, RightFace, BackFace, LeftFace, 
    TopFace, BottomFace);
  TRectangularCuboidFaces = set of TRectangularCuboidFace;

  TLocaQuariumRenderer = class(TThread)
  private
    AquariumDepthScaled, AquariumHeightScaled, AquariumWidthScaled: Single;
    AquariumBiggestDimension: Single;
    AquariumHorizontalRotation, AquariumVerticalRotation: Single;

    FontGMF: Array[0..255] of GLYPHMETRICSFLOAT;

    PrevCursorPosition, CursorPosition: TPoint;

    FishX, FishY, FishZ: Single;
    FishXOpenGLUnits, FishYOpenGLUnits, FishZOpenGLUnits: Single;
    FishAngleHorizontal, FishAngleVertical: Single;
    PreviousFishX, PreviousFishY, PreviousFishZ: Single;

    FishTexture: GLUInt;
    SkyBoxFrontFaceTexture, SkyBoxRightFaceTexture, SkyBoxBackFaceTexture,
    SkyBoxLeftFaceTexture, SkyBoxTopFaceTexture, SkyBoxBottomFaceTexture:
      GLUInt;

    TableModel: T3DModel;

    RenderingFPS: Single;

    ShouldResizeAquarium: Boolean;
    ShouldResizePerspective: Boolean;

    //Alle display lists
    AquariumDisplayList: GLUInt;
    AquariumOutlineDisplayList: GLUInt;
    AxesDisplayList: GLUInt;
    FontDisplayListsBase, Font3DDisplayListsBase: GLUInt;
    SkyBoxDisplayList: GLUInt;
    TableDisplayList: GLUInt;

    procedure CalculateAquariumDimensionsOpenGL;
    function CalculateGLPrintWidth(Text: String; Scale: Single = 0.2): Single;

    procedure DrawScene;
    procedure DoCalculations;

    procedure FreeOpenGLRendering;

    procedure glDrawRectangularCuboid(X1, Y1, Z1, X2, Y2, Z2: Single;
      FacesToDraw: TRectangularCuboidFaces;
      PColors: POpenGLCuboidColors4f = Nil;
      PTextures: POpenGLCuboidTextures = Nil;
      TexturesNumRepeat: Integer = 1);
    procedure glDrawHorizontalRectangle(X1, Y1, Z1, X2, Y2, Z2: Single;
      Color: POpenGLColor4f = Nil; Texture: GLUInt = 0;
      TextureNumRepeat: Integer = 1);
    procedure glDrawVerticalRectangle(X1, Y1, Z1, X2, Y2, Z2: Single;
      Color: POpenGLColor4f = Nil; Texture: GLUInt = 0;
      TextureNumRepeat: Integer = 1);
    procedure glPrintHorizontalText3D(X, Y, Z, Angle: Single; Text: String;
      Scale: Single = 0.2; CenterText: Boolean = True);
    procedure glPrintVerticalText3D(X, Y, Z, Angle: Single; Text: String;
      Scale: Single = 0.2; CenterText: Boolean = True);
    procedure glPrintScreenHorizontalText2D(X, Y: Single; Text: String);

    procedure InitOpenGLFonts;
    procedure InitOpenGLModels;
    procedure InitOpenGLObjectAquarium;
    procedure InitOpenGLObjectAquariumOutline;
    procedure InitOpenGLObjectAxes;
    procedure InitOpenGLObjectSkyBox;
    procedure InitOpenGLObjectTable;
    procedure InitOpenGLTextures;
    procedure InitOpenGLRendering;

    procedure KillFont;
    procedure ResizePerspective;
  public
    AquariumScalingFactor: Single;

    constructor Create(CreateSuspended: Boolean = False);
    procedure Execute; override;
    procedure InvokeResizeAquarium;
    procedure InvokeResizePerspective;
  end;
var
  LocaQuariumRenderer: TLocaQuariumRenderer;

implementation

uses
  Controls, Forms, Math, SysUtils, Textures, UFilteringSettings,
    UfrmRenderingSettings, UfrmLocaQuarium, URenderingSettings;

constructor TLocaQuariumRenderer.Create(CreateSuspended: Boolean = False);
begin
  inherited Create(CreateSuspended);

  AquariumScalingFactor := 1;

  ShouldResizeAquarium := False;
  ShouldResizePerspective := False;

  {
    Omdat OpenGL in de negatieve richting kijkt op de z-as maar wij de assen
    tekenen vanaf negatieve naar positief op de z-as moeten we het aquarium
    180 graden draaien om in de goede richting te kijken. De overige 45 graden
    zorgen ervoor dat we schuin naar het aquarium kijken.
  }
  AquariumHorizontalRotation := 225;
  //We kijken standaard iets van boven naar het aquarium
  AquariumVerticalRotation := 20;
end;

procedure TLocaQuariumRenderer.CalculateAquariumDimensionsOpenGL;
begin
  {
    We berekenen de afmetingen van het aquarium. We willen dat de langste zijde
    altijd 1.0 OpenGL 'eenheden' is. Op die manier past de aquariumbak altijd
    op de tafel.
  }
  AquariumBiggestDimension := Max(Max(AquariumDepth, AquariumHeight),
    AquariumWidth);

  AquariumDepthScaled := AquariumDepth / AquariumBiggestDimension;
  AquariumHeightScaled := AquariumHeight / AquariumBiggestDimension;
  AquariumWidthScaled := AquariumWidth / AquariumBiggestDimension;
end;

{
  Deze functie berekent de breedte van text in de OpenGL wereld. Het is soms
  benodigd de breedte van de tekst te weten om goede positionering mogelijk te
  maken.
}
function TLocaQuariumRenderer.CalculateGLPrintWidth(Text: String; Scale:
  Single = 0.2): Single;
var
  Loop: Integer;
begin
  Result := 0;
  for Loop := 1 to System.Length(Text) do
    Result := Result + FontGMF[Ord(Text[Loop])].gmfCellIncX;

  Result := Result * Scale;
end;

{
  Deze functie bevat alle rendering code
}
procedure TLocaQuariumRenderer.DrawScene;
var
  I: Integer;
  TextVerticalPosition: Integer;
begin
  {
    Het scherm wordt geleegd.
  }
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
  
  {
    We bevinden ons een kleine afstand van alle objecten af.
  }
  glTranslatef(0, 0.2, -3.0);

  {
    We roteren alle objecten.
  }
  glRotatef(AquariumVerticalRotation, 1, 0, 0);
  glRotatef(AquariumHorizontalRotation, 0, 1, 0);

  if ShowSkybox then
  begin
    {
      We tekenen de skybox.
    }
    glCallList(SkyBoxDisplayList);
  end;

  {
    We schalen de wereld met de AquariumScalingFactor variabele die met de muis
    groter of kleiner te maken is. De schaling staat na de skybox omdat die
    altijd even groot moet blijven.
  }
  glScalef(AquariumScalingFactor, AquariumScalingFactor, AquariumScalingFactor);

  {
    We tekenen de tafel. We doen een aanroep naar glPushMatrix omdat we weer
    terug willen naar de oude stand van de rendering als we klaar zijn.
  }
  glPushMatrix;
    glTranslatef(1.23, -1.237 + (1 - (AquariumHeight /
      AquariumBiggestDimension)) / 2, 0.6);
    glRotatef(90, 0, 1, 0);
    glScalef(0.02, 0.02, 0.02);
    glCallList(TableDisplayList);
  glPopMatrix;

  if ShowAxes then
    glCallList(AxesDisplayList);

  {
    Voor het tekenen van het aquarium zetten we blending aan om het aquarium
    doorzichtig te maken. Depth test zetten we tijdelijk uit omdat alle zijdes
    van het aquarium getekend moeten worden.
  }
  glEnable(GL_BLEND);
  glDisable(GL_DEPTH_TEST);

  if ShowPositionIndicators then
  begin
    {
      We tekenen de balken die de positie aangeven van de vis. We hebben er voor
      gekozen om deze balken niet in display lists te zetten.
    }
    glColor4f(0.30, 0.46, 0.46, 1);

    glDrawHorizontalRectangle(
      -AquariumWidthScaled / 2,
      -AquariumHeightScaled / 2 + 0.006,
      -(AquariumDepthScaled / 2) - 0.005 + FishZOpenGLUnits,
      AquariumWidthScaled / 2 + 0.05,
      -AquariumHeightScaled / 2 + 0.006,
      -(AquariumDepthScaled / 2) + 0.005 + FishZOpenGLUnits
    );

    glPrintHorizontalText3D(
      AquariumWidthScaled / 2 + 0.1,
      -AquariumHeightScaled / 2 + 0.01,
      -(AquariumDepthScaled / 2) + FishZOpenGLUnits,
      90,
      FloatToStrF(FishZ, ffNumber, 7, 2),
      0.05
    );

    glDrawHorizontalRectangle(
      AquariumWidthScaled / 2 + 0.005 - FishXOpenGLUnits,
      -AquariumHeightScaled / 2 + 0.006,
      -(AquariumDepthScaled / 2) - 0.05,
      AquariumWidthScaled / 2 - 0.005 - FishXOpenGLUnits,
      -AquariumHeightScaled / 2 + 0.006,
      (AquariumDepthScaled / 2)
    );

    glPrintHorizontalText3D(
      AquariumWidthScaled / 2 - FishXOpenGLUnits,
      -AquariumHeightScaled / 2 + 0.01,
      -(AquariumDepthScaled / 2) - 0.1,
      180,
      FloatToStrF(FishX, ffNumber, 7, 2),
      0.05
    );
  end;

  {
    We tekenen het aquarium
  }
  glCallList(AquariumDisplayList);

  {
    We hoeven geen doorzichtige objecten meer te tekenen. We zetten blending uit
    en zetten de depth test weer aan.
  }
  glDisable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);

  glCallList(AquariumOutlineDisplayList);

  {
    We gaan de vis tekenen. We tekenen de vis alleen als de x en y as zijn
    toegewezen aan een filter.
  }
  if AxisIsAssigned(X) and AxisIsAssigned(Z) then
  begin
    {
      Als de y-as is toegewezen zetten we de vis op de goede hoogte. Als de
      y-as niet is toegewezen zetten we de vis op het midden van het aquarium.
    }
    if AxisIsAssigned(Y) then
      glTranslatef(
        AquariumWidthScaled / 2 - FishXOpenGLUnits,
        -AquariumHeightScaled / 2 + FishYOpenGLUnits,
        -AquariumDepthScaled / 2 + FishZOpenGLUnits
      )
    else
      glTranslatef(
        AquariumWidthScaled / 2 - FishXOpenGLUnits,
        0,
        -AquariumDepthScaled / 2 + FishZOpenGLUnits
      );

    glRotatef(FishAngleHorizontal, 0, 1, 0);
    glRotatef(FishAngleVertical, 0, 0, 1);
    {
      De texture van de vis is in het ARGB kleuren formaat. De A in dit formaat
      geeft het alpha kanaal aan. Het alpha kanaal van het texture van de vis is
      overal ��n waar de vis niet meer getekend wordt.

      Als we het alpha kanaal willen meenemen in de rendering zullen we blending
      tijdelijk aan moeten zetten.
    }
    glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, FishTexture);

    glColor4f(1, 1, 1, 1);
    glScalef(0.06, 0.06, 0.06);

    glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -1.0,  0);
      glTexCoord2f(1.0, 0.0); glVertex3f( 1.0, -1.0,  0);
      glTexCoord2f(1.0, 1.0); glVertex3f( 1.0,  1.0,  0);
      glTexCoord2f(0.0, 1.0); glVertex3f(-1.0,  1.0,  0);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);
  end;

  if ShowDistancesInLeftCorner then
  begin
    {
      We willen linksboven in de hoek ook de posities van de vis tekenen.

      Eerst moeten we de matrix mode op projection zetten. We laden de identity
      matrix en zetten een tweedimensionale projectie op via gluOrtho2D.

      Dan gaan we terug naar de modelview matrix en laden de identity matrix.

      Nu kunnen we tweedimensionale tekst tekenen op het scherm.
    }
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity;
    with frmLocaQuarium.pnlLocaQuarium3D do
      gluOrtho2D(0, Width, Height, 0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity;

    glColor3f(1, 1, 1);

    TextVerticalPosition := 24;

    if AxisIsAssigned(X) then
    begin
      glPrintScreenHorizontalText2D(10, TextVerticalPosition, 'X-As: ' +
        FloatToStrF(FishX, ffNumber, 7, 2));

      TextVerticalPosition := TextVerticalPosition + 18;
    end;

    if AxisIsAssigned(Y) then
    begin
      glPrintScreenHorizontalText2D(10, TextVerticalPosition, 'Y-As: ' +
        FloatToStrF(FishY, ffNumber, 7, 2));

      TextVerticalPosition := TextVerticalPosition + 18;
    end;

    if AxisIsAssigned(Z) then
    begin
      glPrintScreenHorizontalText2D(10, TextVerticalPosition, 'Z-As: ' +
        FloatToStrF(FishZ, ffNumber, 7, 2));

      TextVerticalPosition := TextVerticalPosition + 18;
    end;

    if ShowFPS then
      glPrintScreenHorizontalText2D(10, TextVerticalPosition, 'FPS: ' +
          FloatToStrF(RenderingFPS, ffNumber, 7, 2));

    {
      Tenslotte kunnen we de buffers omwisselen.
    }
    SwapBuffers(h_DCPanel);

    {
      We herstellen de 3D projectie.
    }
    ResizePerspective;
  end else
    SwapBuffers(h_DCPanel);
end;

procedure TLocaQuariumRenderer.DoCalculations;
  {
    De functie ATan2 berekent een hoek tussen een x- en y-coordinaat met een
    bereik van -Pi tot Pi radialen. In de meeste programmeertalen is ATan2 te
    vinden in een math header of unit. Voor delphi is dit niet het geval.
  }
  function ATan2(Y, X: Real): Real;

    function Sign (A : Real) : Real;
    begin
      if A < 0  then  Sign := -1
                else  Sign :=  1;
    end;

  begin
    if X > 0 then
      Result := ArcTan(Y / X)
    else if X < 0  then
      Result := ArcTan(Y / X) + Pi
    else
      Result := Pi / 2 * Sign(Y);
  end;

  function IsMouseOverControl(Control: TWinControl): Boolean;
  var
    P: TPoint;
  begin
    GetCursorPos(P);
    Result := Assigned(Control) and IsWindow(Control.Handle) and
      (WindowFromPoint(P) = Control.Handle)
  end;

var
  HorizontalMovement, VerticalMovement: Integer;

  Angle1, Angle2: Single;
  FishMovementHorizontal, FishMovementX, FishMovementY,
    FishMovementZ: Single;
  NewFishAngleHorizontal: Single;
const
  FishSpinningSmoothing: Integer = 4;

begin
  GetCursorPos(CursorPosition);

  if ((GetAsyncKeyState(VK_LBUTTON) and $8000) <> 0) and
    IsMouseOverControl(frmLocaQuarium.pnlLocaQuarium3D) then
  begin
    HorizontalMovement := CursorPosition.X - PrevCursorPosition.X;
    AquariumHorizontalRotation := AquariumHorizontalRotation +
      HorizontalMovement;

    VerticalMovement := CursorPosition.Y - PrevCursorPosition.Y;
    AquariumVerticalRotation := AquariumVerticalRotation +
      VerticalMovement;
  end;

  PrevCursorPosition := CursorPosition;

  {
    We berekenen de posities van de vis en rekenen om naar OpenGL 'eenheden'.
  }
  FishX := CalcElectrodeDistanceX;
  FishY := CalcElectrodeDistanceY;
  FishZ := CalcElectrodeDistanceZ;

  FishXOpenGLUnits := (FishX / AquariumWidth) * AquariumWidthScaled;
  FishYOpenGLUnits := (FishY / AquariumHeight) * AquariumHeightScaled;
  FishZOpenGLUnits := (FishZ / AquariumDepth) * AquariumDepthScaled;

  {
    We berekenen de bewegingsvectoren van de vis in de x, y en z richting.
  }
  FishMovementX := FishXOpenGLUnits - PreviousFishX;
  FishMovementY := FishYOpenGLUnits - PreviousFishY;
  FishMovementZ := FishZOpenGLUnits - PreviousFishZ;

  {
    We berekenen het verschil in afstand horizontaal. Alleen als deze groter
    dan 0.02 is willen we de vis bijwerken. Op deze manier reageert de vis
    niet op ruis die wordt gemeten.
  }
  FishMovementHorizontal := Sqrt(Sqr(FishMovementX) + Sqr(FishMovementZ));

  if FishMovementHorizontal > 0.02 then
  begin
    NewFishAngleHorizontal := Atan2(FishMovementZ, FishMovementX) / Pi * 180;

    {
      We hebben een nieuwe hoek berekend waarop de vis gedraaid moet worden
      ten opzichte van de y-as. Deze hoek heeft een bereik van -Pi tot Pi.

      We willen de hoek van draaiing niet gelijk bijwerken maar geleidelijk
      laten veranderen. Hiervoor gebruiken wij de formule die wij op meerdere
      plekken in de code gebruiken: X = (N * Xoud + Xnieuw) / (N + 1).

      We kunnen echter niet altijd de volgende regel code gebruiken:
      FishAngleHorizontal := (N * FishAngleHorizontal +
        NewFishAngleHorizontal) / (N + 1);

      We willen namelijk dat de vis altijd draait in de richting van de
      kleinste hoek naar de nieuw berekende hoek.

      Stel de vis bevindt zich huidig op een draaiingshoek van 30 graden. De
      hoek die nieuw is berekend is 10 graden. We kunnen nu bovenstaande
      formule gebruiken.

      Het kan echter ook voorkomen dat de nieuwe draaiingshoek -170 graden is.
      De vis kan nu naar rechts draaien waardoor de vis in totaal 200 graden
      zal moeten draaien. Als de vis naar links draait zal de vis maar 160
      graden hoeven te draaien.

      Onderstaande code houdt rekening met dit probleem.
    }
    if FishAngleHorizontal = 0 then
    begin
      FishAngleHorizontal := (FishSpinningSmoothing * FishAngleHorizontal +
        NewFishAngleHorizontal) / (FishSpinningSmoothing + 1);
    end else if FishAngleHorizontal > 0 then
    begin
      Angle1 := Abs(FishAngleHorizontal - NewFishAngleHorizontal);
      Angle2 := 360 - Angle1;

      if Angle1 <= Angle2 then
      begin
        FishAngleHorizontal := (FishSpinningSmoothing * FishAngleHorizontal +
          NewFishAngleHorizontal) / (FishSpinningSmoothing + 1);
      end else
      begin
        FishAngleHorizontal := (FishSpinningSmoothing * FishAngleHorizontal +
          FishAngleHorizontal + Angle2) / (FishSpinningSmoothing + 1);

        if FishAngleHorizontal > 180 then
          FishAngleHorizontal := -180 + (FishAngleHorizontal - 180);
      end;
    end else if FishAngleHorizontal < 0 then
    begin
      Angle1 := Abs(FishAngleHorizontal - NewFishAngleHorizontal);
      Angle2 := 360 - Angle1;

      if Angle1 <= Angle2 then
      begin
        FishAngleHorizontal := (FishSpinningSmoothing * FishAngleHorizontal +
          NewFishAngleHorizontal) / (FishSpinningSmoothing + 1);
      end else
      begin
        FishAngleHorizontal := (FishSpinningSmoothing * FishAngleHorizontal +
          FishAngleHorizontal - Angle2) / (FishSpinningSmoothing + 1);

        if FishAngleHorizontal < -180 then
          FishAngleHorizontal := 180 + (-FishAngleHorizontal - 180);
      end;
    end;

    PreviousFishX := FishXOpenGLUnits;
    PreviousFishZ := FishZOpenGLUnits;
  end;

  {
    Het berekenen van de hoek waarop de vis gedraaid moet worden ten opzichte
    van de z-as (de vis wijst standaard naar rechts) is makkelijker, omdat het
    bereik loopt van -(1/2)Pi tot (1/2)Pi.

    Als er een horizontale snelheid is wordt deze meegenomen in de
    berekeningen. Als er geen horizontale snelheid is dan gaat de vis recht
    omhoog of omlaag.
  }
  if FishMovementHorizontal > 0.02 then
  begin
    FishAngleVertical := (FishSpinningSmoothing * FishAngleVertical +
      (-ArcTan(FishMovementY / FishMovementHorizontal) / Pi * 180)) /
      (FishSpinningSmoothing + 1);
  end else
  begin
    if FishMovementY < -0.02 then
    begin
      FishAngleVertical := (FishSpinningSmoothing * FishAngleVertical + 90) /
        (FishSpinningSmoothing + 1);
    end else if FishMovementY > 0.02 then
    begin
      FishAngleVertical := (FishSpinningSmoothing * FishAngleVertical - 90) /
        (FishSpinningSmoothing + 1);
    end;
  end;

  if (FishMovementY < -0.02) or (FishMovementY > 0.02) then
    PreviousFishY := FishYOpenGLUnits;
end;

procedure TLocaQuariumRenderer.FreeOpenGLRendering;
begin
  {
    Verwijder display lists
  }
  glDeleteLists(AquariumDisplayList, 1);
  glDeleteLists(AquariumOutlineDisplayList, 1);
  glDeleteLists(AxesDisplayList, 1);
  glDeleteLists(FontDisplayListsBase, 96);
  glDeleteLists(Font3DDisplayListsBase, 96);
  glDeleteLists(SkyBoxDisplayList, 1);
  glDeleteLists(TableDisplayList, 1);
end;

{
  Deze twee functies zijn verwijzingen naar functies uit de windows multimedia
  api. Door TimeBeginPeriod aan te roepen wordt de Thread.Sleep functie een stuk
  nauwkeuriger.
}
procedure timeBeginPeriod(Period: UInt); stdcall; external 'winmm.dll';
procedure timeEndPeriod(Period: UInt); stdcall; external 'winmm.dll';

procedure TLocaQuariumRenderer.Execute;
const
  CalculationRate: Integer = 60;
  RefreshRate: Integer = 60;
var
  TicksAfterRendering, TicksBeforeRendering, TicksFrequency: Int64;
  TimeElapsedWhileRendering: Single;

  AdjustedRenderingTime: Single;
  TimeToSleep: Integer;

  NumCalculationsToPerform: Single;
  NumCalculationsToPerformPerRefresh: Single;
begin
  {
    We maken de h_RC rendering context de huidige OpenGL rendering context op de
    thread.
  }
  with frmLocaQuarium do
  begin
    if (not wglMakeCurrent(h_DCPanel, h_RC)) then
    begin
      Raise Exception.Create(
        'Er is een fout opgetreden bij het activeren van de OpenGL rendering ' +
        'context.');
      Exit;
    end;
  end;

  CalculateAquariumDimensionsOpenGL;

  {
    Initializatie van variabelen en display lists.
  }
  ResizePerspective;
  InitOpenGLRendering;

  AdjustedRenderingTime := 1 / RefreshRate;
  NumCalculationsToPerformPerRefresh := CalculationRate / RefreshRate;
  NumCalculationsToPerform := 0;

  {
    Dit zorgt er voor dat de Thread.Sleep functie meer tot op 1 ms nauwkeurig
    wordt.
  }
  TimeBeginPeriod(1);
  QueryPerformanceFrequency(TicksFrequency);

  while not Terminated do
  begin
    if ShouldResizePerspective then
      ResizePerspective;

    {
      Als de afmetingen van het aquarium zijn veranderd moeten enkele display
      lists opnieuw worden aangemaakt.
    }
    if ShouldResizeAquarium then
    begin
      CalculateAquariumDimensionsOpenGL;
      glDeleteLists(AquariumDisplayList, 1);
      glDeleteLists(AquariumOutlineDisplayList, 1);
      glDeleteLists(AxesDisplayList, 1);
      InitOpenGLObjectAquarium;
      InitOpenGLObjectAquariumOutline;
      InitOpenGLObjectAxes;
    end;

    {
      We slaan het huidig aantal ticks van de performance counter op.
    }
    QueryPerformanceCounter(TicksBeforeRendering);

    {
      We renderer alles.
    }
    DrawScene;

    {
       We slaan weer het huidig aantal ticks van de performance counter op.
       Daarna berekenen de tijd die nodig was om alles te renderer.
    }
    QueryPerformanceCounter(TicksAfterRendering);
    TimeElapsedWhileRendering := (TicksAfterRendering - TicksBeforeRendering) /
      TicksFrequency;

    {
      We berekenen hoe lang we de Thread.Sleep functie aan moeten roepen om
      op de gewenste framerate uit te komen.
    }
    TimeToSleep := Trunc((AdjustedRenderingTime - TimeElapsedWhileRendering) *
      1000);

    if TimeToSleep > 0 then
      NumCalculationsToPerform := NumCalculationsToPerform +
        NumCalculationsToPerformPerRefresh
    else
      NumCalculationsToPerform := NumCalculationsToPerform +
        NumCalculationsToPerformPerRefresh *
        (TimeElapsedWhileRendering / AdjustedRenderingTime);

    {
      Hier worden berekeningen uitgevoerd waarvan wij willen dat deze altijd op
      dezelfde snelheid plaatsvinden.
    }

    while NumCalculationsToPerform > 0 do
    begin
      DoCalculations;
      NumCalculationsToPerform := NumCalculationsToPerform - 1;
    end;

    if TimeToSleep > 0 then
      Sleep(TimeToSleep);

    QueryPerformanceCounter(TicksAfterRendering);
    RenderingFPS := (10 * RenderingFPS + (1 / ((TicksAfterRendering -
      TicksBeforeRendering) / TicksFrequency))) / 11;
  end;

  {
    De Thead.Sleep functie mag weer naar zijn oude nauwkeurigheid worden.
    teruggebracht.
  }
  TimeEndPeriod(1);

  FreeOpenGLRendering;
end;

{
  Een functie die wij hebben gemaakt om gemakkelijk balken te kunnen tekenen.
}
procedure TLocaQuariumRenderer.glDrawRectangularCuboid(X1, Y1, Z1, X2, 
      Y2, Z2: Single; FacesToDraw: TRectangularCuboidFaces;
      PColors: POpenGLCuboidColors4f = Nil;
      PTextures: POpenGLCuboidTextures = Nil;
      TexturesNumRepeat: Integer = 1);
begin
  if PTextures <> Nil then
  begin
    if (FrontFace In FacesToDraw) or (AllFaces in FacesToDraw) then
      glDrawVerticalRectangle(X1, Y1, Z1, X2, Y2, Z1, Nil,
        PTextures^.FrontFaceTexture, TexturesNumRepeat);
    if (RightFace In FacesToDraw) or (AllFaces in FacesToDraw) then
      glDrawVerticalRectangle(X2, Y1, Z1, X2, Y2, Z2, Nil,
        PTextures^.RightFaceTexture, TexturesNumRepeat);
    if (BackFace In FacesToDraw) or (AllFaces in FacesToDraw) then
      glDrawVerticalRectangle(X2, Y1, Z2, X1, Y2, Z2, Nil,
        PTextures^.BackFaceTexture, TexturesNumRepeat);
    if (LeftFace In FacesToDraw) or (AllFaces in FacesToDraw) then
      glDrawVerticalRectangle(X1, Y1, Z2, X1, Y2, Z1, Nil,
        PTextures^.LeftFaceTexture, TexturesNumRepeat);
    if (TopFace In FacesToDraw) or (AllFaces in FacesToDraw) then
      glDrawHorizontalRectangle(X1, Y2, Z1, X2, Y2, Z2, Nil,
        PTextures^.TopFaceTexture, TexturesNumRepeat);
    if (BottomFace In FacesToDraw) or (AllFaces in FacesToDraw) then
      glDrawHorizontalRectangle(X1, Y1, Z1, X2, Y1, Z2, Nil,
        PTextures^.BottomFaceTexture, TexturesNumRepeat);
  end else
  begin
    if (FrontFace In FacesToDraw) or (AllFaces in FacesToDraw) then
      glDrawVerticalRectangle(X1, Y1, Z1, X2, Y2, Z1,
        @PColors^.FrontFaceColor, 0, TexturesNumRepeat);
    if (RightFace In FacesToDraw) or (AllFaces in FacesToDraw) then
      glDrawVerticalRectangle(X2, Y1, Z1, X2, Y2, Z2,
        @PColors^.RightFaceColor, 0, TexturesNumRepeat);
    if (BackFace In FacesToDraw) or (AllFaces in FacesToDraw) then
      glDrawVerticalRectangle(X2, Y1, Z2, X1, Y2, Z2,
        @PColors^.BackFaceColor, 0, TexturesNumRepeat);
    if (LeftFace In FacesToDraw) or (AllFaces in FacesToDraw) then
      glDrawVerticalRectangle(X1, Y1, Z2, X1, Y2, Z1,
        @PColors^.LeftFaceColor, 0, TexturesNumRepeat);
    if (TopFace In FacesToDraw) or (AllFaces in FacesToDraw) then
      glDrawHorizontalRectangle(X1, Y2, Z1, X2, Y2, Z2,
        @PColors^.TopFaceColor, 0, TexturesNumRepeat);
    if (BottomFace In FacesToDraw) or (AllFaces in FacesToDraw) then
      glDrawHorizontalRectangle(X1, Y1, Z1, X2, Y1, Z2,
        @PColors^.BottomFaceColor, 0, TexturesNumRepeat);
  end;
end;

procedure TLocaQuariumRenderer.glDrawHorizontalRectangle(X1, Y1, Z1, X2, Y2, 
      Z2: Single; Color: POpenGLColor4f = Nil; Texture: GLUInt = 0; 
      TextureNumRepeat: Integer = 1);
begin
  if Texture > 0 then
  begin
    glBindTexture(GL_TEXTURE_2D, Texture);
  end;

  glBegin(GL_QUADS);
    if Texture > 0 then
    begin
      glTexCoord2f(0, 1);
      glVertex3f(X1, Y2, Z2);
      glTexCoord2f(1, 1);
      glVertex3f(X2, Y2, Z2);
      glTexCoord2f(1, 0);
      glVertex3f(X2, Y2, Z1);
      glTexCoord2f(0, 0);
      glVertex3f(X1, Y1, Z1);
    end else if Color <> Nil then
    begin
      glColor4f(Color^[0], Color^[1], Color^[2], Color^[3]);
      glVertex3f(X1, Y2, Z2);
      glVertex3f(X2, Y2, Z2);
      glVertex3f(X2, Y2, Z1);
      glVertex3f(X1, Y1, Z1);
    end else
    begin
      glVertex3f(X1, Y2, Z2);
      glVertex3f(X2, Y2, Z2);
      glVertex3f(X2, Y2, Z1);
      glVertex3f(X1, Y1, Z1);
    end;
  glEnd;
end;

procedure TLocaQuariumRenderer.glDrawVerticalRectangle(X1, Y1, Z1, X2, Y2, 
      Z2: Single; Color: POpenGLColor4f = Nil; Texture: GLUInt = 0; 
      TextureNumRepeat: Integer = 1);
begin
  if Texture > 0 then
  begin
    glBindTexture(GL_TEXTURE_2D, Texture);
  end;

  glBegin(GL_QUADS);
    if Texture > 0 then
    begin
      glTexCoord2f(0, 1);
      glVertex3f(X1, Y2, Z1);
      glTexCoord2f(1, 1);
      glVertex3f(X2, Y2, Z2);
      glTexCoord2f(1, 0);
      glVertex3f(X2, Y1, Z2);
      glTexCoord2f(0, 0);
      glVertex3f(X1, Y1, Z1);
    end else if Color <> Nil then
    begin
      glColor4f(Color^[0], Color^[1], Color^[2], Color^[3]);
      glVertex3f(X1, Y2, Z1);
      glVertex3f(X2, Y2, Z2);
      glVertex3f(X2, Y1, Z2);
      glVertex3f(X1, Y1, Z1);
    end else
    begin
      glVertex3f(X1, Y2, Z1);
      glVertex3f(X2, Y2, Z2);
      glVertex3f(X2, Y1, Z2);
      glVertex3f(X1, Y1, Z1);
    end;
  glEnd;
end;

procedure TLocaQuariumRenderer.glPrintScreenHorizontalText2D(X, Y: Single;
  Text: String);
begin
  if Text = '' then
    Exit;

  glRasterPos2f(X, Y);
  glPushAttrib(GL_LIST_BIT);
  glListBase(FontDisplayListsBase - 32);
  glCallLists(Length(text), GL_UNSIGNED_BYTE, PChar(Text));

  glPopAttrib;
end;

procedure TLocaQuariumRenderer.glPrintHorizontalText3D(X, Y, Z, Angle: Single;
  Text: String; Scale: Single = 0.2; CenterText: Boolean = True);
var
  Length: GLFloat;
  Loop: Integer;
begin
  if Text = '' then
    Exit;

  glPushMatrix;

  glTranslatef(X, Y, Z);

  glScalef(Scale, Scale, Scale);

  glRotatef(-90, 1, 0, 0);
  glRotatef(Angle, 0, 0, 1);

  glTranslatef(0, -FontGMF[0].gmfCellIncY, 0);

  if CenterText then
  begin
    Length := 0;
    for Loop := 1 to System.Length(Text) do
      Length := Length + FontGMF[Ord(Text[Loop])].gmfCellIncX;

    glTranslatef(-Length / 2, 0, 0);
  end;
  glTranslatef(0, -0.2, 0);

  glPushAttrib(GL_LIST_BIT);
  glListBase(Font3DDisplayListsBase);

  glCallLists(System.Length(Text), GL_UNSIGNED_BYTE, PChar(Text));
  glPopAttrib;

  glPopMatrix;
end;

procedure TLocaQuariumRenderer.glPrintVerticalText3D(X, Y, Z, Angle: Single;
  Text: String; Scale: Single = 0.2; CenterText: Boolean = True);
var
  Length: GLFloat;
  Loop: Integer;
begin
  if Text = '' then
    Exit;

  glPushMatrix;

  glTranslatef(X, Y, Z);

  glScalef(Scale, Scale, Scale);

  glRotatef(Angle, 0, 1, 0);

  glTranslatef(0, 0, 0.2);
  glTranslatef(0, -FontGMF[0].gmfCellIncY, 0);

  if CenterText then
  begin
    Length := 0;
    for Loop := 1 to System.Length(Text) do
      Length := Length + FontGMF[Ord(Text[Loop])].gmfCellIncX;

    glTranslatef(-Length / 2, 0, 0);
  end;
  glTranslatef(0, -0.2, 0);
  
  glPushAttrib(GL_LIST_BIT);
  glListBase(Font3DDisplayListsBase);

  glCallLists(System.Length(Text), GL_UNSIGNED_BYTE, PChar(Text));
  glPopAttrib;

  glPopMatrix;
end;

{
  Deze functie wijst enkele variabelen benodigd voor een goede rendering waardes
  toe.
}
procedure TLocaQuariumRenderer.InitOpenGLRendering;
begin
  //Shadermodel op smooth
  glShadeModel(GL_SMOOTH);

  {
    De achtergrondkleur van de rendering hetzelfde maken als die van een windows
    venster.
  }
  glClearColor(0.9411, 0.9411, 0.9411, 0);

  glClearDepth(1.0);
  glDepthFunc(GL_LEQUAL);

  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

  InitOpenGLFonts;
  InitOpenGLModels;
  InitOpenGLTextures;

  InitOpenGLObjectAquarium;
  InitOpenGLObjectAquariumOutline;
  InitOpenGLObjectAxes;
  InitOpenGLObjectSkyBox;
  InitOpenGLObjectTable;
end;

{
  Deze functie initialiseert display lists voor het tekenen van letters, cijfers
  en andere symbolen. De display list Font3DDisplayListsBase bevat een
  beschrijving van de 3D modellen voor het font dat gebruikt wordt voor de
  cijfers boven de assen. De display list FontDisplayListsBase bevat een
  beschrijving van de bitmaps voor het font dat gebruikt wordt voor de
  informatie over afstanden links bovenin het scherm.
}
procedure TLocaQuariumRenderer.InitOpenGLFonts;
var
  CFont: HFont;
begin
  Font3DDisplayListsBase := glGenLists(256);
  CFont := CreateFont(-24, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET,
    OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, FF_DONTCARE or
    DEFAULT_PITCH, 'Arial');

  with frmLocaQuarium do
  begin
    SelectObject(h_DCPanel, CFont);
    wglUseFontOutlines(h_DCPanel, 0, 255, Font3DDisplayListsBase, 0, 0.2,
      WGL_FONT_POLYGONS, @FontGMF);
  end;

  FontDisplayListsBase := glGenLists(96);
  CFont := CreateFont(-24, 0, 0, 0, FW_BOLD, 0, 0, 0, ANSI_CHARSET,
    OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, FF_DONTCARE or
    DEFAULT_PITCH, 'Courier New');

  with frmLocaQuarium do
  begin
    SelectObject(h_DCPanel, CFont);
    wglUseFontBitmaps(h_DCPanel, 32, 96, FontDisplayListsBase);
  end;
end;

procedure TLocaQuariumRenderer.InitOpenGLModels;
begin
  {
    Het laden van het 3D model van de tafel.

    We hebben een kleine wijziging gemaakt in de functie
    TMaterial.ProcessNextMaterialChunk in Unit3DS zodat textures worden gezocht
    in de map Models.
  }
  TableModel := T3DModel.Create;
  TableModel.LoadFromFile('Models\Table.3DS');
end;

procedure TLocaQuariumRenderer.InitOpenGLObjectAquarium;
var
  AquariumColors: TOpenGLCuboidColors4f;
  I: Integer;
  PAquariumColors: POpenGLColor4f;
begin
  AquariumDisplayList := glGenLists(1);

  glNewList(AquariumDisplayList, GL_COMPILE);
    PAquariumColors := @AquariumColors;
    for I := 0 to (SizeOf(TOpenGLCuboidColors4f) div
      SizeOf(TOpenGLColor4f)) - 1 do
    begin
      PAquariumColors^[0] := 0.48;
      PAquariumColors^[1] := 0.75;
      PAquariumColors^[2] := 0.91;
      PAquariumColors^[3] := 0.2;
      Inc(PAquariumColors);
    end;

    glDrawRectangularCuboid(-AquariumWidthScaled / 2,
      -AquariumHeightScaled / 2, -AquariumDepthScaled / 2,
      AquariumWidthScaled / 2, AquariumHeightScaled / 2,
      AquariumDepthScaled / 2, [AllFaces], @AquariumColors);
  glEndList();
end;

procedure TLocaQuariumRenderer.InitOpenGLObjectAquariumOutline;
begin
  AquariumOutlineDisplayList := glGenLists(1);

  glNewList(AquariumOutlineDisplayList, GL_COMPILE);
    //Lijnen om het aquarium heen
    glColor3f(0.5, 0.5, 0.5);
    glBegin(GL_LINES);
      //Aquarium Voorkant
      glVertex3f(-AquariumWidthScaled / 2, AquariumHeightScaled / 2,
        AquariumDepthScaled / 2);
      glVertex3f(AquariumWidthScaled / 2, AquariumHeightScaled / 2,
        AquariumDepthScaled / 2);
      glVertex3f(AquariumWidthScaled / 2, -AquariumHeightScaled / 2,
        AquariumDepthScaled / 2);
      glVertex3f(-AquariumWidthScaled / 2, -AquariumHeightScaled / 2,
        AquariumDepthScaled / 2);

      //Aquarium Rechterkant
      glVertex3f(AquariumWidthScaled / 2, AquariumHeightScaled / 2,
        AquariumDepthScaled / 2);
      glVertex3f(AquariumWidthScaled / 2, AquariumHeightScaled / 2,
        -AquariumDepthScaled / 2);
      glVertex3f(AquariumWidthScaled / 2, -AquariumHeightScaled / 2,
        -AquariumDepthScaled / 2);
      glVertex3f(AquariumWidthScaled / 2, -AquariumHeightScaled / 2,
        AquariumDepthScaled / 2);

      //Aquarium Achterkant
      glVertex3f(AquariumWidthScaled / 2, AquariumHeightScaled / 2,
        -AquariumDepthScaled / 2);
      glVertex3f(-AquariumWidthScaled / 2, AquariumHeightScaled / 2,
        -AquariumDepthScaled / 2);
      glVertex3f(-AquariumWidthScaled / 2, -AquariumHeightScaled / 2,
        -AquariumDepthScaled / 2);
      glVertex3f(AquariumWidthScaled / 2, -AquariumHeightScaled / 2,
        -AquariumDepthScaled / 2);

      //Aquarium Linkerkant
      glVertex3f(-AquariumWidthScaled / 2, AquariumHeightScaled / 2,
        -AquariumDepthScaled / 2);
      glVertex3f(-AquariumWidthScaled / 2, AquariumHeightScaled / 2,
        AquariumDepthScaled / 2);
      glVertex3f(-AquariumWidthScaled / 2, -AquariumHeightScaled / 2,
        AquariumDepthScaled / 2);
      glVertex3f(-AquariumWidthScaled / 2, -AquariumHeightScaled / 2,
        -AquariumDepthScaled / 2);
    glEnd;
  glEndList();
end;

procedure TLocaQuariumRenderer.InitOpenGLObjectAxes;
var
  CurrNumber: Single;
  DistanceToNextNumber: Single;
  I: Integer;
begin
  AxesDisplayList := glGenLists(1);

  glNewList(AxesDisplayList, GL_COMPILE);
    glColor4f(0.3, 0.3, 0.3, 1);

    glPrintVerticalText3D(
      AquariumWidthScaled / 2,
      -AquariumHeightScaled / 2 + 0.005 + 0.12,
      -(AquariumDepthScaled / 2),
      135,
      '0',
      0.05
    );

    glDrawVerticalRectangle(
      AquariumWidthScaled / 2,
      -AquariumHeightScaled / 2,
      -(AquariumDepthScaled / 2),
      AquariumWidthScaled / 2,
      -AquariumHeightScaled / 2 + 0.1,
      -(AquariumDepthScaled / 2) + 0.005
    );

    glDrawVerticalRectangle(
      AquariumWidthScaled / 2,
      -AquariumHeightScaled / 2,
      -(AquariumDepthScaled / 2),
      AquariumWidthScaled / 2 - 0.005,
      -AquariumHeightScaled / 2 + 0.1,
      -(AquariumDepthScaled / 2)
    );

    //X-As
    DistanceToNextNumber := CalculateGLPrintWidth(IntToStr(Trunc(AquariumDepth)),
      0.05) + 0.01;
    CurrNumber := 0;
    for I := 1 to Trunc(AquariumDepth) - 1 do
    begin
      CurrNumber := CurrNumber + (AquariumDepthScaled / Trunc(AquariumDepth));

      if CurrNumber > DistanceToNextNumber then
        glDrawVerticalRectangle(
          AquariumWidthScaled / 2,
          -AquariumHeightScaled / 2,
          -(AquariumDepthScaled / 2) + (AquariumDepthScaled / AquariumDepth)
            * I - 0.005,
          AquariumWidthScaled / 2,
          -AquariumHeightScaled / 2 + 0.1,
          -(AquariumDepthScaled / 2) + (AquariumDepthScaled / AquariumDepth)
            * I
        )
      else
        glDrawVerticalRectangle(
          AquariumWidthScaled / 2,
          -AquariumHeightScaled / 2,
          -(AquariumDepthScaled / 2) + (AquariumDepthScaled / AquariumDepth)
            * I - 0.005,
          AquariumWidthScaled / 2,
          -AquariumHeightScaled / 2 + 0.08,
          -(AquariumDepthScaled / 2) + (AquariumDepthScaled / AquariumDepth)
            * I
        );

      if CurrNumber > DistanceToNextNumber then
      begin
          glPrintVerticalText3D(
            AquariumWidthScaled / 2,
            -AquariumHeightScaled / 2 + 0.005 + 0.12,
            -(AquariumDepthScaled / 2) + (AquariumDepthScaled / AquariumDepth)
              * I - 0.0025,
            90,
            IntToStr(I),
            0.05
          );

          CurrNumber := 0;
      end;
    end;

    glDrawVerticalRectangle(
        AquariumWidthScaled / 2,
        -AquariumHeightScaled / 2,
        -(AquariumDepthScaled / 2) + (AquariumDepthScaled / AquariumDepth) * I -
        0.005,
        AquariumWidthScaled / 2,
        -AquariumHeightScaled / 2 + 0.08,
        -(AquariumDepthScaled / 2) + (AquariumDepthScaled / AquariumDepth) * I
      );

    //Y-As
    DistanceToNextNumber := CalculateGLPrintWidth(
      IntToStr(Trunc(AquariumWidth)),
      0.05) + 0.01;
    CurrNumber := 0;
    for I := 1 to Trunc(AquariumWidth) - 1 do
    begin
      CurrNumber := CurrNumber + (AquariumWidthScaled / Trunc(AquariumWidth));

      if CurrNumber > DistanceToNextNumber then
        glDrawVerticalRectangle(
          AquariumWidthScaled / 2 - (AquariumWidthScaled / AquariumWidth) * I +
            0.005,
          -AquariumHeightScaled / 2,
          -(AquariumDepthScaled / 2),
          AquariumWidthScaled / 2 - (AquariumWidthScaled / AquariumWidth) * I,
          -AquariumHeightScaled / 2 + 0.1,
          -(AquariumDepthScaled / 2)
        )
      else
        glDrawVerticalRectangle(
          AquariumWidthScaled / 2 - (AquariumWidthScaled / AquariumWidth) * I +
            0.005,
          -AquariumHeightScaled / 2,
          -(AquariumDepthScaled / 2),
          AquariumWidthScaled / 2 - (AquariumWidthScaled / AquariumWidth) * I,
          -AquariumHeightScaled / 2 + 0.08,
          -(AquariumDepthScaled / 2)
        );

      if CurrNumber > DistanceToNextNumber then        
      begin
          glPrintVerticalText3D(
            AquariumWidthScaled / 2 - (AquariumWidthScaled / AquariumWidth) *
              I - 0.0025,
            -AquariumHeightScaled / 2 + 0.005 + 0.12,
            -(AquariumDepthScaled / 2),
            180,
            IntToStr(I),
            0.05
          );

          CurrNumber := 0;
      end;
    end;
  glEndList();
end;

procedure TLocaQuariumRenderer.InitOpenGLObjectSkyBox;
var
  SkyBoxTextures: TOpenGLCuboidTextures;
begin
  SkyBoxDisplayList := glGenLists(1);

  glNewList(SkyBoxDisplayList, GL_COMPILE);
    SkyBoxTextures.FrontFaceTexture := SkyBoxFrontFaceTexture;
    SkyBoxTextures.RightFaceTexture := SkyBoxRightFaceTexture;
    SkyBoxTextures.BackFaceTexture := SkyBoxBackFaceTexture;
    SkyBoxTextures.LeftFaceTexture := SkyBoxLeftFaceTexture;
    SkyBoxTextures.TopFaceTexture := SkyBoxTopFaceTexture;
    SkyBoxTextures.BottomFaceTexture := SkyBoxBottomFaceTexture;

    glEnable(GL_TEXTURE_2D);
    glDrawRectangularCuboid(-50, -50, -50, 50, 50, 50, [AllFaces], Nil,
      @SkyBoxTextures, 2);
  glEndList();
end;

procedure TLocaQuariumRenderer.InitOpenGLObjectTable;
begin
  TableDisplayList := glGenLists(1);

  glNewList(TableDisplayList, GL_COMPILE);
    TableModel.Draw;
  glEndList();
end;

procedure TLocaQuariumRenderer.InitOpenGLTextures;
begin
  {
    LoadBMPRGBATexture is een wijziging die wij hebben gemaakt op de functie
    LoadBMPRGBTexture uit de Textures unit die wij van het internet hebben
    gehaald. De oorspronkelijke code was niet in staat om BMP bestanden met
    alpha kanalen te laden, maar de afbeelding van de vis heeft wel een
    alphakanaal. Gelukkig was de LoadBMPRGBTexture functie goed geschreven
    dus voldeden twee kleine wijzigingen in de functie.
  }

  LoadBMPRGBATexture('Textures\Fish.bmp', FishTexture, False);

  LoadBMPRGBATexture('Textures\Skybox\Front.bmp', SkyBoxFrontFaceTexture,
    False);
  LoadBMPRGBATexture('Textures\Skybox\Right.bmp', SkyBoxRightFaceTexture,
    False);
  LoadBMPRGBATexture('Textures\Skybox\Back.bmp', SkyBoxBackFaceTexture,
    False);
  LoadBMPRGBATexture('Textures\Skybox\Left.bmp', SkyBoxLeftFaceTexture,
    False);
  LoadBMPRGBATexture('Textures\Skybox\Top.bmp', SkyBoxTopFaceTexture,
    False);
  LoadBMPRGBATexture('Textures\Skybox\Bottom.bmp', SkyBoxBottomFaceTexture,
    False);
end;

procedure TLocaQuariumRenderer.InvokeResizeAquarium;
begin
  ShouldResizeAquarium := True;
end;

procedure TLocaQuariumRenderer.InvokeResizePerspective;
begin
  ShouldResizePerspective := True;
end;

procedure TLocaQuariumRenderer.KillFont;
begin
  glDeleteLists(Font3DDisplayListsBase, 256);
end;

procedure TLocaQuariumRenderer.ResizePerspective;
begin
  with frmLocaQuarium.pnlLocaQuarium3D do
  begin
    glViewport(0, 0, Width, Height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, Width / Height, 0.1, 100.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity;
  end;
end;

end.
