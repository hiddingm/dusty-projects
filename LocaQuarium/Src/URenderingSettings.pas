unit URenderingSettings;

interface

  {
    Dit hoeft niet per se in een aparte unit. Wij houden er echter niet zo van
    om waardes rechtstreeks uit GUI componenten te halen.
  }

var
  ShowAxes: Boolean;
  ShowDistancesInLeftCorner: Boolean;
  ShowFPS: Boolean;
  ShowPositionIndicators: Boolean;
  ShowSkybox: Boolean;

implementation

end.
