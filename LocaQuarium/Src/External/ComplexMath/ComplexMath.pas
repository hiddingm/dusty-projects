{
  Bron: http://delphi.about.com/library/weekly/aa070103a.htm
  Auteur: Thomas Bejstam
}

unit ComplexMath;

interface

uses
  Dialogs, Windows, SysUtils, Classes, Controls, Math;

type
  TComplexNumber = record
    R : single;
    I : single;
  end;

  TComplexNumberArray = Array of TComplexNumber;

  TComplexMath = class(TComponent)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner : TComponent); override;
    class function Add(C1, C2 : TComplexNumber) : TComplexNumber; overload;
    { Returns the complex sum of C1 and C2 }

    class function Add(C1, C2, C3 : TComplexNumber) : TComplexNumber; overload;
    { Returns the complex sum of C1 and C2 and C3 }

    class function Add(C1, C2, C3, C4 : TComplexNumber) : TComplexNumber; overload;
    { Returns the complex sum of C1 and C2 and C3 and C4 }

    class function Arg(C1 : TComplexNumber) : single;
    { Returns the argument of C1 (in radians) }
    class function Complex(RealPart, ImaginaryPart : single) : TComplexNumber;
    { Returns the Complex number formed by RealPart + j * ImaginaryPart }

    class function ComplexAbs(C1 : TComplexNumber) : single;
    { Returns the absolute value of the complex number C1 }

    class function ComplexExp(C1 : TComplexNumber) : TComplexNumber;
    { Returns Exp(C1) as a complex number }

    class function ComplexLn(C1 : TComplexNumber) : TComplexNumber;
    { Returns the natural logarithm of C1 as a complex number }

    class function ComplexPower(Base, Exponent : TComplexNumber) : TComplexNumber;
    { Returns Base raised to Exponent, both in complex }

    class function ComplexToString(C1 : TComplexNumber) : string;
    { Returns a string representation of a complex number }

    class function ComplexToStringF(C1 : TComplexNumber; Format : TFloatFormat; Precision, Digits : integer) : string;
    { Returns a string representation of a complex number, with formatting possibilities }

    class function Conjugate(C1 : TComplexNumber) : TComplexNumber;
    { Returns the complex conjugate of a complex number }

    class function Divide(C1, C2 : TComplexNumber) : TComplexNumber; overload;
    { Returns C1/C2, both complex }

    class function Divide(C1: TComplexNumber; R2: Single) : TComplexNumber; overload;

    class function ImaginaryPart(C1 : TComplexNumber) : single;
    { Returns the imaginary part of a complex number }

    class function Multiply(C1, C2 : TComplexNumber) : TComplexNumber; overload;
    { Returns C1 * C2 }

    class function Multiply(C1: TComplexNumber; R2: Single) : TComplexNumber; overload;

    class function Multiply(C1, C2, C3 : TComplexNumber) : TComplexNumber; overload;
    { Returns C1 * C2 * C3 }

    class function Multiply(C1, C2, C3, C4 : TComplexNumber) : TComplexNumber; overload;
    { Returns C1 * C2 * C3 * C4 }

    class function RealPart(C1 : TComplexNumber) : single;
    { Returns the real part of a complex number }

    class function Square(C1 : TComplexNumber) : TComplexNumber;
    { Returns the square of a complex number, i.e. C1 * C1 }

    class function SquareRoot(C1 : TComplexNumber) : TComplexNumber;
    { Returns the square root of a complex number }

    class function Subtract(C1, C2 : TComplexNumber) : TComplexNumber;
    { Returns C1 - C2 }

    class function ComplexTanh(C1 :TComplexNumber) : TComplexNumber;
    { Returns Tanh for a complex number }

  published
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('delphi.about.com', [TComplexMath]);
end;

constructor TComplexMath.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
end;

class function TComplexMath.Add(C1, C2 : TComplexNumber) : TComplexNumber;
begin
  Result.R := C1.R + C2.R;
  Result.I := C1.I + C2.I;
end;

class function TComplexMath.Add(C1, C2, C3 : TComplexNumber) : TComplexNumber;
begin
  Result.R := C1.R + C2.R + C3.R;
  Result.I := C1.I + C2.I + C3.I;
end;

class function TComplexMath.Add(C1, C2, C3, C4 : TComplexNumber) : TComplexNumber;
begin
  Result.R := C1.R + C2.R + C3.R + C4.R;
  Result.I := C1.I + C2.I + C3.I + C4.I;
end;

class function TComplexMath.Subtract(C1, C2 : TComplexNumber) : TComplexNumber;
begin
  Result.R := C1.R - C2.R;
  Result.I := C1.I - C2.I;
end;

class function TComplexMath.Multiply(C1, C2 : TComplexNumber) : TComplexNumber;
begin
  Result.R := C1.R * C2.R - C1.I * C2.I;
  Result.I := C1.R * C2.I + C1.I * C2.R;
end;

class function TComplexMath.Multiply(C1: TComplexNumber; R2: Single) : TComplexNumber;
begin
  Result.R := C1.R * R2;
  Result.I := C1.I * R2;
end;

class function TComplexMath.Multiply(C1, C2, C3 : TComplexNumber) : TComplexNumber;
var
  Temp : TComplexNumber;
begin
  Temp := Multiply(C1, C2);
  Result := Multiply(Temp, C3);
end;

class function TComplexMath.Multiply(C1, C2, C3, C4 : TComplexNumber) : TComplexNumber;
var
  Temp : TComplexNumber;
begin
  Temp := Multiply(C1, C2, C3);
  Result := Multiply(Temp, C4);
end;

class function TComplexMath.Divide(C1, C2 : TComplexNumber) : TComplexNumber;
var
  Temp : TComplexNumber;
begin
  Temp := Multiply(C1, Conjugate(C2));
  Result.R := Temp.R / (Sqr(C2.R) + Sqr(C2.I));
  Result.I := Temp.I / (Sqr(C2.R) + Sqr(C2.I));
end;

class function TComplexMath.Divide(C1: TComplexNumber; R2: Single) : TComplexNumber;
var
  Temp : TComplexNumber;
begin
  Result.R := Temp.R / R2;
  Result.I := Temp.I / R2;
end;

class function TComplexMath.Conjugate(C1 : TComplexNumber) : TComplexNumber;
begin
  Result.R := C1.R;
  Result.I := -C1.I;
end;

class function TComplexMath.ComplexExp(C1 : TComplexNumber) : TComplexNumber;
begin
  Result.R := Exp(C1.R) * Cos(C1.I);
  Result.I := Exp(C1.R) * Sin(C1.I);
end;

class function TComplexMath.Complex(RealPart, ImaginaryPart : single) : TComplexNumber;
begin
  Result.R := RealPart;
  Result.I := ImaginaryPart;
end;

class function TComplexMath.Arg(C1 : TComplexNumber) : single;
begin
  Result := Arctan(C1.I / C1.R);
end;

class function TComplexMath.ComplexAbs(C1 : TComplexNumber) : single;
begin
  Result := Sqrt(Sqr(C1.R) + Sqr(C1.I));
end;

class function TComplexMath.ComplexLn(C1 : TComplexNumber) : TComplexNumber;
begin
  Result.R := Ln(ComplexAbs(C1));
  Result.I := Arg(C1);
end;

class function TComplexMath.ComplexPower(Base, Exponent : TComplexNumber) : TComplexNumber;
begin
  Result := ComplexExp(Multiply(Exponent, ComplexLn(Base)));
end;

class function TComplexMath.ImaginaryPart(C1 : TComplexNumber) : single;
begin
  Result := C1.I;
end;

class function TComplexMath.RealPart(C1 : TComplexNumber) : single;
begin
  Result := C1.R;
end;

class function TComplexMath.Square(C1 : TComplexNumber) : TComplexNumber;
begin
  Result := ComplexPower(C1, Complex(2, 0));
end;

class function TComplexMath.SquareRoot(C1 : TComplexNumber) : TComplexNumber;
var
  ValueToRoot: Double;
begin
  //Geeft foute resultaten bij negatieve re�le getallen
  //Result := ComplexPower(C1, Complex(0.5, 0));

  //Blijkbaar gaat iets mis met de afronding waardoor een getal onder de 0 kan komen met een miniem aantal
  ValueToRoot := Sqrt(Power(C1.R, 2) + Power(C1.I, 2)) + C1.R;
  if ValueToRoot < 0 then
    ValueToRoot := 0;

  Result.R := Sqrt(ValueToRoot / 2);

  ValueToRoot := Sqrt(Power(C1.R, 2) + Power(C1.I, 2)) - C1.R;
  if ValueToRoot < 0 then
    ValueToRoot := 0;

  Result.I := Sqrt(ValueToRoot / 2);

  if C1.I < 0 then
    Result.I := -Result.I;
end;

class function TComplexMath.ComplexToStringF(C1 : TComplexNumber; Format : TFloatFormat; Precision, Digits : integer) : string;
begin
  if Abs(C1.R) < Power(10, -Precision) then
  begin // Real part is zero
    if Abs(C1.I) < Power(10, -Precision) then
      // Real part and imaginary part is zero
      Result := '0'
    else
      if C1.I < 0 then
        // Real part is zero, imaginary part is negative
        Result := '-j' + FloatToStrF(-C1.I, Format, Precision, Digits)
      else
        // Real part is zero, imaginary part is positive
        Result := 'j' + FloatToStrF(C1.I, Format, Precision, Digits);
  end
  else
  begin // Real part is not zero
    if Abs(C1.I) < Power(10, -Precision) then
      // Real part is not zero, imaginary part is zero
      Result := FloatToStrF(C1.R, Format, Precision, Digits)
    else
      if C1.I < 0 then
        // Real part is not zero, imaginary part is negative
        Result := FloatToStrF(C1.R, Format, Precision, Digits) + ' - j' + FloatToStrF(-C1.I, Format, Precision, Digits)
      else
        // Real part is not zero, imaginary part is positive
        Result := FloatToStrF(C1.R, Format, Precision, Digits) + ' + j' + FloatToStrF(C1.I, Format, Precision, Digits);
  end;
end;

class function TComplexMath.ComplexToString(C1 : TComplexNumber) : string;
begin
  Result := ComplexToStringF(C1, ffGeneral, 5, 5);
end;

class function TComplexMath.ComplexTanh(C1 :TComplexNumber) : TComplexNumber;
begin
  Result := Divide(Subtract(ComplexExp(C1),
            ComplexExp(Multiply(C1, Complex(-1, 0)))),
            Add(ComplexExp(C1), ComplexExp(Multiply(C1,
            Complex(-1, 0)))));
end;

end.
