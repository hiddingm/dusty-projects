unit UfrmRenderingSettings;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmRenderingSettings = class(TForm)
    GroupBox1: TGroupBox;
    chkShowAxes: TCheckBox;
    chkShowSkybox: TCheckBox;
    chkShowPositionIndicators: TCheckBox;
    chkShowDistancesInLeftCorner: TCheckBox;
    chkShowFPS: TCheckBox;
    procedure chkShowAxesClick(Sender: TObject);
    procedure chkShowPositionIndicatorsClick(Sender: TObject);
    procedure chkShowSkyboxClick(Sender: TObject);
    procedure chkShowDistancesInLeftCornerClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chkShowFPSClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRenderingSettings: TfrmRenderingSettings;

implementation

uses
  URenderingSettings;

{$R *.dfm}

procedure TfrmRenderingSettings.chkShowAxesClick(Sender: TObject);
begin
  ShowAxes := chkShowAxes.Checked;
end;

procedure TfrmRenderingSettings.chkShowDistancesInLeftCornerClick(
  Sender: TObject);
begin
  ShowDistancesInLeftCorner := chkShowDistancesInLeftCorner.Checked;

  if ShowDistancesInLeftCorner then
  begin
    chkShowFPS.Enabled := True;
    chkShowFPS.OnClick(chkShowFPS);
  end else
    chkShowFPS.Enabled := False;
end;

procedure TfrmRenderingSettings.chkShowFPSClick(Sender: TObject);
begin
  if chkShowFPS.Enabled then
    ShowFPS := chkShowFPS.Checked
  else
    ShowFPS := False;
end;

procedure TfrmRenderingSettings.chkShowPositionIndicatorsClick(Sender: TObject);
begin
  ShowPositionIndicators := chkShowPositionIndicators.Checked;
end;

procedure TfrmRenderingSettings.chkShowSkyboxClick(Sender: TObject);
begin
  ShowSkybox := chkShowSkybox.Checked;
end;

procedure TfrmRenderingSettings.FormShow(Sender: TObject);
begin
  chkShowAxes.Checked := ShowAxes;
  chkShowDistancesInLeftCorner.Checked := ShowDistancesInLeftCorner;
  chkShowPositionIndicators.Checked := ShowPositionIndicators;
  chkShowSkybox.Checked := ShowSkybox;
end;

end.
